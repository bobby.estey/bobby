# [Ronald Lee Ermey](https://rleeermey.com/)

# Links

- [Mini Lee vs Doughboy](https://www.youtube.com/watch?v=R_J0gYl66EA)

- [Snafu's Salty Bloopers](https://www.youtube.com/watch?v=goSXxZNTTqo)
     - [Mini Lee -  33:57](https://youtu.be/goSXxZNTTqo?t=2040)
     - [Great Sign Off - 49:58](https://youtu.be/goSXxZNTTqo?t=2998)
     
# Full Metal Jacket
- Put side by side and compare, so great:
     - [Rudolph vs Full Metal Jacket](https://www.youtube.com/watch?v=dg-2eMhw0Sw)
     - [Original](https://youtu.be/tHxf17yJsKs?t=81)
