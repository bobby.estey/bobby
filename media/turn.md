TURN - Washington Spies

# History

- [Washington Spies](https://en.wikipedia.org/wiki/Washington%27s_Spies)

## Series

- There were 4 seasons.  You have to watch this series, true story of how we won the Revolutionary War
- If we didn't have the spies, there would be no United States, PERIOD.
- FINAL EPISODE:  The actual stories of what happened to each individual.

## DO NOT MISS THIS OPPORTUNITY - OR PURCHASE THE DVDs

#### Season 1

- [Episode 1](https://www.dailymotion.com/video/x3i6kvb)
- [Episode 2](https://www.dailymotion.com/video/x8f3b5x)
- [Episode 3](https://www.dailymotion.com/video/x8hulla)
- [Episode 4](https://www.dailymotion.com/video/x8f3b6a)
- [Episode 5](https://www.dailymotion.com/video/x8huln8)
- [Episode 6](https://www.dailymotion.com/video/x8f3b76)
     - 03:20 - Prisoner Exchange - Colonists are beaten almost to death, starving and yet, we return the British with no reprisals
     - 05:10 - Hanging for stealing spoons
     - 06:20 - The Start of the Spy Ring
     - 09:20 - Using Civilians as Spies
     - 16:55 - Learning how to Spy (SO VERY IMPORTANT) - from the expert, dropping information, never use name but numbers, etc.
     - 33:50 - TRUST
- [Episode 7](https://www.dailymotion.com/video/x8f3b6f)
     - POWs on Ships - so sad
- [Episode 8](https://www.dailymotion.com/video/x8hw4xq)
- [Episode 9](https://www.dailymotion.com/video/x8f3b8b)
- [Episode 10](https://www.dailymotion.com/video/x8f4ra1)

## Greatest Scenes

- I will try and find the episode of the following on better quality
- [General Washington to General Lee, TO THE REAR](https://www.youtube.com/watch?v=EljK5Md4bXk)

## Media for Purchase

- [Brian Kilmeade](https://www.amazon.com/George-Washingtons-Secret-Six-audiobook/dp/B00GDJI1R6/ref=sr_1_16?hvadid=557160863412&hvdev=c&hvlocphy=1014226&hvnetw=g&hvqmt=e&hvrand=9669202384389163977&hvtargid=kwd-315665342041&hydadcr=13962_13352821&keywords=dvd+turn+washington+spies&qid=1690517726&sr=8-16)
