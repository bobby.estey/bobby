# [1973](https://www.ttttontheweb.com/tttt69s5guide.html)

# [George Tweed / Miss Indian America - Oct 22, 1962](https://www.youtube.com/watch?v=gZd6791UF6w)
     - https://www.youtube.com/watch?v=jAU4QcVE2eg

# Joyce Hinson - Young girl rescued by a St. Bernard / Bernie P. - Author of "Compulsive Gambler"

[73-1723](https://www.youtube.com/watch?v=bwzKKWhlReg)

# Richard Buggy - Undercover Policeman / George Gerbner - Researcher of the effects of heavy TV viewing

[73-1724](https://www.youtube.com/watch?v=rhsIyWQF2kA)

This was the funniest episode.  Kitty Carlisle and Joe Garagiola had no idea that impostors 2 and 3 were their kids, 12:33.  Joe even made the comment his kid had a hockey puck in his mouth, 5:33.  The next set was funny with Peggy Cass here, 24:18 at the end the comparisons to the make up.  GREAT SHOW.
