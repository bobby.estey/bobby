# Google Meet

- Go here:  [Google Meet](https://meet.google.com/)
- Press New meeting

![New Meeting](newMeeting.png)

- Start an instant meeting

![Start Instant Meeting](startInstantMeeting.png)

- Lower left meeting link appears
- Copy link

![Copy Link](copyLink.png)

- Go to email, paste link in email and send to participants
- Recipient Will ask to Join
- Accept Join