# Media

- [Example of Hurricane Milton 2024-10-09 Draining Tampa Bay](drainingTheBay20241009T185839.mp4)
     - Northern Hemisphere Low Pressure Systems rotate counterclockwise
     - Note that Hurricane Milton, just south of Tampa, FL is forcing water out of the Bay
     - When we use to live in VA, sometimes we were save by flooding the the eye would be just East of the Chesapeake Bay
     - [Clearwater Tide Chart 2024-10-09](tideChart20241009.png)

