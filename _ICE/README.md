# Hurricane Information

## DO NOT LISTEN TO THE MEDIA, only Trust NOAA

- Only go here [National Oceanic and Atmospheric Administration (NOAA)](https://www.noaa.gov/) for the **FACTS**
- The Media does not have a **CLUE** or **UP-TO-DATE INFORMATION** on **WEATHER**

## Miscellaneous Links

- [Google Meet Instructions](./googleMeet)
- [Media](./media)

## Excellent Sites

- [Real Time World Status on Weather](https://earth.nullschool.net/)
     |Address|Latitude|Longitude|
     |--|--|--|
     |316 Windrush Blvd 33785|27.887560|-82.845170|
     |3180 Horse Road 23072|37.297950|-76.439640|
- [Hurricane Watch Net](https://www.hwn.org/)
- [FEMA Search](https://msc.fema.gov/portal/search)

## NOAA Active Hurricane Sites

- [Hurricane Milton](https://tidesandcurrents.noaa.gov/inundationdb/cidstorm.html?stormname=Milton)

## Hurricane - Florida

- [Duke Energy Florida Outage Map](https://outagemaps.duke-energy.com/#/current-outages/fl)
- [Clearwater Beach, FL Tides and Currents - Forecast Guidance](https://tidesandcurrents.noaa.gov/ofs/ofs_station.html?stname=Clearwater%20Beach&ofs=tb&stnid=8726724&subdomain=0)
     - Scroll down to the 2nd Chart for Tide and Predicted Tide Levels
- [Fort Myers, FL Tides and Currents - **NO FORECAST GUIDANCE**](https://tidesandcurrents.noaa.gov/stationhome.html?id=8725520)

## Hurricane - Achilles, VA

- [Yorktown, VA Tides and Currents - Forecast Guidance](https://tidesandcurrents.noaa.gov/ofs/ofs_station.html?stname=Yorktown%20USCG%20Training%20Center&ofs=cb&stnid=8637689)
     - Scroll down to the 2nd Chart for Tide and Predicted Tide Levels

## Categories

|Category|Speed MPH|Speed KTS|Speed KMH|Details|
|--|--|--|--|--|
|1|74-95|64-82|119-153|Very dangerous winds will produce some damage: Well-constructed frame homes could have damage to roof, shingles, vinyl siding and gutters. Large branches of trees will snap and shallowly rooted trees may be toppled. Extensive damage to power lines and poles likely will result in power outages that could last a few to several days. Irene of 1999, Katrina of 2005, and several others were Category One hurricanes at landfall in South Florida.|
|2|96-110|83-95|154-177|Extremely dangerous winds will cause extensive damage: Well-constructed frame homes could sustain major roof and siding damage. Many shallowly rooted trees will be snapped or uprooted and block numerous roads. Near-total power loss is expected with outages that could last from several days to weeks. Frances of 2004 was a Category Two when it hit just north of Palm Beach County, along with at least 10 other hurricanes which have struck South Florida since 1894.|
|3|111-129|96-112|178-208|Devastating damage will occur: Well-built framed homes may incur major damage or removal of roof decking and gable ends. Many trees will be snapped or uprooted, blocking numerous roads. Electricity and water will be unavailable for several days to weeks after the storm passes. Unnamed hurricanes of 1909, 1910, 1929, 1933, 1945, and 1949 were all Category 3 storms when they struck South Florida, as were King of 1950, Betsy of 1965, Jeanne of 2004, and Irma of 2017.|
|4|130-156|113-136|209-251|Catastrophic damage will occur: Well-built framed homes can sustain severe damage with loss of most of the roof structure and/or some exterior walls. Most trees will be snapped or uprooted and power poles downed. Fallen trees and power poles will isolate residential areas. Power outages will last weeks to possibly months. Most of the area will be uninhabitable for weeks or months. The 1888, 1900, 1919, 1926 Great Miami, 1928 Lake Okeechobee/Palm Beach, 1947, Donna of 1960 made landfall in South Florida as Category Four hurricanes.|
|5|157 >|137 >|252 >|Catastrophic damage will occur: A high percentage of framed homes will be destroyed, with total roof failure and wall collapse. Fallen trees and power poles will isolate residential areas. Power outages will last for weeks to possibly months. Most of the area will be uninhabitable for weeks or months. The Keys Hurricane of 1935 and Andrew of 1992 made landfall in South Florida as Category Five hurricanes.|
