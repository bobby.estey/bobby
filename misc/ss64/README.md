# Simon Sheppard

```
From:  simon@ss64.com
To:    bobby cv64

Wed, Feb 10, 2021 at 7:36 AM

Hi Bobby,

Interesting but not at all what I was thinking of when I named the website, I just wanted something short and quick to type.

Thinking about it, a long long time ago I worked at Ferranti defence, so maybe I was influenced by their naming systems to some extent. Everything had a TLA.

Best wishes

Simon

On 2021-02-10 15:17, bobby cv64 wrote:
Just wanted to inform you that I have been to your informative site many times all the way back to 2010 and really appreciate what you have done.  I was there today and just realized SS64 would be a US Submarine #64.  So I decided to look up the ship and there it was USS O-3 (kind of weird name).  The photos are public domain of the O-3.

https://www.google.com/search?q=ss64+us+navy&tbm=isch&ved=2ahUKEwjHiJboyt_uAhVUqXIEHYs_DMoQ2-cCegQIABAA&oq=ss64+us+navy&gs_lcp=CgNpbWcQAzoGCAAQBRAeOgQIABAYUIwdWIkrYLktaABwAHgAgAF-iAHeA5IBAzcuMZgBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=QPUjYMfoNtTSytMPi_-w0Aw&bih=886&biw=1848&client=ubuntu&hs=aZu#imgrc=VSyAU2tlt80I1M


I was on CV64 Constellation :-)  Bobby
```