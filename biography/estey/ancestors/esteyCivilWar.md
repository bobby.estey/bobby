# Estey's of the Civil War

|NAME|STATE|UNIT|COMPANY|MISCELLANEOUS|
|--|--|--|--|--|
|Abraham|NY|4th Heavy Artillery|E||
|Charles|ME|1st Veteran Infantry|E||
|Charles|ME|7th Infantry|||	
|Charles C|VT|9th Infantry|K||	
|Charles E|NH|4th Infantry|K||
|Charles S|NY|173rd Infantry|D||	
|Charles W|IL|117th Infantry|K|Corporal|
|David M|NY|26th Calvary|F||
|Edgar W|NH|9th Infantry|G||
|Edward S|MA|1st Calvary|B||
|George W|ME|29th Infantry|D||
|Henry J|IL|1st Calvary|K|Black, Far|
|Henry Warner|VT|9th Infantry|K||
|Horace P|NH|4th Infantry|K||
|Issac H|ME|17th Infantry|C||
|James H|OH|8th Calvary|A|Sergent|
|James R|MA|28th Infantry|H||
|Lemuel F|MA|23rd Infantry|B||
|Lester|NY|142nd Infantry|D||
|Nathan C|WI|1st Calvary|A||
|Silas V|OH|26th Infantry|D||
|Taylor W|IL|65th Infantry|E||
|William H|MA|51st Infantry Military|C||
|William J|VT|9th Infantry|K||
|William H|NH|5th Infantry|B||
|Thomas|LA|1st Strawbridges Infantry|C|Confederate|
