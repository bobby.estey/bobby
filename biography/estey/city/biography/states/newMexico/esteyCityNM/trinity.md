# Trinity Nuclear Test  
  
- [Trinity Nuclear Test](trinityNuclearTest.jpg)    
- [Trinity - William Fink](trinityWilliamFink.jpg)  

- The Trinity nuclear test site is located in New Mexico, Southeast of San Antonio, NM.  This area was named "Journade del Muerte" by the early Spaniards.  This translates into Dead Man's Route.  It is located in a section of the White Sands Missile Range that was originally used for bomb practice during WW II and is in the area bounded by 33-28' to 33-50' and 106-22' to 106-41'.
- The site was given the code name TRINITY by J. Robert Oppenheimer after he had read a poem the night before the meeting to assign a code name.
- "Batter my heart, three-personed God, for you.  As yet but my knock, breathe, shine, and seek to mend.  That I may rise and stand, o'er throw me and bend.  Your force to break, blow, burn, and make me now."  by: John Dunne
- The "three-personed God" is known as the Trinity which is the Father, Son and Holy Ghost.
- The nuclear bomb program was run out of Los Alamos, NM laboratories and there were two types of bomb being made ready for use against Japan. (One Uranium 235 and one Plutonium 239.)
- The Uranium 235 bomb (Little Boy) was a two sector, gun type and was considered simple enough that no test would be required prior to actual use.
- The Plutonium 239 bomb (Fat Man) was a more complex implosion device using a 5 kgm Plutonium spherical core and a conventional high explosive outer shell to compress the spherical configuration that gave the bomb the name "Fat Man".  It was due to the uncertainties of the explosive compression of Pu 239 that required this bomb to be tested at the Trinity site.
- The "Fat Man" explosion was at 5:30 AM on July 16, 1945 atop a 100 foot tower above ground zero.  The 5 kgm of Pu 239 yielded about the equivalent of 17 kilotons of TNT.
- Data on the Trinity site and the nuclear bombs was taken form the book, City of Fire by James W. Kunetka, copyright in 197B and published by Printice Hall Publishing Company.

# W. L. Fink, October 17, 1996

- Mr. Fink has a BSEE degree form the University of Oklahoma and worked from 1956 to 1969 at the General Dynamics Corporation's Nuclear Aerospace Research Facility.
- Mr. Fink later worked with the United States Department of Transportation from 1972 to 1993 in the Maritime Admninistration where he was the Project Engineer for the overisght of the Nuclear Ship SAVANNAH, now being held in the James River Reserve fleet in Virginia.
- Mr. Fink used a Geinger Counter to check the level of residual radioactivity on a typical pendant (weight was 84 grains including the 18 kt gold backing).  The unit was checked with the Alpha shield in place and the reading was essentially background, wihich in California near sea level was 50 counts per minute.  With the Alpha radiation shield measured a nominal 175 counts per minute, which indicates an Alpha emission rate of 125 counts per minute or approximately 0.28 milliR/hour.  Background radiation varies with altitude; but, in California the 50 counts per minute would equal about 0.08 milliR/hour.  This indicates that the Trinity Glass emissions form the tested sample are about 3.5 times background.
- These levels of radiation are far less than a person would experience in a typical commerical airliner at altitude and are harmless.  The fact that the primary radioactivity measured appears to be from Alpha radiation means that if you are concerned about radiation from the samples, a clear coating of finger nail polish is adequate to shield out the Alpha radiations.  No spectral analysis was performed and there is a probability that a very low level of Beta and Gamma radiation may exist; however they are clearly below background.
- The Alpha emissions are function of surface area exposed and a larger sample size (big pendant) of Trinity Glass would give proprtionally higher readings; however, they are still low emitters.
- This low level Alpha emission is, however, proof of the authenticity of the Trinity Glass.
