# Estey

## Famous Dates

- 02 April 1917 - US enters WWI
- 11 November 1918 @ 1100 - end of WWI
- 13 September 1814 - star spangled banner - http://www.nps.gov/fomc/
- 27 October 1858 - teddy roosevelt

## Estey Farm Dairy

- ![Milk Bottle Cap](esteyFarmDairy1.jpg)
- ![Milk Bottle Front](esteyFarmDairy2.jpg)
- ![Milk Bottle Back](esteyFarmDairy3.jpg)
