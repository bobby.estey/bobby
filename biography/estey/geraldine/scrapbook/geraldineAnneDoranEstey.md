# Geraldine Anne Doran Estey (19360820-20140125)

- [00 front](00front.jpg)

## Family
|Relation|Name|
|--|--|
|Great Grandmother Ann Walsh|
|Grandfather|[Michael Dennis Doran](https://www.findagrave.com/memorial/139618353/michael-m-doran)|
|Grandmother|[Anna Ella Keet Doran](https://www.findagrave.com/memorial/139618400/anna-ella-doran)|
|Father|[John Dennis Doran](https://www.findagrave.com/memorial/123981249/john-dennis-doran)|
|Mother|[Annabelle Jean Stephens Doran](https://www.findagrave.com/memorial/123981209/annabelle-j-doran)|
|Brother|Dennis Doran|
|Sister|Annabelle Doran|
|Sister|Kathy Doran|
|Aunt|[Edna Rebecca Doran](../../edna/edna.md)|
|Aunt|[Julia Ann Doran](https://www.findagrave.com/memorial/123981188/julia-ann-doran)
|Cousin|Myrna Stephens|

#### Additional Information

- Great Grandmother Ann Walsh - Passed 1909
     - Son John passed Spring 1909
     - Son Michael Dennis Doran
     - Son Michael - Born Peoria, IL
     - Niece Mary Long
- [Nellie Keet](https://www.findagrave.com/memorial/175404966/nellie-s-keet)
     - Daughter Anna Ella Keet - married John Dennis Doran
     - Daughter Susie - married L.H. Abrams
     - Daughter [Edna B Keet](https://www.findagrave.com/memorial/152982122/edna-b-coppers) - married Jesse L Coppers
     - Son [John N Keet](https://www.findagrave.com/memorial/47701336/john-n-keet)
     - Son [Manley John Keet](https://www.findagrave.com/memorial/234340579/manley-john-keet)
     - Son Herbert - died early 20s and diabetes
- [Martin Doran](https://www.findagrave.com/memorial/55209575/martin-doran) - Born 1838 Ireland
- [Mollie A Keet](https://www.findagrave.com/memorial/47701335/mollie-a-keet)
- [Lillian Valoras White](https://www.findagrave.com/memorial/47701341/lillian-valoras-white)
- [Jesse L Coppers](https://www.findagrave.com/memorial/152982153/jesse-l-coppers)
- [Caroline Haines Coppers](https://www.findagrave.com/memorial/67755600/caroline-coppers)

## Addresses
|Key|Address|
|--|--|
|Lafayette|[3428 Lafayette Street, Denver, CO 80205](https://www.google.com/maps/@39.7650954,-104.9709023,3a,67.7y,75.47h,89.87t/data=!3m7!1e1!3m5!1sEQrR9HakXwXnmKv9j0YtMg!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3DEQrR9HakXwXnmKv9j0YtMg%26cb_client%3Dsearch.gws-prod.gps%26w%3D360%26h%3D120%26yaw%3D75.534874%26pitch%3D0%26thumbfov%3D100!7i16384!8i8192?entry=ttu)|
|Decatur|[4564 Decatur Street, Denver, CO 80211](https://www.google.com/maps/@39.7795408,-105.0216777,3a,75y,75.23h,90t/data=!3m7!1e1!3m5!1s7cPfDao6Iir6qAhfJW3-Xw!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3D7cPfDao6Iir6qAhfJW3-Xw%26cb_client%3Dsearch.gws-prod.gps%26w%3D86%26h%3D86%26yaw%3D75.23436%26pitch%3D0%26thumbfov%3D100!7i16384!8i8192?entry=ttu)|
|Winona|[1577 Winona Court, Denver, CO 80204](https://www.google.com/maps/place/1577+Winona+Ct,+Denver,+CO+80204/@39.7419317,-105.047492,3a,75y,263.82h,90t/data=!3m7!1e1!3m5!1smmSjYr7NmfJTubwQ58KSew!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3DmmSjYr7NmfJTubwQ58KSew%26cb_client%3Dsearch.gws-prod.gps%26w%3D360%26h%3D120%26yaw%3D263.82272%26pitch%3D0%26thumbfov%3D100!7i16384!8i8192!4m15!1m8!3m7!1s0x876b8745bb426e41:0x8f1e651e3b5f97b2!2s1577+Winona+Ct,+Denver,+CO+80204!3b1!8m2!3d39.741901!4d-105.0478358!16s%2Fg%2F11c1bh4k00!3m5!1s0x876b8745bb426e41:0x8f1e651e3b5f97b2!8m2!3d39.741901!4d-105.0478358!16s%2Fg%2F11c1bh4k00?entry=ttu)|
|29th|[6301 West 29th Ave, Denver, CO 80207](https://www.google.com/maps/@39.7583939,-104.9152268,3a,75y,90.83h,90t/data=!3m7!1e1!3m5!1sPG3We32BqNmILUQ4sA1QrQ!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3DPG3We32BqNmILUQ4sA1QrQ%26cb_client%3Dsearch.gws-prod.gps%26w%3D86%26h%3D86%26yaw%3D90.82953%26pitch%3D0%26thumbfov%3D100!7i16384!8i8192?entry=ttu)|
- [01 front](01front.jpg)
- [01 back](01back.jpg)
     - St Anthony Hospital - see the "X", my Father said that was where I was born.  August 20, 1936 (19360820)
     - My Father, holding me on Thanksgiving Day 1936 at my Grandmothers and Grandfathers home.
     - My Mother, holding me Thanksgiving Day 1936 at Lafayette
- [02 front](02front.jpg)
- [02 back](02back.jpg)
     - My GrandMother, Thanksgiving Day at Lafayette
     - Aunt Edna, holding me at Lafayette
     - My Grandfather and my Father
     - My Grandfather is holding me
     - My Grandfather and me 19370704
- [03 front](03front.jpg)
- [03 back](03back.jpg)
     - Grandfather, Grandmother holding me.  Aunt Edna and Mother, 19370704
     - Mother and me
- [04 front](04front.jpg)
- [04 back](04back.jpg)
     - Mother and me
- [05 front](05front.jpg)
- [05 back](05back.jpg)
     - me
- [06 front](06front.jpg)
- [06 back](06back.jpg)
     - me
     - Town Talk Bread Truck
- [07 front](07front.jpg)
- [07 back](07back.jpg)
     - me
- [08 front](08front.jpg)
- [08 back](08back.jpg)
     - me
     - me next to an Ash Pit
     - me rides her first car
     - me and Denny Doran (Beach Court)
- [09 front](09front.jpg)
- [09 back](09back.jpg)
     - me
- [10 front](10front.jpg)
- [10 back](10back.jpg)
     - me and Denny
     - me, Grandmother and Denny
     - Grandfather, his nickname on C&S Railroad "Sliver"
     - me at Lafayette
- [11 front](11front.jpg)
- [11 back](11back.jpg)
     - me and Denny
     - me
- [12 front](12front.jpg)
- [12 back](12back.jpg)
     - me and Probabaly Denny?
     - me rolls
     - me rides again
- [13 front](13front.jpg)
- [13 back](13back.jpg)
     - me and Charlie McCarthy?  What is he worth today?
     - me and Myrna?
- [14 front](14front.jpg)
- [14 back](14back.jpg)
     - me - see that Harness, I was a runaway!
- [15 front](15front.jpg)
- [15 back](15back.jpg)
     - me
- [16 front](16front.jpg)
     - me - Merry Christmas 1939
- [17 front](17front.jpg)
- [17 back](17back.jpg)
     - me - builds a snowman
     - me - Christmas 1939, I loved to hand the red bell!
     - me - Christmas 1939, A happy time!
- [18 front](18front.jpg)
- [18 back](18back.jpg)
     - me - Christmas 1939
     - me - holding a gift from Santa
     - me and Denny Doran
- [19 front](19front.jpg)
     - My mother and kids playing ring around the rosy
- [20 front](20front.jpg)
- [20 back](20back.jpg)
     - me
- [21 front](21front.jpg)
- [21 back](21back.jpg)
     - me and Denny
     - me and Noreen Hershburger?
- [22 front](22front.jpg)
     - Annabelle, Denny and me
- [23 front](23front.jpg)
- [23 back](23back.jpg)
     - me - Decatur
- [24 front](24front.jpg)
- [24 back](24back.jpg)
     - me - Decatur
     - Denny, Annabelle and me - Decatur
     - me, Auntie Marg, Chuck Stephens, Denny
     - Myrna, me and Denny
- [25 front](25front.jpg)
- [25 back](25back.jpg)
     - me
     - me and Mother
- [26 front](26front.jpg)
- [26 back](26back.jpg)
     - me, Annabelle and Denny - Decatur
     - me
     - Denny, Annabelle, Mother, me
     - me, Father, Mother, Annabelle, Denny
- [27 front](27front.jpg)
- [27 back](27back.jpg)
     - me
     - me, Annabelle, Denny - Decatur
     - me, Christmas time
     - Myrna, Denny, me (Back), Annabelle (front)
- [28 front](28front.jpg)
- [28 back](28back.jpg)
     - me - Decatur
     - me, Denny - Decatur
     - Denny, me, Annabelle - Cool Hats - Decatur
     - me, Denny, Annabelle and our Treasured Joys - Decature
- [29 front](29front.jpg)
- [29 back](29back.jpg)
     - me
     - me and my Buggy
- [30 front](30front.jpg)
- [30 back](30back.jpg)
     - me and Denny
     - Annabelle, me, Denny - Probably Rocky Mountains
     - Mother and me
     - me
- [31 front](31front.jpg)
- [31 back](31back.jpg)
     - me - Decatur
- [32 front](32front.jpg)
- [32 back](32back.jpg)
     - Cousin Patty Stephens, Uncle Bill and me - Decatur
     - me - Fishing Trip
     - me, Denny, Annabelle - Fishing Trip
     - Annabelle, Denny, me - Fishing Trip
- [33 front](33front.jpg)
- [33 back](33back.jpg)
     - me, Father LeMeiux @ St Catherines, Denver, CO
     - me, Sister Rose Terese @ St Catherines, Denver, CO
     - me, 1st Communion @ St Catherines, Denver, CO
     - Barbara Young (Friend) - 1st Communion @ St Catherines, Denver, CO
- [34 front](34front.jpg)
- [34 back](34back.jpg)
     - Mother at my 1st Communion @ St Catherines, Denver, CO
     - Sister Rose Terese (Back), me, Don Durant, Barbara Young (Middle), Joe Canary? (Front)
     - Group of 1st Communion @ St Catherines, Denver, CO
- [35 front](35front.jpg)
- [35 back](35back.jpg)
     - Nuns leading Communicants @ St Catherines, Denver, CO
     - Photo of everyone from West Side of Federal @ St Catherines, Denver, CO
- [36 front](36front.jpg)
- [36 back](36back.jpg)
     - me and Grandfather
     - Grandfather
     - Aunt Julia, me - Decatur
     - me and Aunt Edna - Decatur
- [37 front](37front.jpg)
- [37 back](37back.jpg)
- [V Mail 1](vmail1.jpg)
- [V Mail 2](vmail2.jpg)
     - Denny, me, Annabelle - Decatur
     - Johnnie Gray (Friend of Parents and Best Man at wedding)
     - me, Annabelle, Mother, Denny
     - Denny, our snowman with legs, me
- [38 front](38front.jpg)
- [38 back](38back.jpg)
     - me on a four legger
     - me, Denny, our Dad loved flying Kites
     - me at Winona
- [39 - 1](39-1.jpg)
- [39 - 2](39-2.jpg)
     - Stack Imploding
- [40 front](40front.jpg)
- [40 back](40back.jpg)
     - me - 29th
     - Annabelle
- [41 front](41front.jpg)
- [41 back](41back.jpg)
     - Picnic with Hershburger Family
     - James "Hershie" Hershburger, Mother, me, Midge Hershberger
     - Our Rabbit "Judy" from Buck and Jean O'Brien
- [42 front](42front.jpg)
- [42 back](42back.jpg)
     - I graduated from Grade School, went out of state with Aunt Julia and Cousin Carole
     - Church in Taos, NM
     - St Thomas Church, Santa Fe, NM
     - Carole and me at the State Line
- [43 front](43front.jpg)
- [43 back](43back.jpg)
     - Miscellaneous Post Cards
- [44 front](44front.jpg)
- [44 back](44back.jpg)
     - Miscellaneous Post Cards
- [45 front](45front.jpg)
- [45 back](45back.jpg)
     - me, Father
     - Annabelle, Mother, Denny
     - COG Railway, Colorado Springs, CO
- [46 front](46front.jpg)
- [46 back](46back.jpg)
     - me, 1949 - served cotton dress, blue ribbon
     - me, 1949 - 4h competition, white ribbon
- [47 front](47front.jpg)
- [47 back](47back.jpg)
     - me, Saint Domenic's Grade School
- [48 front](48front.jpg)
- [48 back](48back.jpg)
     - Aunt Edna
- [49 front](49front.jpg)
- [49 back](49back.jpg)
     - Aunt Edna and Mother in India


