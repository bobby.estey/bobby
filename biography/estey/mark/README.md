# Mark Daniel Estey (Omaha, Nebraska, USA)

- Mark is my Cousin, however, he is more than a cousin, he is a BROTHER to my immediate family and SON to my Parents
- Mark is 7 months older than me and we were raised about 500 miles apart
     - Mark was raised in Omaha, Nebraska 
     - I was raised in Denver, Colorado
- Mark was a GIFTED Athlete:  Wrestling, Baseball and Track were the sports he excelled

## Wrestling (Mark's Primary Sport)

#### Junior High School (72-74)

- Weight 132

#### High School (75-77)

- Weight 132, 138

#### College (78-79)

- Weight 142, 150

#### 1980 Olympics

- Selected to represent the United States of America Olympic Wrestling Team - weight class 143 pounds
- Mark was unable to attend due to the Boycott in 1980

#### Amateur Athletic Union (AAU) Wrestling

- [AAU WebSite](https://aauwrestling.net/)
     - Mark Estey - First Place - weight class 140 pounds
     
# Fun Stuff

- Haven't seen Mark for Years
- Bobby, Ester and Mark Estey
     - BTW, my wife has a funny name, no "H", the Spanish spelling, Ester Estey

![Bobby, Ester, Mark Estey](esteyBobbyEsterMark.jpg)

- A Fun Joke at the Airport, haven't seen Mark for Years, so I waited at the Las Vegas Airport made up this sign, "Mr Teabag" as passengers were arriving
- People were all laughing waiting for the arrivals, wondering, who is Mister Teabag
- Even the arriving passengers saw the sign, were laughing, who is Mister Teabag
- IT'S A JOKE
- When we left the Airport and went to Luxor for Lunch, I had Mark pose for a picture with the sign
- That is the best about Mark, always liked Humor

![Mister Tea Bag](20231110_154943_mrTeaBag_50.jpg)

Cheers! :beers:
