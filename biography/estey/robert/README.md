# Robert William Estey Sr

29 June 1935 (St Catherine Hospital) - 24 April 2007

## Images

- [Brand New](bobEstey.png)
- [Miscellaneous](bobEstey11.png)
- [High School](bobEstey20.png)
- [Parents](rolandBobEthelEstey.png)

## Quotes to me (Bobby) from my Father

#### Some of my Father's statements to me as a kid, while they are funny, they had a clear message 🙂

- When they were passing out Brains, you thought they said Trains and you said I will take the first one
- Go to the Right, I would go to the Left and Dad would say, no your other Right
- MY FAVORITE:  Your friend has half a Brain, You have half a Brain, put your Brains together and you still end up with half a Brain

## Obituary

Bob was born in [Omaha, Nebraska](https://en.wikipedia.org/wiki/Omaha,_Nebraska), June 29, 1935 to Ethel Marie Toohey and Roland Benedict Estey.  Having two Brothers, John (deceased) and Joseph and a Sister, Margaret and his being the youngest of the four children, this completed their family.

He attended [St. Philip Neri Kindergarten](https://spnschoolomaha.com/), [St. Cecelia's](https://www.stcecilia.net/) Grade School and St. Cecelia's High School, Omaha.

At a young age, he played hockey with his many friends.  He even took a few lessons from [Gordie Howe](https://en.wikipedia.org/wiki/Gordie_Howe), [Terry Sawchuck](https://en.wikipedia.org/wiki/Terry_Sawchuk) and [Maurice Pronovost](https://en.wikipedia.org/wiki/Marcel_Pronovost#Toronto_Maple_Leafs) of the [Omaha Knights](https://en.wikipedia.org/wiki/Omaha_Knights) Hockey Team who later were admitted to the Hockey Hall of Fame.

Bob loved sports - Baseball, Football, Hockey, Basketball - High School, College and Professional.  He would "inhale" the newspapers and magazines to the point where time would get away from him and got him "in trouble" a time or two.  He knew what happened to players no one heard of for a few years and knew their "stats".  He remained faithful to [Notre Dame](https://www.nd.edu/) and the [Nebraska Huskers](https://www.unl.edu/).

Bob met Geri (a native Coloradan!) at [Creighton University](https://www.creighton.edu/).  The often found themselves going out to dinner (he never had Italian Spaghetti until they dated!), watching [Boy's Town](https://www.boystown.org/) play basketball during the regular season and the State Playoffs, going to movies watching high school football games, going for rides in the country (how he loved the country!) or visiting relatives.

They became engage and married May 18, 1957 at [St. Peter and Paul Catholic Church](https://wheatridgecatholic.org/) in Wheat Ridge, CO.  They lived in Omaha and [Council Bluffs](https://en.wikipedia.org/wiki/Council_Bluffs,_Iowa) and had three of the five children, Robert W. Estey, Jr., Jeanne Marie Estey and Michael Dennis Estey, who were all born in Omaha, Nebraska.  Eventually, Bob decided to move to the Denver area where they added Stephen Mark Estey and Judy Terese Estey to their family.

Bob also belonged to the [Nebraska National Guard](https://ne.ng.mil/).  He rode to most of these meetings with someone many have hear of.

Bob coached baseball for the [Northglenn](https://en.wikipedia.org/wiki/Northglenn,_Colorado) Little League for many years, was active in [Immaculate Heart of Mary Catholic Church's](https://ihmco.org/) many programs including one that interested youngsters in politics, teaching CCD and helped with the transportation of children to these classes.

- [1971 King Soopers](kingSoopers.jpg)
- [1972 Empire Savings](empireSavings.png)
- [1972 Weaver Electric](mikeEsteyBaseball1.png)

Bob worked for the ["the old Frontier Airlines"](https://en.wikipedia.org/wiki/Frontier_Airlines_(1950%E2%80%931986)) for 27 years and was [Frontier Airlines 1964 Chairmen](../../frontierAirlines/robertEsteyChairman.jpg) for the I.A.M. Union at Frontier.  He was deeply saddened when this company was taken into bankruptcy because in the end all bills were paid with interest to the various firms.  He then was a Counselor at Consumer Credit Counseling, Denver, but later had to take a medical disability from this company.

Bob loved his Grand Children - he was so proud of them!

- Rebekah - daughter of Bob Jr. and Ester
- Cassie and Danielle - daughters of Jeanne and Tom Collins (deceased)
- Kyle Daniel - son of Bart Warren, Jeanne's stepson
- Marissa - daughter of Steve and Marla
- Kyle Andrew - son of Steve and Shelley
- Jordan - daughter of Judy and Jim Krause

A few years back, Bob decided to get classmates who had moved to Colorado together and this has been a blessing for him as well as their spouses.  It certainly helped him get through some of the rough times of his many medical problems.

## Funeral Mass

- 11:00 a.m. Tuesday, 1 May 2007, Immaculate Heart of Mary
- [Funeral Mass Front](robertEstey0Front.jpg)
- [Funeral Mass Back](robertEstey0Back.jpg)
