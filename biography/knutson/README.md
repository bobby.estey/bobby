# Knutson

![Mitch Knutson - 7th Grade - Northeast Junior High](mitchKnutson7thNortheast.jpg)

## Mitch (AKA Eddie Haskell)

- Mitch was two years younger than me.  I befriended Mitch when I first met him as a friend of the Pachecos and the other neighborhood kids
- Mitch was also a friend of my middle Brother, [Mike Estey](../mike/README.md)
- Mitch and I would do all kinds of activities, especially, Biking, Golfing and most of all: **BEING RASCALS**
- Mitch would always come over to the "ESTEY house" after school and we would have a lot of fun with my Family and the other kids

#### Mischief and Rascals

- Mitch and I would do all kinds of fun stuff, nothing EVIL, just kid stuff
- [My Father](../robert/README.md) would tell us kids, stay away from Mitch, he is just trouble
- SORRY DAD, that troublemaker kid is **SUPER SUCCESSFUL**

#### Hole In One

- [Report](report.jpg)
- [Trophy](trophy.jpg)

####  BOTTOM LINE

- If you ever wanted to know who is Mitch Knutson, look at the **[Leave It to Beaver](https://en.wikipedia.org/wiki/Leave_It_to_Beaver)** TV Shows
- **MITCH WAS [EDDIE HASKELL](https://en.wikipedia.org/wiki/Eddie_Haskell)**
     - **Mitch, "Hello Mrs Estey, you are looking nice today?"**
     - **Mitch, "Hello Mr Estey, I hope you had a wonderful day at work today, would you like anything from me today?"**
- MITCH is a Great Guy and my Brothers and Sisters know this :-)

## Cheryl

![Cheryl Knutson - 7th Grade - Northeast Junior High](cherylKnutson9thNortheast.jpg)

- Cheryl was a classmate in several classrooms, a very nice person always and I have so much respect for Cheryl
- Sixth Grade I was ashamed of the Class and myself of a Science Exhibit Vote of who had the best Exhibit for our class
     - I know the Student's name will not disclose, Former Student (FS) - had a simple poster board of an eye ball with documentation
     - Cheryl - built a VOLCANO with many HOURS of effort, which was an actual working model
- After the vote of 100% for FS's exhibit I was disgusted with MYSELF, HOW UNFAIR, I will never forget, until this day
- A couple days later Cheryl was given the opportunity to demonstrate the Volcano Exhibit and the entire class was amazed, explosions, lava flow simulations and much more

## Kids are Mean

- Cheryl and God please forgive me for not being RIGHTEOUS and not standing for excellence
- Cheryl, I apologize and am very sorry for what I have done to you during that vote
- You were the Real Champion
- I do remember how you were crying and could not believe no one voted for you

# Please forgive me for my cruelty for not voting the truth
