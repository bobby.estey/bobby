# Hiking

- Below are the two trails that I walk.

- I usually walk the 5 mile route because I am mostly away from the traffic on US 50.

![5 Mile Walk](5mileWalk.png)

- The 6 mile route, I actually walk to the Dayton Disc Golf Course (arrow 1) and walk the dirt path next to the Carson River and end the dirt path at River Park (arrow 2).  I estimate the route is an extra 0.5 to 1 mile longer, than what Google Maps is plotting.

![6 Mile Walk](6mileWalk.png)
