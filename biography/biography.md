# Bobby Estey Biography

I am Bobby Estey and this is my Biography.

I have been to 48 of 50 states (Alaska, North Dakota are remaining) and several countries in my lifetime.

- Nebraska my birth state
- Colorado where I was raised
- California when I was in the Navy
- Arizona my first employment when I graduated with a Computer Science Degree
- Virginia my previous state
- Nevada my current residence

|State<br>Entered Union|Shape / Flag|State<br>Entered Union|Shape / Flag|
|--|--|--|--|
|Arizona (AZ)<br>48th - 1912|![](arizona.png)|California (CA)<br>31st - 1850|![](california.png)|
|Colorado (CO)<br>38th - 1876|![](colorado.png)|Nebraska (NE)<br>37th - 1867|![](nebraska.png)|
|Nevada (NV)<br>36th - 1864|![](nevada.png)|Virginia (VA)<br>10th - 1788|![](virginia.png)|

## So what is my favorite state?

Arizona.  I learned so much in Arizona.  [Sunrise Park Resort](https://www.google.com/search?q=sunrise+park+report&tbm=isch&ved=2ahUKEwiNn-vNtZz4AhVhlmoFHXRaB34Q2-cCegQIABAA&oq=sunrise+park+report&gs_lcp=CgNpbWcQAzoFCAAQgAQ6BggAEB4QCDoECAAQGFDoYVjPkAFg4JIBaABwAHgAgAFriAGHCpIBAzcuNpgBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=H9WfYs2TCuGsqtsP9LSd8Ac&bih=970&biw=1854)

You would think I learned to snow ski in Colorado, never happened.

Water skiing, yes Arizona, the reservoirs north and east of Phoenix.  Hunting, Fishing and more, I learned all these activities in Arizona.  What is very impressive are the Architecture of homes, landscaping, nothing else compares.  Everything is new and much more.  Arizona is a diverse state:  
 - [Mountains & Canyons](https://www.google.com/search?q=havasupai+falls&client=ubuntu&hs=mnR&sxsrf=ALiCzsbtatq4dsNFCFHL5bfNJbe78i7AjQ:1654644123702&source=lnms&tbm=isch&sa=X&ved=2ahUKEwip-ePZvZz4AhVdK0QIHbYBDUMQ_AUoAXoECAIQAw&biw=1658&bih=889&dpr=1)
 - [Grasslands](https://www.google.com/search?q=arizona+grasslands&tbm=isch&ved=2ahUKEwjU5ob-vZz4AhW8tGoFHfz5DDcQ2-cCegQIABAA&oq=arizona+grasslands&gs_lcp=CgNpbWcQAzIFCAAQgAQ6BggAEB4QBzoICAAQgAQQsQM6BAgAEBhQgRVYxDBgyzNoAHAAeACAAXCIAdkIkgEDNC43mAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=592fYpTXLrzpqtsP_POzuAM&bih=889&biw=1658&client=ubuntu&hs=mnR)
 - [Desert](https://www.google.com/search?q=arizona+saguaro+national+park&tbm=isch&ved=2ahUKEwiH2eHavZz4AhXDnGoFHeWNBcwQ2-cCegQIABAA&oq=arizona+sagu&gs_lcp=CgNpbWcQARgDMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDoECCMQJzoECAAQQzoICAAQsQMQgwE6CwgAEIAEELEDEIMBOggIABCABBCxAzoHCAAQsQMQQ1DuJ1iGPGCGW2gAcAB4AIABbYgBsAqSAQM0LjmYAQCgAQGqAQtnd3Mtd2l6LWltZ8ABAQ&sclient=img&ei=nd2fYseWLsO5qtsP5ZuW4Aw&bih=889&biw=1658&client=ubuntu&hs=mnR)

## Constellation CV64 (United States Navy)

https://gitlab.com/bobby.estey/bobby/-/blob/main/biography/navy/navy.md

## Motorola

I received 3 degrees, AAS, BS and MS.  I was awarded the University Thesis Award for the year 2000 which was both a document and actual software that my employer at the time, Motorola, utilized and implemented.  Motorola was my favorite company because they were the pioneers on several technologies and the best practice was the technical ladder.

The technical ladder enabled advancement through 5 graded principals from your peers:
- Technical Impact
- Contribution
- Mentoring
- Leadership
- Participation and Business Impact

This might not sound to interesting but these attributes I live my life daily.  I like to keep up-to-date with technology and concentrate my studies primarily with Java and Web Technologies.  I believe in the open source technology paradigm of which I enjoy the W3C, Apache, SourceForge and GNU Web sites.

## Family

I have a wonderful daughter Rebekah who is in the movie industry as a film and 3D editor and my awesome Wife, and that is no typo, her name is Ester with the 
Spanish spelling and of course Ester Estey.

![Biography](biography.png)
