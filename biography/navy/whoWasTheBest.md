# USS Constellation (CV 64) WestPac 1980 - AIMD - Officers and Chiefs

## Who was the Best

I saw this photo of my last Western Pacific (WestPac) cruise in 1980 of the Officers and Chiefs who lead the Aviation Intermediate Maintenance Department (AIMD). AIMD was the best Department with Ship's Company and everyone knew this as fact.  5 of 6 Officers were PRIOR ENLISTED, which was an amazing statistic.

![Officers and Chiefs](officersChiefsCV641980.jpg)

I tried to rank them starting at the greatest to the least.  COULDN'T DO IT.  I knew 10 personally, even among the 10, who was the greatest?  Then I realized.  EVERYONE was great and different.  Each one of them was unique and we were lucky to be lead by wonderful Men.  Who was #1, who was #2, the answer, NONE, they were all great for their own particular reason.  I know for sure CDR Robertshaw, who made (CAPTAIN USN) died and I am sure others in this picture have moved on.  You think nothing of this until later.  I also knew a couple of the others, not as detailed.  God will reward all these leaders of our past.  TRUST ME.  

Going from left to right (again the Men I knew):

- LCDR Orton - didn't know him to much but he was always the energetic, positive attitude guy
- PRC Belt - had to be #1 or very close.  Everyone hated Belt, until you knew the Man.  Chief Belt was Sgt Carter in Gomer Pyle USMC, REGULATIONS, ORDERS, Man by the Book.  Chief Belt was the greatest if you were on the right side of the law, if you ever were wrong, you would pay.  I can go on and on about Chief Belt.  Brow lines through the hanger bay, standing watches were absolutely great, great to work with, awesome man and the crew loved him once they understood, there were NO GREY areas.  Chief Belt was BINARY, either RIGHT OR WRONG.  He would even go up against a USN CAPTAIN or ADMIRAL if you were correct.  REGULATIONS AND UNDERWAY WAS THE ONLY WAY
- CDR Robertshaw (19340613-20170217) - LEADERSHIP, INTEGRITY, RESPECTFUL, HONOR!!! - super smart, kind, everyone loved the guy (Officers and Enlisted).  His going away party the entire DEPARTMENT was allowed off DUTY, UNHEARD OF, and one guy from IM4 was crying so bad that Robertshaw was leaving, he kiss him on the LIPS, EVERYONE, LAUGHED SO HARD, even the ULTRA CONSERVATIVE OFFICER Women were laughing couldn't stop.  I WISH YOUTUBE was around, EVERYONE, I MEAN EVERYONE LOST IT.  CDR Robertshaw even had the SHOCK of WHAT THE HELL JUST HAPPENED TO ME, with the funny looking face of disgust while at the same time, LAUGHING TOO. Robertshaw respected his crew first!!!  That was his secret.  He didn't DEMAND respect.  He gave everyone RESPECT FIRST.
     - [Donald George Robertshaw Obituary](https://www.turrentinejacksonmorrow.com/obituaries/donald-george-robertshaw)
     - [AIMD Department Head Photo](https://nara.getarchive.net/media/commander-donald-g-robertshaw-usn-uncovered-4374d1)
- ENS Duke - had a couple discussions with this man.  He was a kind and respectable man.  He was IM4 Officer and his group even painted a HUGE SIGN in the back of the ship, "Chief, Duke and Sons".  The previous IM4 officers were JERKS, and Duke was GREAT.
- LCDR Prebul - was my last Division Officer, a LT at the time - kind and gentle man.  VERY SMART.  This man was very sensitive on performance evaluations.  The division Chiefs would send their write up and Mr Prebul would completely do rewrites turning a performance rating into something with meaning.  The Division had no idea the amount of effort this man did on evaluations. 
- AZCS Wagner - smart man, just met him a couple times, very professional
- AMSC Coffland - my last Chief that I reported to.  The man was no nonsense, kind and always worked agreements with anyone.  This man could turn an argument into friendship, I saw him do this EVERYDAY.  Tell me what the problem is, let's find a solution and let's move on and be Brothers.  I was so saddened when Chief Belt was replaced by Chief Coffland, but this was the beginning, everyone is great in their own way.
- LTJG Lauseng - An Officers, Officer - this was my first Division Officer of IM2, being transferred from IM1 to IM2.  SUPER SMART, saved my ASS so many times from a total disaster of the C1A COD to OTHER THINGS I CANNOT MENTION.  The man like many of our Officers was prior enlisted.  The man was a Machinest Mate working the BOILERS and now an Officer.  The man also knew EVERYTHING, everything from Logistics (Paperwork) to actual mechanics (Gas Turbines).  SUPER SMART, GOOD MAN.  He was always scolded by Chief Belt, "YOU NEED A HAIRCUT SIR".
- AZC Carr - work with the man a few times, very smart and good man
- AZCS Rorick - same thing as Carr, just a little KINDER :-)

# References

- I am keeping information that one day will be lost, archived, website gone, etc.

## Captain Donald George Robertshaw, USN

- Captain Donald George Robertshaw, USN, age 82, of McKinney, Texas, passed away February 17, 2017.  Donald was born June 13, 1934, in Cuyahoga Falls, Ohio, to George and Jeanie (Valance) Robertshaw.  He married Dianna Lynn Weber on November 17, 1979, in San Diego, California.  Donald proudly served in the United States Navy during the Korean War and the Vietnam Conflict.  He retired in 1988 after thirty-seven years of service.  Donald was a past VFW Commander in Knoxville, Pennsylvania as well as a volunteer at the VA Hospital Nursing Home in both Bath, New York and Bonham, Texas.  Donald received a military burial at sea.
