# Lance Corporal W. Mande USMC - [USS Long Beach (CGN-9)](https://en.wikipedia.org/wiki/USS_Long_Beach_(CGN-9))

## References

- [USS Constellation CV-64 Cruisebook 1980 - AIMD Jet Shop](https://www.navysite.de/cruisebooks/cv64-80/019.htm)
- [USS Long Beach CGN-9 Cruisebook 1980 - Marine Detachment](https://www.navysite.de/cruisebooks/cgn9-80/091.htm)

## Windjammer Club (E1 - E6) Enlisted Man Club, [Naval Air Station (NAS) North Island, Coronado, CA](https://en.wikipedia.org/wiki/Naval_Air_Station_North_Island)

- My Best Friend Steve Shoback and I were just leaving the Windjammer Club and we were walking back to our Ship [USS Constellation CV64](https://en.wikipedia.org/wiki/USS_Constellation_(CV-64))
- About 100 yards in front of us was a service member also leaving the Windjammer Club
- We both said, **"Hey let's mess with this guy"**
- We started yelling at the guy, he turned around and ran towards us with lightning speed, stopped and said, **"What did you say?"**
- We realized this was a Marine who we learned later on to be Mande
- This was no typical Marine, **Mande was Huge, 6'4", 230 lbs Solid Muscle**
- Steve did his fake crying scene, apologizing, saying he was drunk (when he wasn't), etc.  I was apologizing for Steve
- Mande said, **"Ok, you better watch your mouth"** and then turned around and walked away
- We gave Mande distance, about 200 yards and then again, Steve did the same thing
- Mande came charging and Steve again did the fake crying
- **HERE'S THE BEST PART**, Steve said in response, **"Mister Marine Corps Sir, ...", I WAS LOSING IT**, however, I went along with the act, apologizing for Steve
- Mande told Steve, **"Alright last time, next time I am kicking your ass"**
- We then shut our mouths and walked in front of Mande as our ship was further down the pier from Mande's ship, Long Beach
- We walked really fast and got to our ship while Mande was approaching his ship
- When we got to the After Brow, we started yelling again
- Here comes Mande, we ran on the After Brow, did the **"PERMISSION TO COME ABORD"** stuff, **ACTUALLY RAN ACROSS THE DECK** and then ran back to the Jet Shop
     - Jet Shop is the very back end of the ship, towards the Fantail
- We then ran to Sponson 3 - the last Starboard side outlet on the main deck and saw Mande being stopped at the After Brow
- Mande was not allowed on the Constellation and was to told to go back to the Long Beach
- As Mande was walking back when he was parallel to us on Sponson 3, we started yelling at him

![Sponson 3](cv64Sponson3.png)

- Tie Down Rope 4" diameter rope - ties the Ship to the Pier
- This is an image of forward tiedowns

![Tie Down](tiedown.jpg)

- The tiedown Mande was climbing went to Sponson 3 on the rear of the ship
- Look closely in the following image, the last rope is going to Sponson 3

![Tie Down Ropes](tiedownRopes.png)

## Superman Mande

- Next thing we saw was **INCREDIBLE**
- Mande ran to the last tie down on the ship tied to the pier
- Mande started climbing the rope, **ARMS ONLY** upwards towards Sponson 3
- The Junior Officer of the Deck (JOOD - E7) was yelling at Mande, had the Master At Arms (MAA) called, and the Junior Officer of the Watch (JOOW - E5) running on to the pier
- EVERYONE was yelling at Mande, go back to the pier get off the rope
- When Steve and I saw this **SUPERMAN** coming up the rope, we took off, knowing the MAAs and others would be looking for us
- We ran back on the 2nd Deck, because we knew they would be expecting us on the Main Deck, to our Berthing 01-39-0L
- We stripped into our night gear, jumped into our Racks and faked sleeping
- About 5 minutes later, **FLASHLIGHTS IN OUR FACE**, from MAA saying, **"Alright Assholes, we know who you are, the Officer of the Deck (OOD - 05) wants to talk to you"**
- We were denying what they accussed of us and faking they woke us up, they didn't want hear it

## OH SHIT MOMENT

- We put on our Dungarees approached to the Quarterdeck, took one step through the doorway and there he was, **MANDE**
- Mande was breathing crazy in and out (huffing and puffing), like a PITBULL DOG getting ready to kill something, with the stare like **LET ME KILL THEM**
- Mande was standing between a USN Commander (O5) and a USMC (E8)
- The E8 yelled at us, **"What did you do to this Marine?"**
- Yelling was back and forth and finally the O5 said, **"Alright, enough"**
- The O5 said, **"You two, go back to berthing and if I ever see you again, I will send you to Captain's Mast"**
- The O5 then said to Mande, **"Go back to your ship"**

## Christmas 1980

- Steve then said, **"Hey Bobby, let's send Mande a Christmas Card"**
- Steve wrote, **"Merry Christmas Mande, your buds from the Connie" and we both signed the Card**

## Windjammer Club, Shore Patrol

- Steve's enlistment was over, honorably discharged from the Navy
- I was in the USN for another 6 months
- I had Shore Patrol duty at the Windjammer one night
- I went to the HEAD (Bathroom), while washing my hands, I saw in the mirror a HUGE Guy walk up next to me, started washing his hands
- **YES, IT WAS MANDE**
- I tried to downplay, acting I didn't see or know who he was and then suddenly, I saw an **EVIL SMILE** on Mande's face looking **DIRECTLY** at me in the mirror
- **OH NO, I am DEAD, start saying your prayers**
- Mande said, 

```
I remember you Estey and I remember your friend Shoback
I have nothing against you, however, tell Shoback, if I ever see him again, I WILL KILL HIM
BTW, thank you for the Christmas Card
```
