# Navy AZ "A" School - Meridian, MS - September - November 77

I went to AZ "A" School, the Class was 50/50 Sailors / Marines, I remember my instructors:  AZ1 Gamble and SSGT Kunes, both were excellent role models and mentors.

The Navy and Marines PTed separately, everyone was great and most of all we were good friends.  Every Saturday we would have a sporting event and every time Navy would beat Marines.  We had two football games and Navy would clobber the Marines, 49 - 7 and 28 - 0.  Basketball a little closer but the Marines would always lose.  Sorry, just telling the truth.

The Barracks had a Common Open area with 4 rooms with 3 racks each.  My roommates were Watts and Pressley and we were in the same class, so we were in the same room the entire time.  The last day for the entire class, none of us could sleep because some guy was making all kinds of noise, I walked out and asked nicely for him to tone it down.  He got up and slapped me, I punched him in the mouth, he went down and I continued to beat the crap out of him, both arms pounding his face.  Finally, Watts pulled me off and the Watch showed up, seeing this Clown all beat up.  Then the Chief shows up and almost had me charged, heard the story and told all of us, to go back to bed.

Six months later, I was at Naval Station North Island, CA and I saw the same guy at the pool hall, building 614.  He was with 4 friends and I was alone and he started talking shit.  I told his friends the story and then told him, "Do you want me to kick your ass again".  His so called friends walked away from him.
