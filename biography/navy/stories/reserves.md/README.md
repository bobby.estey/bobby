# US Navy Reserves - Funny Stories

- These are funny stories while I was in the US Navy Reserves 
- The USN Reserves was a great get away with your buddies to party the weekends
- The best part of my service in the USN and USNR was we still lived the life of NON-POLITICAL correctness **BULLSHIT**
- **HAZZING WAS ENCOURAGED, DRINKING WAS EXPECTED, SAILORS WERE TO BE RASCALS**

- My service in the US Navy was:
     - 77-81 Active Duty
     - 82-88 Reserves
          - NSC Oakland, Denver, CO
          - HSL-84, NASNI
     - Why I left the USN
          - Received my Bachelors Degree (1985)
          - Applied for a Commission
          - Approved by Commanding Officer of HSL-84, Former AIMD Department Head USS Constellation, Master Chief USN, etc
          - Major Cutbacks - Congress removed the Enlisted to Officer Program

- While in the Reserves, my residence was Mesa, AZ and I would fly once a month on a C9

- The route would be either:
     -  NASNI (start) -> Las Vegas, NV -> Phoenix, AZ -> Davis Monthan AFB, AZ -> NASNI (end)
     -  NASNI (start) -> Davis Monthan AFB, AZ-> Phoenix, AZ -> Las Vegas, NV-> NASNI (end)
     
## Links / Acronyms

|Links / Acronym|Description|
|--|--|
|NAS North Island|[NASNI](https://cnrsw.cnic.navy.mil/Installations/NAVBASE-Coronado/About/Installations/Naval-Air-Station-North-Island/) - Coronado, CA|
|46|Petty Officers (46er E4 - E6) Club|
|C9|[US Navy Cargo C-9](https://upload.wikimedia.org/wikipedia/commons/0/03/US_Navy_030819-N-6501M-017_A_C-9B_Skytrain_II_from_the_Conquistadors_of_Fleet_Logistics_Squadron_Fifty_Seven_%28VR-57%29_flies_over_the_Pacific_Ocean.jpg)|
|HSL-84|[HSL-84](https://nara.getarchive.net/media/crew-members-service-a-helicopter-light-anti-submarine-squadron-84-hsl-84-sh-ed386d?zoom=true)|
|Davis Monthan AFB|[Davis Monthan AFB](https://www.dm.af.mil/) - Tucson, AZ|

## Mark St_d_l - Super Great Guy - an American Hero

#### In Flight Alcohol

- My first flight on the C9, the Tucson, AZ group was picked up previously
- Mark was from Tucson and he was my indoctrination guy on how things worked
- Everyone from Phoenix boarded the plane, I sat next to Mark, plane takes off
- Mark opened up his brief case and surprised me, the thing was full of **Little Liquor Bottles**
- Mark asked me, "What do you want?".  The Officers and Enlisted in eye site would look at Mark
- Mark would ask them, "Tell me what you want".  Mark would give the bottle to them, again, even Officers
- Flight Crew only had Sodas, so Sailors were making Mixed Drinks, **SALUTE**
- Mark was not funded but did this every flight with his own money
- Mark only had the bottles on the incoming flight to NASNI

#### Beer Delivered with a Bicycle

- Every flight back from Reserves we would go to the 46 Club across the street from the NASNI Air Terminal
- We would wait for the C9 to arrive and then dash to the Air Terminal
- One weekend the club was closed, they were renovating the club and we were so depressed, **WHAT NO DRINKS?**
- Mark saved the day.  Mark saw bicycles and they were locked up, walked into the shop and paid some guy $20 to unlock the bike
- Mark got on the bike, rode to **Touch N Go** a convience store on the base about a mile away and returned with two cases of beer
- The Air Terminal alcohol was not allowed so we were on the side of the building, drinking up, even our Senior Chief (E8) was drinking up

#### Corrosion Control Officer

- Mark was my informative on the Enlisted to Officer program and how to apply
- Mark had a Master's Degree in Electronics (Very Smart Guy) and also applied for the Officer Program
- Mark also told me about an event that happened that day
- Mark said an Officer told him to washdown one of the Helicopters
- Mark told me, "I asked the Officer what he did on the outside"
- Officer, "Corrosion Control"
- Mark said, "What a joke, ..."

## Popcorn -  SO HILARIOUS

- I finished with Reserve weekend, getting ready to fly back to Arizona and was at the NASNI Air Terminal
- I was hungry so I went to the Vending Machine and purchased Microwave Popcorn
- I put the Popcorn in the Microwave and just put the time to 10 minutes, because I would just watch and wait for the last Kernel to pop and then open the door
- After about 30 seconds, I had to go to the HEAD.  I was thinking I will be back just in time
- While in the HEAD, **I TOTALLY FORGOT ABOUT THE POPCORN**
- When I exited the HEAD, the entire Air Terminal, a very small building had 3 feet of smoke from the ceiling and down
- A Petty Officer First Class, was holding a **Smoldering Popcorn Bag**, still emitting SMOKE like crazy asked
- **WHO LEFT THE POPCORN in the Microwave**
- I cowardlessly walked directly to a chair, sat down and said nothing

## Senior Chief

- HSL-84 had a Senior Chief that had many slogans
- My favorite that I remember, "Either Play Ball or get the Bat shoved up your Ass"

## Robert (forgot Lastname) - Didn't Salute and Disrespectful

- Forgot his last name, a Petty Officer Third Class (E4)
- While at HSL-84 there was a checkpoint to check all who entered the Helicopter Squadrons
- Robert would stand duty and when an Officer approached, you were suppose to check ID and Salute
- Robert instead of Saluting, he would disrespectfully wave them through, **Like Get the Hell out of my Face, WHATEVER, etc**
- I was surprised, Officers would give the look at him like aren't you suppose to Salute me???, **LOL**
- We would tell Robert, you better stop doing that or you will be written up, Never happened

## 46 Club - Halloween

#### Wolfman Mark

- **YES, MARK AGAIN**
- We happened to be in the Reserves during Halloween
- Went to the 46 Club and they had a Halloween Contest
- Mark was dress up like a Wolfman and did his thing for the Contest, Mark received 3rd place
- Senior Chief didn't know at the time and said, "Is that St_d_l?", we said "YES"
- Senior Chief laughed and said, "I thought so, that's the kind of stuff he would do"

#### F... You Guy (FUG)

- During the entire night there was this Sailor FUG who was drinking like a Fish
- FUG was so **Obliterated** FUG would stand up, point at someone and yell at anyone, "F... YOU!!!", and this wasn't quick but very SLOW
- For Example:  Fffffuuuuuuuuuu..............  Youuuuuuuuuuuu!!!!!
- SO HILARIOUS, everyone was laughing at the CLOWN
- The entire night about every 5 minutes, FUG would stand up, point and yell, "F...  You!!!!!"
- FUG even pointed at the Disc Jockey (DJ) once in a while, "F... You!!!!!"
- Then others after being told by FUG, would **RETALIATE**, stand and say "FU" back.  This would happen back and forth
- Finally after about an HOUR, FUG walked up to the DJ with his beer and then accidently dropped his beer on the Sound Mixer
- Sparks are flying, shorted out everything, destroyed the sound system, the DJ could not continue, Music was gone for the night
- While were laughing the DJ was yelling at Dumbass, Dumbass started **SOBERING UP**, realizing, **OH SHIT, I GOT TO PAY FOR THIS, LOL**
- You had to be there, so hilarious, everyone was laughing at FUG

## Beer Machine / Cafeteria

- Those days the USN Base had Beer Machines, **THIS WAS GREAT**
- We would go to the 46 Club and stay until the Bar closed
- At the time our Barracks was next the 46 Club
- Before the Bar closed 2400, we would go to the Bartender, give the Bartender $10 and get a roll of Quarters
- Hit the Beer Machine, drink until sometimes 0400
- Wake up 0600, go to the Cafeteria, get Breakfast and then to the Squadron 0800

## Squadron Van Drive of Hell

- HSL-84 had a Squadron Van that was used for Personnel Transportation or move parts / engines between the Squadron and AIMD
- The Van's back seats were not screwed down to the floor, because the seat would be removed for large parts
- The Van that was suppose to pick us up to go to the Air Terminal didn't show up, so they rushed us out in the Squadron Van
- Airman Apprentice (AA) was driving the vehicle and we were in a rush, so he did his job
- He was over 30 MPH on the speed limit everywhere on the base
- While he was driving, we were sliding in all directions, forward, aft, left, right, **EVERYWHERE**

## USAF 2nd Lieutenants getting it on

- We just left Davis Monthan AFB flying on a C9 flight flying to NASNI for Reserve Duty
- Once at flight level, across from me were two USAF 2nd Lieutenants (O1) kissing each other
- I told them, "Knock It Off!!!"
- Both of them looked at me and the Male said, "You need to respect your superiors", as I am Enlisted
- I called the USN Lieutenant Commander (LCDR O4) a row in front of them and told him what was going on
- The LCDR got up and loudly said to them, "KNOCK IT OFF!!!"
- I got the evil stare from the O1s, I smile at them, almost laughing and they couldn't do anything about it, LOL

## Commander Quick - USN (O5) - Great Naval Aviator - Can Drink anyone under the table

- First of all, **WHAT A NAME, COMMANDER QUICK** and SAILORS can drink, Commander Quick was the **King**
- The Squadron I was attached to was HSL-84 and we just finished a Change Of Command, so there was a celebration in a field at NASNI
- There were several large coolers with Beers, everyone was doing their thing and the Enlisted were have a good time while at the same time drinking the **FREE BEERS**
- Then Commander Quick USN (O5) shows up, a Naval Aviator and there were 5 of us Enlisted guys next to a cooler, **NO ONE ELSE**
- Commander Quick was telling us stories of him in Vietnam which were (Vulgar, Hilarious, Obscene, Derogatory) and he was VOCAL
- Commander Quick would tell a quick story about his missions in Vietnam, pound a Beer and once anyone of the Enlisted asked for a Beer
     - **"Quick was QUICK"**, he would always say, **"Grab me another"**
- This was like 5 Enlisted against 1 Officer on the Beer drinking, Commander Quick was amazing
- You could see the Officers Wives looking at him, like he was disgusting and he would would look at them and say something that would put them in their place
- Everyone one of us Enlisted guys was thinking, **COMMANDER QUICK WAS THE BOSS**
- Then other Enlisted guys started showing up with us, asking how we are doing and we would ask Commander Quick to please continue with his stories
- NEXT thing you know, we have a crowd, around Commander Quick
- Then one of the Junior Officers wives complained about Commander Quick to the exiting Commanding Officer (O6) who became an Admiral (O7) later
- We heard our exiting CO say to her, **"THAT MAN SAVED MY LIFE IN VIETNAM, I OWE MY LIFE TO THAT MAN"**, then the CO turned his back on her with disrespect
- YES.  **COMMANDER QUICK ROXS**, then again he will never make USN Captain, LOL

## US Navy Captain (O6) Rips US Air Force Major (O4)

- Location - Davis Monthan AFB
- Early Morning, approximately 0400

#### Stayover

- During the time all flights into Tucson during certain hours were forbidden
- We arrived late into Davis Monthan and were required to sleep over in the Operations Center (I am guessing what this place was called)
- The Operations Center (OC) was a building probably next to the control tower and the main office was enclosed by glass, e.g. Fish Bowl
- USAF Major (O4) was in charge of the OC 
- We were told to just relax outside the Fish Bowl in the hallways, so we were just standing, resting, sleeping, waiting hours until the flight was allowed to continue to Phoenix and Vegas

#### Prepare to Leave

- Approximately 0400, the USAF Major (O4) informed his Airman First Class (E3) to contact our superior officer a USN Captain (O6)
- The order was to prepare your men to board the aircraft at 0500 and depart to Phoenix and Vegas
- The E3 walked out of the Fish Bowl and most of us were awake, **I WAS**
- The E3 asked us, "Who is your superior officer?"
- We pointed to the Captain, who was at the time sleeping, he was out
- The E3 walked up to the Captain and instead of tapping the Captain's shoulder, the E3 kicked his feet, LOL

```
WHAT WE WERE THINKING:

The E3 thinks a USN Captain (06) is the same rank as a USAF Captain (O3), which is not true, USN Captain = USAF Colonel

The E3 thinks his Officer a USAF Major (O4) out ranks the USN Captain (O6)

BIG MISTAKE
```

- The USN Captain, immediately jumped up and said, "Where is your Officer?", the Captain was **Pissed Off**
- The E3 pointed and said, "He is inside the office over there"
- The Captain yelled, **"STAY HERE"**, walked quickly to the Fish Bowl, opened the door, slammed the door and you could not hear anything, **SOUND PROOF**
- All we could see was a one way conversation
- The Captain was yelling and the Major was just receiving
- The E3 asked us and said, "Did I do something wrong?"
- We explained **RANKS** and told him, **HE WAS SCREWED**, LOL
- Then the Captain leaves the room and told us, "Board the Aircraft"
- The Captain just looked at the E3 and walked away
