# Collision - 1980 June 25 (Wednesday) - Indian Ocean

USS CONSTELLATION collides with a Bangladesh merchant ship in the Arabian Sea. The US Navy says there was minor damage to both ships but no injuries.

![Collision](./collision.png)

## Connie, freighter collide - SUBIC BAY NB, Philippines - Article Dated:  1980 June 26 (Thursday)

The U.S. aircraft carrier [Constellation](https://en.wikipedia.org/wiki/USS_Constellation_(CV-64)) collided with a Bangladesh freighter in an accident that cut a 15-foot gash in the carrier early Wednesday, according to a Navy spokesman.<br>
He said there were no injuries among the crewmen of the 80,000 ton 7th Fleet carrier, which has been part of the Indian Ocean task force.<br>
The gash was made on the after port side of the Constellation about 15 feet above the water line, he said.<br>
There were no fires and water was not taken in, he said.<br>
The Constellation is "fully mission capable" and "has resumed normal flight operations", he added.<br>
The freighter was identified as the [Banglar Joy](./banglarJoy.jpg).  The extend of its damage was not reported and the spokesman said it was being escorted by the U.S. frigate Bagley to an undisclosed port.<br>
Details of the accident and its location were also withheld.<br>
The Constellation received damage to its portside catwalks and superstructure as well as in the forward portion of the protruding deck, he said.<br>
Temporary repairs were expected be completed sometime Friday.<br>
The accident occurred shortly after midnight Wednesday while the Constellation was steaming along the oiler [Passumpsic](https://en.wikipedia.org/wiki/USS_Passumpsic) during underway replenishment, according to the spokesman.

## Damage

- Port Side Illuminator [AN/SPG-55](https://en.wikipedia.org/wiki/AN/SPG-55)

## Youtube

@WardCarroll you are correct most Commanding Officers (CO) are relieved of duty after a Collision.  1980 June 26 - USS Constellation CV64 was in a Collision with a Bangladesh freighter (Banglar Joy) that cut a 15-foot gash port side removing the Port Side Illuminator AN/SPG-55 which fell on the Banglar Joy.  The Constellation's CO Captain Edney was not relieved and saved the ship, by taking control of the HELM, turning immediately HARD PORT and ordered FLANK SPEED.  This manuever, turned Constellation which would of been T-BONED from a major disaster.  The CO of the Tanker was relieved.  Captain Edney eventually made the Rank of Admiral O10 and Vice Chief of Naval Operations.  The Greatest CO I was fortunate to serve.

## References

- [Banglar Joy](https://app.shipvault.com/ships/34316)
- [19790405 - Ranger Collision](https://www.reddit.com/r/interestingasfuck/comments/qng2iw/bow_damage_on_the_uss_ranger_cv61_in_the_straight/)
- [19800626 - Constellation Collision](README.md)
- [19800729 - Midway Collision](https://midwaycurrents.org/summer-2021/collision-at-sea/)
