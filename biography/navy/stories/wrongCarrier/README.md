# Wrong Carrier - Tuesday 27 May 1980

## Connie Gram - July 1980 - Lost Dog visits Connie / Nice trap...wrong ship

- Story by JOSN John Haughey

#### Photo Caption

The **VF-143 'Pukin Dog' Tomcat** takes a rest from its wayward wanders around the Indian Ocean on the Connie's flightdeck.  This story has a happy ending as the **'lost dog'** was catapulted back to its home, the USS Eisenhower.

#### Page 30

The crew of the USS Constellation has participated in many missions of mercy in the past; however, it is doubtful that the Connie, or any other aircraft carrier, has ever sheltered a lost dog.  Especially not while deployed in the Indian Ocean.

Nevertheless, the Connie was host to a most unexpected visitor late Tuesday afternoon, May 27, when a wayward Tomcat, of the **VF-143 Pukin Dogs**, embarked in the nearby USS Eisenhower (CVN-69), dropped in for a surprise trap.

More surprised than anyone in the Connie were the two dislocated 'Ike' airmen.

The stray F-14 had been on the return leg of a routine flight operation with other Pukin' Dog aircraft when, apparently, someone got his wires crossed.

"We set up, did a pass over the ship and then got the O.K.", said [Lieutenant Commander John Carter](lcdrCarter.png), the aircraft commander.

"Our lead brought us in...we weren't even looking", [Lieutenant Junior Grade Shephard](ltjgShephard.png), the co-pilot, tried to explain.

"He led us in, then took a wave off.  We trapped and he left", laughed LCDR Carter alone with his amused audience on the Connie's flightdeck.

"We were happy with the trap...it was a good one", snickered Lieutenant Shephard.

Constellation's Assistant Air Boss, Commander Don Herman, never lets a good trap go unnoticed.

Shortly after the Tomcat-flying duo had landed **Pukin' Dog 105**, his unfamiliar voice barked over their radio, "Nice trap...welcome to the Constellation!"

The stunned aviators glanced about them, and sure enough, they had landed on the wrong ship.

Connie's flightdeck crews immediately seized the opportunity to show their hospitality and were soon taxiing the aircraft to the aft section of the flightdeck.

#### Page 31

While LCDR Carter and LTJG Shephard were escorted to the Bridge to speak with Constellation's Commanding Officer, Captain Bud Edney, Connie crewmen were busy redecorating their guests' jet.

By the time the pilots returned, every squadron embarked in the Connie as well as many ship's company divisions, had left their mark, emblem, motto, symbol, initials and official designation imprinted on the once sleek gray Tomcat.  That resembled a victim of Halloween pranksters, having been transformed into an in-depth display of Constellation graffiti.

LTJG Shepherd solemnly stared at his aircraft and mumbled dryly, **"It's nice to see some traditions never die".**

LCDR Carter could only welcome the approaching darkness saying, "Well, I'm really looking forward to the night trap on the 'Ike' now.

Finally, the Air Boss ordered the flightdeck cleared of nonessential personnel , and flightdeck crews readied to launch the visitors.

"Lets send this lost dog home", commanded the Air Boss.  With a blast of steam accompanied with the screech of grating steel, the lost dog was catapulted and homeward bound.

## Navy Times - Navy Jet Lands On Wrong Ship; 'Ransom' Is Paid

NORFOLK, VA - A Navy jet mistakenly landed on the wrong U.S. carrier in the Indian Ocean and was taken "hostage" by the crew until the plane's home carrier paid a ransom - all of its newest movies - it was reported yesterday.

The Norfolk Ledger-Star quoted sources as saying the pilot and co-pilot of an F-14 jet fighter mistook the conventional carrier Constellation for the nuclear carrier Eisenhower and landed on the vessel last month.

The California-based Constellation wasted no time in taking full advantage of the mistake by the Norfolk based airmen, a source was quoted by the newspaper as saying.

"They seized the two fliers and held them hostage until the Ike paid a ransom", the source said.  "The Ike had to give the Connie all of its latest (movie) films".

## Denver Post - Landing's Fine...but It's Wrong Carrier!

NORFOLK, Va. (AP) - Fighter Squadron 143 catapulted its sleek F-14 Tomcat jets off the deck of the carrier Dwight D. Eisenhower for a typical patrol of the Persian Gulf and Arabian Sea.

The pilots flew a flawless day-light mission - the kind of routine flight that the fliers, from Oceana Naval Air Station in Virginia, have been conducting for 90 days over the Indian Ocean.

There was only one hitch.  As the wing commander ordered the planes down on the return flight, the squadron headed for the wrong aircraft carrier.  At the last moment, the commander realized the mistake and waved off the flight.  But one pilot didn't get the word.

He landed perfectly - on the desk of the USS Constellation.

The unidentified jet pilot and his radar-inercept officer still are red-faced several weeks after the event, according to word reaching Norfolk, base of the Eisenhower.

They apparently thought the 20 year-old oil-fired Constellation, with a big number "64" on its deck, was the 3-year old nuclear-powered Eisenhower, with an equally big "69" on its deck.

The California-based Constellation wasted no time taking full advantage of the Virginians mix-up, one unofficial Navy source said.

"They seized the two flyers and held them hostage until the Ike paid a ransom", the source said the Norfolk Ledger-Star.

"The Ike had to give the Connie all its latest (movie) films.  They they painted the plane with slogans and logos.

"They (the Eisenhower crew) had to paint the F-14 once it got back to its proper ship".

Navy officials at Norfolk said they couldn't comment on the incident because they hadn't heard of it.

But one official said such as incident isn't unknown.

"It has happened, particularly if you are returning to the ship in a radio silence situation.  You just come down... and work yourself in", he said.

Even the billowing smoke from the Constellation failed to trigger a signal that the pilot was landing on the wrong ship.  The Eisenhower had no smokestack because it is nuclear-powered.

Nobody could say how close the two carriers were when the incident occurred.

There is no official reprimand for such a landing.

## References

#### News Articles

![Connie Gram page 30](cg8007-30.jpg)

![Connie Gram page 31](cg8007-31.jpg)

![Navy Times](wrongCarrier1.jpg)

![Denver Post](wrongCarrier2.jpg)

#### Images

- [Pukin Dog 105 Crew](https://www.navysite.de/cruisebooks/cvn69-80/258.htm)
- [Pukin Dog from the past during the Vietnam war](cv64capturedbyCV31.jpg)
- [Constellation Air Boss](https://www.navysite.de/cruisebooks/cv64-80/033.htm)
