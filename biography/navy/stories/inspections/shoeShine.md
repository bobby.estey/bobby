# Inspections

- Once a week the Aircraft Intermediate Maintenance Department (AIMD) Jet Shop would have inspections by the IM-2 Division Officer USN LT and Division Chief Petty Officer (CPO)
- They would always check for haircuts, uniforms and shoes
- The shoes were not dress shoes but working shoes, called Boondockers

![Boondockers](usnBootsBoondockerChukka.png)

- Some Clown (I think it was Steve Young) figured out a way to pass the shoe inspection
- He got a Black Spray Paint can, went back to the fan tail
- Still wearing the shoes, sprayed his shoes and then went to inspection
- We told him, there is no way you are going to pass inspection
- During inspection, Chief Belt said, "It's black, has a shine, you pass"
- Next inspection, everyone was on the fantail spraying their shoes, LOL
- During the week the paint would start cracking, chipping off, etc.
- No problem, we would spray the boots every week just before inspection

# Boatswains Mates (BMs) - Pissed Off

- The Boatswains Mates (BMs) were in charge of the Fan Tail and hardly ever in the space except when the ship was leaving or arriving in port
- Once the BMs saw all the boot marks all over the deck, the BMs were pissed off, **SO HILARIOUS**

# You Need a Haircut

- Chief Belt always said many times, "You need a Haircut"
- The Biggest Violator?? **LTJG Lauseng** - always needed a Haircut, he also was the Coolest Officer
