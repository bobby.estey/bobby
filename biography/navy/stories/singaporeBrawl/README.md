# Singapore Brawl - 1980 August 05

# 5,500 US Sailors brawl in Singapore

SINGAPORE (UPI) - A pre-dawn brawl today invovling some 5,500 sailors ended the first night of shore leave in Singapore for the US 7th Fleet task force.<br><br>
A US Embassy official said the fighting broke out on Bugis Street, which is noted for its colorful nightlife, including plenty of alcohol and spectacularly dressed transvestites.<br><br>
The Official said the fighting by US Navy personnel did not involve any Singaporeans and was stopped by US military police who were on the scene.<br><br>
The sailors, many of whom were apprarently drunk after a night of carousing, started the brawl because of a misunderstanding among themselves, reports said.<br><br>
No one was seriously injured in the fight, and no damage was done to the property, the official said.  Further details of the incident were unavailable.<br><br>
The task force which includes the aircraft carrier USS Constellation, three frigates - the USS Bagley, USS Harold E Holt and USS Davidson - and the ammunition ship USS Flint, is commanded by Rear Admiral William Ramsey.<br><br>
The frigates are scheduled to leave Singapore Friday, while the Constellatipn and Flint leave next Tuesday.

![Singapore Brawl](singaporeBrawl.jpg)
