# General Alarm - Subic Bay, Philippines (September 1980)

There are many locations on the ship, where you can turn on the General Alarm.  I was in charge of the Security Augmentation Force (SAF) on [United States Ship (USS) Constellation CV64](https://gitlab.com/bobby.estey/wikibob/-/blob/master/README.md).  I always instructed my Watches, if you see anything wrong, sound the General Alarm.  Was about 0200 and one of my watches did such a thing, LOL.  

When the Officer of the Deck (OOD) heard the alarm, he and his group were running all over the place wondering what is going on, are we under attack.  

Then finally, I get over to Sponson 3 (Inboard Quarterdeck) and see my poor guy, getting chewed out.  What he did was wondering how to set the alarm.  So he moved the switch, (WHICH HAS A LOCK, that you have to push down, to silence).  He was so in shock, he could not turn off the alarm, until others showed up.  NO WORRIES, simple mistake.

## General Alarm Links

- [What CV64 Had](https://www.policeinterceptor.com/sounds/gqold5.wav)

- [OLD WW2 General Alarm](https://www.google.com/search?q=general+quarters+alarm+%22ww2%22&client=ubuntu-chr&sca_esv=578b7ad8c3c168d5&sca_upv=1&biw=1711&bih=838&tbm=vid&sxsrf=ADLYWIJxpp6FynqfG2TOWJvqUqKFAtuKBQ%3A1726811656651&ei=CA7tZpW7J5Crur8P1-3ioQo&ved=0ahUKEwiV343b6tCIAxWQle4BHde2OKQQ4dUDCA0&uact=5&oq=general+quarters+alarm+%22ww2%22&gs_lp=Eg1nd3Mtd2l6LXZpZGVvIhxnZW5lcmFsIHF1YXJ0ZXJzIGFsYXJtICJ3dzIiMgQQABgeMgsQABiABBiGAxiKBTILEAAYgAQYhgMYigUyCxAAGIAEGIYDGIoFMggQABiABBiiBDIIEAAYgAQYogRIiyhQwQ1Y8yVwAHgAkAEAmAFVoAHzAaoBATO4AQPIAQD4AQGYAgOgAvwBwgIFEAAYgATCAgYQABgWGB6YAwCIBgGSBwEzoAekCw&sclient=gws-wiz-video#fpstate=ive&vld=cid:25ebb8bf,vid:eqqXvPOEg3E,st:0)

- [General Alarm](https://www.google.com/search?q=general+quarters+alarm&oq=general+quar&gs_lcrp=EgZjaHJvbWUqBwgCEAAYgAQyCggAEAAY4wIYgAQyEAgBEC4YrwEYxwEYgAQYjgUyBwgCEAAYgAQyBggDEEUYOTIHCAQQABiABDIHCAUQABiABDIHCAYQABiABDIHCAcQABiABDIHCAgQABiABDIHCAkQABiABNIBCDQ1NDFqMGo3qAIAsAIA&client=ubuntu-chr&sourceid=chrome&ie=UTF-8#fpstate=ive&vld=cid:0b76edc8,vid:Dzt6uTRT_Y0,st:0)

- [Modern General Alarm](https://www.google.com/search?q=general+quarters+alarm&oq=general+quar&gs_lcrp=EgZjaHJvbWUqBwgCEAAYgAQyCggAEAAY4wIYgAQyEAgBEC4YrwEYxwEYgAQYjgUyBwgCEAAYgAQyBggDEEUYOTIHCAQQABiABDIHCAUQABiABDIHCAYQABiABDIHCAcQABiABDIHCAgQABiABDIHCAkQABiABNIBCDQ1NDFqMGo3qAIAsAIA&client=ubuntu-chr&sourceid=chrome&ie=UTF-8#fpstate=ive&vld=cid:2cd0bea1,vid:TyVzeh7rHlM,st:0)

- [Modern General Alarm 2](https://www.google.com/search?q=general+quarters+alarm&oq=general+quar&gs_lcrp=EgZjaHJvbWUqBwgCEAAYgAQyCggAEAAY4wIYgAQyEAgBEC4YrwEYxwEYgAQYjgUyBwgCEAAYgAQyBggDEEUYOTIHCAQQABiABDIHCAUQABiABDIHCAYQABiABDIHCAcQABiABDIHCAgQABiABDIHCAkQABiABNIBCDQ1NDFqMGo3qAIAsAIA&client=ubuntu-chr&sourceid=chrome&ie=UTF-8#fpstate=ive&vld=cid:0a4b0e67,vid:ZSYtFpbhfsE,st:0)
