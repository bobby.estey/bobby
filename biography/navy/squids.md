# SQUIDS - Cruisebooks

- Term given to a US Navy Sailor

## Memorial to all my Brothers

- [Jesus Albert Herrera Junior](./herrera/jesusHerrera.md)

|USS Constellation (CV64)

#### 1977 

- [SHOBACK - CV64 (1977)](https://www.navysite.de/cruisebooks/cv64-77/125.htm)
     - This was Steve's Division at the time V4, his picture is not there, however, he knows all these guys

#### 1978 - 1979

- [SHOBACK](https://www.navysite.de/cruisebooks/cv64-78/062.htm)
- [ESTEY](https://www.navysite.de/cruisebooks/cv64-78/060.htm)
     - my back looking at a VIDS/MAF board
- [ESTEY, GRECO](https://www.navysite.de/cruisebooks/cv64-78/067.htm)
     - new arrivals, I was not a new arrival in 78-79, tried to not get my picture taken and then months later Senior Chief ordered me to get a photo
     
#### 1980

- [GRECO](https://www.navysite.de/cruisebooks/cv64-80/024.htm) 
- [SHOBACK, GUTJAHR, ESTEY](https://www.navysite.de/cruisebooks/cv64-80/019.htm)
     - THE GREATEST PHOTO OF ALL TIME - Yes, the Jet Shop, all others serious.  This was picture 1 of 3, pictures 2 and 3 were crazy|

## USS Enterprise (CVN65) 

- [SHAFTO (1978)](https://www.navysite.de/cruisebooks/cvn65-78/278.htm)

## USS Coral Sea (CV43) 

- [SHAFTO (1980)](https://www.navysite.de/cruisebooks/cv43-80/236.htm)

## USS Theodore Roosevelt (CVN71) 

- [BIG G (1999)](https://www.navysite.de/cruisebooks/cvn71-99/218.htm) 
- [CHIPPS (2001)](https://www.navysite.de/cruisebooks/cvn71-01/244.htm)
