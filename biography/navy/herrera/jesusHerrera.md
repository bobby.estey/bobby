# Jesus Albert Herrera Junior - RIP Brother (1957-06-29 - 2023-03-25)

# God Bless You Jesus Herrera

I am honored and privileged to have known Jesus.  If the world had more peaceful, loving individuals like Jesus the world would be a better place.

God Bless Our Brother, Jesus Albert Herrera Junior:

- https://www.navysite.de/cruisebooks/cv41-77/204.htm

![Jesus Herrera - USS Midway CV41](jesusHerreraCV41.png)

Jesus Albert Herrera Junior is now with Jesus the CHRIST. 

Jesus was a very kind, thoughtful and best of all, a LISTENER.  He always listened to people, let them give their thoughts / opinions and then would always respond with kindness, OR BEST OF ALL, change the subject.  Jesus and my Sister were best of friends would visit our home and visit with my Parents.  My Parents Loved Jesus and always told me, that he is a "WELL RAISED MAN".

I was informed December 2022 that Jesus was diagnosed with Cancer with life expectancy of 2 to 4 months to live.  I flew out to Denver, CO, February 2023.  I told Jesus, I wanted to talk to him, instead of a box, so I visited Jesus for the last time.

# His Future

I told Jesus this is how it's going to work:

-  You will Die, Resurrect and see a crowd at a Finish line, you will cross that Finish Line
-  Jesus the Christ will be the first to greet you crossing the line
-  Your Parents will be the next, greeting you
-  My Parents will be next, congratulating you
-  Then everyone else will be there
-  Finally, please do me a favor and PRAY for us on the Earth, that we will be with you and our God (Father, Son (Jesus) and the Holy Spirit)

# US Navy

Jesus was committed to the Navy Core Values:  Honor, Courage and Commitment.  Jesus served on two ships, which so happen to be Museums and his basement is a memorial to his two ships:

- [CV41 Midway](https://www.midway.org/)
- [DD951 Turner Joy](https://ussturnerjoy.org/)

# US Loyalty

**During our conversation, Jesus was very clear and bold with the following statement:**

**Jesus said, "My Heritage is Mexican, however, I am American.  I was proud to serve our Country".**

# Obituary

Jesus Albert Herrera Junior, age 65, of Northglenn passed away on Saturday, March 25, 2023

It is with heavy hearts that we announce the passing of Jesus Herrera.  He was a man of god, a beloved father, husband, brother, grandfather, uncle, and friend, Jesus passed away peacefully on March 25th, surrounded by his loved ones.

A Colorado native, Jesus lived a full life filled with love and laughter.  He had a deep devotion to his family, and throughout his life, he made many friends and touched the hearts of countless people.

Jesus relished his time in the United States Navy, where he earned the rank of Boatswain Mate Third Class Petty Officer.  During his time in the Navy, he served on the USS Midway and the USS Turner Joy, where he made many treasured, lifelong friends.  A master craftsman, he also took great pride in his work as an upholsterer, his chosen profession for most of his life.  He also made many friends while working in the roofing business in later years.

Jesus will be deeply missed by his entire family:  wife, Carrielyn, son and daughters, Kyle, Kristen, and Ashley, son-in-law, Mike, his grandchildren, Sienna, Cheyenne, Richie, Cyrus, and Zeppelin; his sisters and brothers, Dorene, Corrine, Annette, Nick, and David; his in-laws, Reed, Juan, Randall, Linda, Lisa, Kenneth, and Sherie; his father-in-law, Dean; his nephews and nieces, Brandy, Lance, Tiffany, Kayla, Dylan, and Christopher; and a number of great nephews and nieces. Jesus was preceded in death by his parents, Al and Jane, his mother-in-law, Maryfrances, and his grandson Remy Martin.

Jesus will be laid to rest on the afternoon of Monday, April 17, at Fort Logan National Cemetery, preceded by a memorial service at Olinger Highland Mortuary & Cemetery in Northglenn, where we'll be remembering and celebrating the life of this remarkable individual.

A visitation for Jesus will be held Monday, April 17, 2023 from 9:00 AM to 10:00 AM at Olinger Highland Mortuary & Cemetery, 10201 Grant St, Thornton, CO 80229. A funeral service will occur Monday, April 17, 2023 from 10:00 AM to 11:00 AM, 10201 Grant St, Thornton, CO 80229. A graveside service will occur Monday, April 17, 2023 from 1:30 PM to 2:00 PM at Fort Logan National Cemetery, 4400 W. Kenyon Ave., Denver, CO 80236. A reception will occur Monday, April 17, 2023 from 3:30 PM to 5:00 PM, 10201 Grant St, Thornton, CO 80229.

# Readings

|Verse|Title|
|--|--|
|1 Thessalonians 4:14-18|Jesus those who have fallen asleep|
|Revelation 21:|Wipe Tears Away|
|Psalm 34:18|Close to the brokenhearted|
|Psalm 147:3|Broken in Heart|
|Romans 14:8|We are the Lords|
|John 16:22|I will see you again|
|Proverbs 18:24|Closer than a Brother|
|Daniel||
|1 Peter 1:6-|Grieved by Trials|
|Joshua 1:9|Faith not Fear|
|Matthew 26:36–46 Mark 14:32–42 Luke 22:40–46|Let your will be done|
|Hebrews 11|Faith||
|Matthew 22:36-40|The Greatest Commandment|
|Ephesians 2 8-9|Saved through Faith|
|John 3:16-18|Whoever believes in him is not condemned|

# Quotes

- "If you have time to lean, you have time to clean".
- "If you are 15 minutes early, you are on time, if you are on time, you are late".

# Service

- [Service](https://view.oneroomstreaming.com/index.php?data=MTY4MTU5Njg5ODI0NjM4MyZvbmVyb29tLWFkbWluJmNvcHlfbGluaw==)
- [Charity Gayle - I Speak Jesus](https://www.youtube.com/watch?v=PcmqSfr1ENY)
