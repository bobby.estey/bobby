# United States Navy (19770701 - 19880811)

## ![](./icons/cv64AmericasFlagShip100x100.png) Constellation CV64 - America's Flagship - Also Known As:  Connie

### History

I was active duty Navy from November 1977 - June 1981 on America's Flagship, Constellation CV64.  I was ship's company in the Aviation Intermediate Maintenance Department (AIMD) initially IM-1 Division Production Control and IM-2 Division Jet Shop.  I made two cruises (1978 - 1979) and 1980. President Ronald Reagan named Constellation America's Flagship, 20 August 1981. Constellation is named after the formation of stars of the US Flag. There were 3 Constellations that served the United States:

- [1797 Frigate](https://en.wikipedia.org/wiki/USS_Constellation_(1797)), the first US Navy Ship, the first to go to battle and first win
   - [1797 Constellation Plaque](./stories/1797.jpg)
       - The 1797 Constellation (first ship in the Navy's registry) was a Frigate style ship meaning that the ship had two decks of cannons
       - Many have made the same mistake that the [Sloop](https://en.wikipedia.org/wiki/Sloop) in [Baltimore, Maryland](https://en.wikipedia.org/wiki/Baltimore) is the same ship, with only one deck of cannons
       - The 1797 ship was disassembled in the 1800's.  Even the plaque from 1972 makes the same mistake
       
###### Plaque Reads as Follows:

        - The world's oldest ship continuously afloat.  Built at Major David Stodder's Shipyard on Harris Creek Baltimore.  
        - She was launched September 7, 1797 under Command of Captain Thomas Truxtun.  She served in every declared war from 1797 until she was decommisioned on February 15, 1955.  
        - Constellation dock permanent berth of the Frigate Constellation was formerly pier 1 and the site of historic Darby Lux wharf from which privateers under the command of Joshua Barney and David Porter sailed during the American Revolution and the War of 1812.
        - Presented By:  THE COMMODORE JOSHUA BARNEY CHAPTER, DAUGHTERS OF THE AMERICAN REVOLUTION, SEPTEMBER 7, 1972
    
- [1854 Sloop](https://historicships.org/explore/uss-constellation), which is currently in Baltimore, Maryland
- [1961 Carrier (CV64)](./stories/1978.jpg), my ship.  
    - You can see the call sign for Constellation NNUL the moto, "Spirit of the Old Pride of the New" and the seal.  
    - Constellation was also unique as the only ship with red, white and blue lights illuminating the ships number, this image is from the superstructure.  
    - [Gulf of Tonkin Incident](https://en.wikipedia.org/wiki/Gulf_of_Tonkin_incident)
    - [First aviator and Navy Prisoner of War was from Constellation](https://en.wikipedia.org/wiki/Everett_Alvarez_Jr.)
    - [First ace of the Vietnam War was from Constellation](https://en.wikipedia.org/wiki/Duke_Cunningham)
    - First strikes on Iraq - Part 2
    - Constellation was decommissioned 07 August 2003 and was scrapped in Brownsville, Texas.

![Constellation CV64](../cv64.png)

My time in the USN was great and then about half way through my 1st deployment - Western Pacific (WestPac), was bad, very bad.  Many people just do not know.  We were the only US presence in the Indian Ocean for decades.  There were only 4 ships.  Now they have tasks forces.  Bottom Line:  Anytime you see aircraft carriers, those are Naval Aviators (either USN or USMC):
- Carrier - [Constellation CV64](https://en.wikipedia.org/wiki/USS_Constellation_(CV-64))
- Tanker - [Kansas City AOR3](https://en.wikipedia.org/wiki/USS_Kansas_City_(AOR-3))
- Cruiser - [Sterett CG31](https://en.wikipedia.org/wiki/USS_Sterett_(CG-31))
- Destroyer - [Waddell DDG24](https://en.wikipedia.org/wiki/USS_Waddell)

# Videos

- [Connie's History - Best Video on Constellation](https://www.youtube.com/watch?v=B76XY3L8t_I)
     - pause the video and look at the words, this person who posted this video was great.  He was about 10 years before me and the greatest video I have seen on the Constellation
     
- [NAVY USS CONSTELLATION green light](https://youtu.be/VXYLGCHk4TI?t=225)
     - everyone has seen (USS CONSTELLATION CV64), that's all the media could record during Iraq 2
     - main reason, the US Navy was trying to extend CV64 Service with publicity and the ship was in excellent condition
     - CV64 had first strikes on Vietnam and Iraq 2, green light
     
- [WestPac 99 / USS Constellation Tiger Cruise](https://www.youtube.com/watch?v=vCz4dAogJPE)
     - this is called the Tiger Cruise, 
     - we use to take our family members from Hawaii back to San Diego after our WestPac (Western Pacific) cruise
     - SO WORTH THE WATCH, the recording was totally cool

- [Ends Final Voyage - Chopping Up](https://www.youtube.com/watch?v=8qUqEXU-6L4)
     - one of many memories that was super sad, Connie was getting ready to be chopped up
     - Connie was towed from [Bremerton, WA](https://en.wikipedia.org/wiki/Bremerton,_Washington), around [Cape Horn](https://en.wikipedia.org/wiki/Cape_Horn) to [Brownsville, Texas](https://en.wikipedia.org/wiki/Brownsville,_Texas), half way across the world
     - speed up to [1:35](https://youtu.be/8qUqEXU-6L4?t=90) if you wish, I wouldn't

- [USS Constellation CV64 - Bremerton, WA](https://rumble.com/v5f4525-uss-constellation-cv64-bremerton-wa.html)

```
AND OF COURSE, THIS IS WHY YOUTUBE SUX AND EVERYONE SHOULD MOVE TO RUMBLE. YOUTUBE removed my original post years ago. I served on CV64 (77-81) and these clowns are DISRESPECTING my ship. SO THIS IS WHAT I AM GOING TO DO. COPY this video and POST ON RUMBLE and CONTROL, what the COMMUNIST BLEED HEART LIBERALS are doing to this country. MAGA - BTW, if you don't know your history, CV63 was a great ship, don't know anything about CV62. CV61, was the BIGGEST POS EVER. EVERYONE in the Pacific, HATED THE POS RANGER. Never relieved anyone on time in HISTORY, ALWAYS A FAILURE. I am so glad CV61 was scrapped.
```

## Connie Gram / Stories

- [Connie Gram - News before the Internet](./connieGram)

- [Brand New - departing Brooklyn Naval Ship yard for the first time](./stories/commissioned)
- [Fire - while being built - 50 workers were killed](./stories/fire)
- [Indian Ocean - 78/79](./io)
- [Collision - 25 June 1980](./stories/collision/collision.md)
- [History](https://www.youtube.com/watch?v=MNzVGCPS9L0)

###### Funny Stuff

- [Brawl in Singapore - I know the guy who started this](./stories/brawl.jpg)
- [Fishy Story](./stories/fishyStory.jpg)
- [Crossing the Line - before sensitivity](https://www.youtube.com/watch?v=s4XLnkq4Wec)
- [Truck Launch - shot a truck off into the Ocean](./stories/alertTruck)
- [Wally - Huge smiley on the superstructure](./stories/wally)
- [Wrong Carrier - July 1980 - F14 from USS Eisenhower CVN-69 landed on Constellation CV-64 by mistake](./stories/wrongCarrier)
- [General Alarm - September 1980](./stories/generalAlarm)

### Military History

Finally, I like history:

- US ARMY - was always the leader in Aviation
- US NAVY - is always the leader in Space
     - first Astronaut - [LCDR Shepherd](https://en.wikipedia.org/wiki/Alan_Shepard) - USN
     - first American to Orbit - [John Glenn](https://en.wikipedia.org/wiki/John_Glenn) - USMC - Naval Aviator
     - first Man on the Moon - [Neil Armstrong](https://en.wikipedia.org/wiki/Neil_Armstrong) - USN
     - last accident with Shuttle - 4 of 7 were USN

<hr>

# MORE NAVY - other stories

- [US Navy Drill Team](navyDrillTeam.md)
- [US Navy Rifle Squads](https://www.youtube.com/watch?v=e92mZparox0)
- [Blue Angels - 6 Landing Simultaneously](https://youtu.be/h2WMwISL3Tw?t=280)
- [Blue Angels - Currently](https://youtu.be/Ynvoriv09Ks?t=340)
- [Dropping Off Heavily Greased Pigs](https://rumble.com/v2oy4l8-a-helicopter-from-uss-america-dropped-off-heavily-greased-pigs-on-uss-john-.html)
- [Navy Traditions - Swim Call, Beer, Rum](https://www.youtube.com/watch?v=CvoyYPVUibM)
