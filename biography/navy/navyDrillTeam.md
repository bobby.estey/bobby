# Navy Drill Team - Superior to All Services

- [#1 USN](https://www.youtube.com/watch?v=mJTPwv29QcI)
     - Mostly E2s and E3s
- [#2 USAF](https://www.youtube.com/watch?v=iD5gjbduPa0)
     - Senior Personnel - E5s through E7s
- [#3 USA](https://www.youtube.com/watch?v=TQrLOpfLegY)
- [#4 USMC](https://www.youtube.com/watch?v=i0sBXTQzVWE)
     -  Not even close, I saw NOTHING, NOTHING FROM THE USMC. I saw more from a Marine Detachment on my ship.
     
# WORLD CHAMPIONS - USN

[Oslo, Norway](https://www.youtube.com/watch?v=OgcGNDxuyoI)
