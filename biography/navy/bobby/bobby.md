# Bobby Estey - US Navy

|Image|Content|
|--|--|
|[VIDS/MAF Board](./images/vids_maf.jpg)|My back looking at the VIDS/MAF board|
|[WestPac 78-79](./images/westPac1978_1979.jpg)|I wasn't a new arrival, when they had picture day, didn't want to participate. Later on, I was ordered to get my picture|
|[WestPac 80](./images/westPac1980.jpg)|Funny picture, everyone on the entire ship were all serious.  We were clowns in AIMD Jet Shop|
|[AIMD Man of the Month](./images/AIMDMOMAward.jpg)|AIMD Man of the Month Award|
|[Constellation Decommissioned](./images/constellationDecommisioned.jpg)|Constellation Decommissioned|

# Awards

### Ribbons

<p align="left" width="25%">
    <img width="152px" height="44px" src="./awards/meritoriousUnitCommendationRibbon.jpg">
    <img width="152px" height="44px" src="./awards/goodConductRibbon.jpg">
    <img width="152px" height="44px" src="./awards/navyReserveMeritoriousServiceRibbon.jpg">
    <img width="152px" height="40px" src="./awards/navyExpeditionaryRibbon.jpg">
    <img width="152px" height="44px" src="./awards/seaServiceDeploymentRibbon.jpg">
</p>

### Medals

<p align="left" width="100%">
    <img width="10%" src="./awards/goodConductMedal.jpg" alt="Good Conduct Medal">
    <img width="10%" src="./awards/navyReserveMeritoriousServiceMedal.jpg" alt="Navy Reserve Meritorious Service Medal">
    <img width="10%" src="./awards/navyExpeditionaryMedal.jpg" alt="Navy Expeditionary Medal">
</p>

# Defend the Constitution of the United States

- I LOVE MY COUNTRY and anyone who disrespects my flag, my country and my GOD.  Everyone in the US MILITARY has SWORN the oath.  Here is a SNIPPET of the OATH and there is a LOT more:

- **Do solemnly swear that I will support and defend the Constitution of the United States against all enemies, foreign and domestic**

Here's a post I sent to an ARMY guy on YOUTUBE:

I was NAVY and we bleed the same RED, WHITE, BLUE.  You Volunteered and I did also.  I LOVE MY COUNTRY and the CONSTITUTION.  The United States Military is the greatest ever.  Not only are we superior, but also HUMBLE, CHARACTER, INTEGRITY, HONESTY and more.  I just checked, only 7% of the US Population have ever served.  We are the exception.  So there you go ARMY.  George Washington was the ONE MAN that made this country, and yet, many do not know what he did.  IT'S IN THERE FACE EVERY DAY.  He is on the $1 Dollar Bill.  OUR GOD and his SON (Jesus CHRIST) made all this possible.  WE CAN SEE, THEY CANNOT.  GO ARMY, AND FLY NAVY.  Bobby Estey.
