# USS Constellation CV-64 Honor Roll

## KIA

|Year|Service|Rank|Rank|Name / Link|
|--|--|--|--|--|
|1967|USN|O5|O5|[Laurent Norbert Dion](https://valor.militarytimes.com/hero/305952)|
|1967|USNR|02|O2|[Charles David Hom](https://dpaa-mil.sites.crmforce.mil/dpaaProfile?id=a0Jt00000001UeLEAU)|
|1967|USN|O3|04|[Charles Richard Lee](https://www.findagrave.com/memorial/99540693/charles-richard-lee)|
|1967|USNR|O1|01|[Patrick Lawrence Ness](https://dpaa-mil.sites.crmforce.mil/dpaaProfile?id=a0Jt000000vsbKQEAY)|
|1967|USNR|O2|O4|Dain Vanderlin Scott](https://dpaa-mil.sites.crmforce.mil/dpaaProfile?id=a0Jt0000000KYCqEAO)|
|1967|USN|O4|O4|[Thomas Walter Sitek](https://dpaa-mil.sites.crmforce.mil/dpaaProfile?id=a0Jt000000vsbKLEAY)|
|1967|USN|O2|O4|[J Forrest George Trembley](https://dpaa-mil.sites.crmforce.mil/dpaaProfile?id=a0Jt000000vjG31EAE)|

## MIA

|Year|Service|Rank POW|Rank|Name / Link|
|--|--|--|--|--|
|1967|USN|O5|O6|[Leo Twyman Profilet](http://www.veterantributes.org/TributeDetail.php?recordID=313)|
|1967|USN|O5|O9|[William Porter Lawrence](https://www.pownetwork.org/bios/l/l042.htm)|
|1967|USNR|O2|O6|[Wayne K Goodermote](http://veterantributes.org/TributeDetail.php?recordID=97)|
|1967|USN|O4|O6|[Leo Gregory Hyatt](http://veterantributes.org/TributeDetail.php?recordID=1411)|
|1967|USN|O4||[Jimmy Lee Buckley](https://dpaa-mil.sites.crmforce.mil/dpaaProfile?id=a0Jt000000y1gOUEAY)|
|1967|USNR|O4|O5|[William Morgan Hardman](http://www.veterantributes.org/TributeDetail.php?recordID=927)|
|1967|USN|O3|O5|[Robert James Flynn](https://en.wikipedia.org/wiki/Robert_J._Flynn)|
|1967|USN|O3|O6|[John Michael McGrath](http://www.veterantributes.org/TributeDetail.php?recordID=8)|
|1967|USNR|O2|O5|[James William Bailey](http://veterantributes.org/TributeDetail.php?recordID=1860)|

## Miscellaneous Links

- [Several Officers on same Mission](https://aviation-safety.net/wikibase/158631)
