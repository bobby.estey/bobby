# Skills / Experiences

- This is not to insult or brag.  I have learned so much from others in my life.  I have been fortunate to know so many people who are HEROES to me and we should always remember these people.  Without Human Beings and GOD, we are nothing.  I can continue, but let me be brief.  These are the various Skills and Experiences I have acquired throughout my life and I could have only performed / learned these by PEOPLE and GOD.

## Occupations

#### Parents

- Under Construction - my Parents taught me the following and I have continued that knowledge which I have passed on to my Daughter:
     - Construction, Electrical, Plumbing, Automotive, and a lot more

#### [Furr's Cafeteria](https://en.wikipedia.org/wiki/Furr%27s) - My Furr's Company (LOL)

- I was a dishwasher, worked there for a couple months.  The number one thing I learned about was SANITATION.  That place after work, we spent hours CLEANING and the Cleaning was so incredible, you could eat off the floors.  
- We had a Manager (I know his name well, however, will not disclose due to privacy and a security question), was Stern and Fair.  He was a true Manager, who knew what was going on everywhere.  I respected him so much, even though he was a VERY GRUFF MAN.

#### [Karl's Farm Dairy (KFD)](https://www.facebook.com/karlsfarmdairy/)

- First of all, I try and stay away from Social Media, so I don't have Facebook
- KFD was a farm located in [Eastlake, CO](https://en.wikipedia.org/wiki/Eastlake,_Colorado) and when I was employed, was in a Rural location, North East of [Northglenn, CO](https://en.wikipedia.org/wiki/Northglenn,_Colorado)
- KFD at the time had approximately 500 dairy cows and cows were milked every 12 hours.  There were two shifts, night (0000 - 0800) and day (1200 - 2000).  There was no overlap, so if anyone wanted to have the day off, the rule was, perform both shifts.  For example, if night shift wanted the day off, then day shift would work both shifts.  This was a hard life.
- Occupation - Cow Chaser (CC) - what is that?  The job is to go out to pens divided up with approximately 100 cows per pen.  The CC (me) would go out to pen 1 at the start of the shift, gather them up against the gate.  The milkers would go to the gate, once all the cows were gathered up, open up the gate and the cows would walk to the Milk House.  The CC would push the herd towards the Milk House and have 8 cows per side walked into the Milk House to be Milked.  When the entire side was finished milking, the cows would be released and the CC would move the next group of cows into the Milk House.  When the herd from the pen was finished, repeat the cycle with the other herds.
- Milk House was a facility at KFD, which had 8 milkers on two sides (port / starboard) a total of 16 milkers.

###### Funny Stories

- Riding Cows -
- Surprising Cows -
- Snowball Fights -

#### Robo Car Wash

###### Funny Stories

- Customers Not Reading Instructions
- Slap Fight -




#### Software Development

## Rankings

|Ranking|Description|
|--|--|
|3||


## Bottom Line

- GOD is #1, PERIOD, GOD has put us on this Planet with a purpose and we are to utilize that talent in our lives.
- Parents, I was fortunate to have my Parents, who did everything they could for me
- It is very unfortunate for others who never received the Love from their Parents.  Very Sad, that they didn't get the same benefits that I received.  Always Love your fellow Brothers and Sisters.  
