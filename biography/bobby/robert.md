# [Robert](https://en.wikipedia.org/wiki/Robert)

## Patriot

- [Make America Great Again](maga)

## Robert Types

- The name Robert is a traditional name in many languages, for example:
- Robert (Germanic)
- Roberto (Spanish)
- Роберт (Russian)

There are variants / nicknames of the name Robert and these nicknames also come with a stereotype of personalities.  This page describes the 4 most popular Robert nicknames

## Robert - while not a nickname have the most common traits as follows:
- Leadership
- Serious

## Rob - most Robs I know have these common traits:
- Feminine
- Follower
- Punk
- Pushed around

## Bob - most Bobs I know have these common traits:
- Fair and Balanced
- Great Guy
- Likeable by All

## Bobby - most Bobbys I know have these common traits:
- Active
- Adventurer
- Thrillseeker
- Willing to take the Risks
- and a lot of negatives that make people Scared :-)

#### There are ALMOST no Roberts, Robs or Bobs in anything with Comedy, Hockey, NASCAR or any other TOUGH GUY SPORT.  Only BOBBYs.
- Comedians (Bobby Slayton) - in your face Comedian that tells the Truth and the Greatest
- BASEBALL - Abreu, Bonds, Bradley, Crosby, Doerr, Jones, Livingston, Lowe, Murcer, Richardson, Scales, Thomson, Witt
- HOCKEY - Bauer, Clark, Hewitson, Hull, Orr
- NASCAR - Allison, Hamilton, Hillin, Isaac, Johns, Labonte, Unser
- Evil Knievel who went by BOBBY

## World Famous Roberts

|Lastname|Recognition|
|--|--|
|Bartlett|Explorer|
|Fulton|Engineer / Inventor|
|McClure|Explorer|
|Peary|Explorer|
|Scott|Explorer|
