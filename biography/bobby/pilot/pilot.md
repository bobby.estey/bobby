# Pilot

## License

- [Temporary Pilot License](temporaryPilotLicense.jpg)
- [Pilot Certificates](pilotCertificates.jpg)

## North Island Navy Flying Club - (Gone, so sad)

North Island Navy Flying Club was the greatest to me EVER. This is where I learned how to fly. I knew everyone there back during my service on the USS Constellation (77-81). I always wanted to fly and I had the greatest instructor ever.

FRANK HARDY. He was very popular with everyone and super kind, gentle man teaching flying. You have to be patient and kind with students, because flying is stressful. FRANK, I know you are in Heaven.

Everyone there were very kind. I still have my logbook and the first plane I ever flew was a C-152 - Aircraft ID 24918 on 06/26/1979 with Frank Hardy, we flew 1.3 hours on my first takeoff, lesson and landing.

To bad the school is gone, so sad to me.

## Stories

#### [Tiger Crash](tigerCrash.jpg)

- I was suppose to fly this plane the next day the Pilot crashed
- The Pilot forgot to turn on the fuel valve

#### Needles, CA (EED) - Learned about Density Altitude (DA) - 14 July 1981

- [Density Altitude](https://en.wikipedia.org/wiki/Density_altitude) - The altitude relative to standard atmospheric conditions
- I was flying a C-152 on my 3rd Cross Country (Student Solo) Flight.  My instructor warned me about Needles and Density Altitude.  I never understood or had experience with Density Altitude (DA), since all my Flight Training was in San Diego, CA
- When taking off in San Diego, DA is low and pulling on the yoke, the plane would climb immediately
- After landing at Needles, I had the logbook signed verifying my flight, I noticed other pilots waiting for something, not flying.  I was puzzled
- I was the only Pilot to takeoff, DA was probably 5000 feet, temperature 115F / EED Altitude 1000
- I started the takeoff going down the runway, pulled back on the yoke and nothing happened, keep going down the runway, pulling, pulling, pulling, NOTHING
- Dropped 10 degree flap, NOTHING, 20 degree, end of runway coming up and finally off the ground about 20 feet above the ground for about 0.5 miles
- The extreme winds 9 o'clock were hitting the fuselage and the [Vertical Speed Indicator](https://i.ebayimg.com/images/g/1dMAAOSwu6teinal/s-l1600.webp) usually about 200 feet per minute at sea level was past 2000 feet per minute
- I was flying like a helicopter, I could feel the aircraft being lifted up vertically

#### Erie

- Antenna
- 9 degree runway

#### Lamar - don't use Flaps

- 

## Cessna 24981 - First Plane Flown

|Date|Hours|
|--|--|
|19790626|1.3|
|19790727|1.2|
|19790811|1.4|
|19791116|1.2|
|Total|5.1|

#### Images

- [In Flight](aircraft/cessna24981.jpg)
- [Ground 1](aircraft/cessna24981_1.webp)
- [Ground 2](aircraft/cessna24981_2.webp)
- [Ground 3](aircraft/cessna24981_3.webp)

## [Logbook](logbook)

|Code|New Code|Location|Date Start|Date End|Comment|
|--|--|--|--|--|--|
|NZY||North Island, CA|||Started Flying|
|L39|RNM|Ramona, CA|||
|BWN|SDM||Brown, CA|||
|SEE||Gillespie, CA|19791126|19791126|First Solo|
|CNO||Chino, CA||||
|TOA||Torrence, CA||||
|YUM||Yuma, AZ||||
|TNP||Twentynine Palms, CA||||
|SNA||San Antonio, CA||||
|RAL||Riverside, CA||||
|EED||Needles, CA||||
|APV||Applevalley, CA||||
|L18||Fallbrook, CA||||
|IPL||Imperial Valley, CA||||
|2L0||Rancho California, CA|||No Longer Exists|
|BJC||Jeffco||19830506|Last Flight|
|2V2|LON|Longmont, CO||||
|48V|EIK|Erie, CO||||
|CYS||||||
|GXY||Greeley, CO||||
|LAA||Lamar, CO||||
