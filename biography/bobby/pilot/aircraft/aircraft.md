# Aircraft 

## Cessna 24981

|Date|Hours|
|--|--|
|19790626|1.3|
|19790727|1.2|
|19790811|1.4|
|19791116|1.2|
|Total|5.1|

#### Images

[In Flight](cessna24981.jpg)
[Ground 1](cessna24981_1.webp)
[Ground 2](cessna24981_2.webp)
[Ground 3](cessna24981_3.webp)

