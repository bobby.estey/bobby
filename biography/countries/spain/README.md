# Spain

- My Family went to Spain with my Daughters High School Class in 2004 and traveled starting in Madrid and visited the southern part of the Country

# Filipinos Cookies

- Found a site, however, they are charging a **REDICULOUS PRICE** - should only be at the most $10, not $50
- [Order Online](https://ibspot.com/products/artiach-filipinos-cookies-crunchy-ring-shaped-biscuits-dipped-in-real-chocolate-128g-each-product-of-spain-milk-chocolate-pack-of-3)

- 20040415 we walked into a Grocery Store and my Daughter was laughing
- She said, "Daddy look over there"
- That's when we spotted the Filipinos Cookies
- What was really hilarious was the back of the wrapper
- My Wife is Filipina and I told her to get in the Fridge

This is what was on the back of the wrapper that reads the following:

```
Spanish - Prueba Filipinos Despues De I Hora En La Nevera
Portuguese - Prova Filipinos Depois De 1 Hora No Frigorifico
English - Try Filipinos After One Hour in the Fridge
```

## Images

![Filipinos Wrapper](filipinosRefrigerator.jpg)

![Filipinos Receipt](filipinosReceipt.jpg)

![Filipinos CCM](filipinosChocoloateCookiesMadrid.jpg)

![Filipinos Cookies](filipinosCookies.jpg)

## Video

- [Filipinos Cookies](filipinosCookies.mp4)
