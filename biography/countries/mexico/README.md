# United Mexican States (Mexico)

- [United Mexican States](https://en.wikipedia.org/wiki/Mexico) is the official name of Mexico
- Mexico consists of 31 States and 1 Federal District (Mexico City) the Capital 

## Mexican States Map

- [Mexican States Map](https://www.vexels.com/vectors/preview/117234/mexico-map-vector-design)

## Mexican States

![Mexican State Codes](mexicanStatesCode.png)
