# Tecopa, CA

- We wanted to drive the Old Spanish Trail to see the route
- We drove Westboard, departed NV 160 and drove to Tecopa, CA

## Route

![NV 160 to Tecopa, CA](routeNV160_tecopa.png)

## View looking Eastbound near Summit

- The actual view is very nice, the image from Google Maps doesn't do justice

- [Near the top of the Mountain](https://www.google.com/maps/@35.8829008,-116.0604073,3a,75y,138.57h,111.71t/data=!3m6!1e1!3m4!1s-KezZMSpNdugufeVeka68w!2e0!7i16384!8i8192?coh=205409&entry=ttu)

## Vegetation

- First Vegetation we saw after going over the Summit

![Vegetation](vegetation.png)

- [Vegetation Google Maps](https://www.google.com/maps/@35.8697236,-116.1690537,2356m/data=!3m1!1e3?entry=ttu)

## Tecopa

- [Tecopa, CA](https://www.google.com/maps/place/Tecopa,+CA/@35.8519359,-116.2285195,1352m/data=!3m1!1e3!4m6!3m5!1s0x80c66f8b818c9a5b:0x15a1230c17f65d14!8m2!3d35.8482993!4d-116.2264127!16zL20vMHF6X2o?entry=ttu)

- [Death Valley Brewing](https://www.google.com/maps/@35.8484773,-116.2267349,3a,90y,176.99h,78.13t/data=!3m7!1e1!3m5!1sxZ1HEkduRt2qh7f35NtvfA!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3DxZ1HEkduRt2qh7f35NtvfA%26cb_client%3Dmaps_sv.tactile.gps%26w%3D203%26h%3D100%26yaw%3D32.07147%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656?coh=205409&entry=ttu)

## Tecopa - Francis School

- This is an old school image
- The building is abandoned, broken windows, boarded up, destroyed

- [Old School Image](https://www.google.com/maps/@35.8490739,-116.2170866,3a,75y,159.02h,84.86t/data=!3m7!1e1!3m5!1sXaARacSBSh_YZdF6FI_z4A!2e0!5s20081201T000000!7i13312!8i6656?coh=205409&entry=ttu)
