# Homeless Food Pantry - 936 5th Ave, San Diego, CA 92101

I believe this was the address.

Late 1980, Steve and I were in downtown San Diego just hanging out and after a while this is what happened:

I said, "Steve, I'm hungry, let's get something to eat".

Steve said, "I know a place that has good food, I go there all the time".

Steve takes me to a Homeless Food Pantry.

https://www.google.com/maps/place/936+Fifth+Ave,+San+Diego,+CA+92101/@32.715151,-117.1610968,19z/data=!3m1!4b1!4m6!3m5!1s0x80d954a7f48d1077:0x50afe9daee7ad521!8m2!3d32.715151!4d-117.1604531!16s%2Fg%2F11bw40_ysr?entry=ttu&g_ep=EgoyMDI0MTIxMS4wIKXMDSoASAFQAw%3D%3D
