# [Mount Airy, NC](https://en.wikipedia.org/wiki/Mount_Airy,_North_Carolina)

- Mount Airy is primarily a tourist town set with the theme of the Andy Griffith Show

- [Andy Griffith](https://en.wikipedia.org/wiki/Andy_Griffith) 
     - was born and raised in Mount Airy, NC
     - now you know where the name Mayberry came from :-)
     
## [Mayberry](https://www.visitmayberry.com/attractions/category/mayberry/)
     
- a mockup of the original television show including:
     - [Courthouse and Jail](https://www.google.com/search?q=mayberry+courthouse&tbm=isch&sa=X&ved=2ahUKEwjjxfmmsYv_AhXQI0QIHUYvCjEQ0pQJegQICxAB&biw=1854&bih=938&dpr=1)
     - [Wally's Service](https://www.google.com/search?q=mayberry+wallys+service&tbm=isch&ved=2ahUKEwil9KSpsYv_AhWmPEQIHSaSDb0Q2-cCegQIABAA&oq=mayberry+wallys+service&gs_lcp=CgNpbWcQAzoHCAAQigUQQzoFCAAQgAQ6BggAEAcQHjoGCAAQCBAeOgcIABAYEIAEOgYIABAFEB5QiQ5YlkRg90VoAXAAeACAAZwBiAHcDZIBAzguOJgBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=AqpsZKXrMKb5kPIPpqS26As&bih=938&biw=1854)
     - Floyd's Barber Shop
     - and more
     
## [Pilot Mountain, NC](https://en.wikipedia.org/wiki/Pilot_Mountain,_North_Carolina)

- was called Mount Pilot on the Mayberry Show

![12 miles southeast of Mount Airy, NC](./mountAiryPilotMountain.png)
