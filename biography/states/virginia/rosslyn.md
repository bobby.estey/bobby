# Rosslyn, VA

- We just drove from [Maricopa, AZ](https://en.wikipedia.org/wiki/Maricopa,_Arizona) October 1999 to [Rosslyn, VA](https://en.wikipedia.org/wiki/Rosslyn,_Virginia) to start a new job on 01 November 1999.
- I was working for Nichols Research who was later acquired by Computer Sciences Corporation (CSC) had two contracts with the Federal Government.  I was working the Source Determination Imaging System (SDIS) contact
- Address 1700 North Moore Street, 21st Floor (we had the entire 21st Floor)

# Funny Story

The first day at work, there were about 8 (people who have been working at the company 6 months and more, before I showed up) of us that went to lunch, we were walking to a restaurant a couple blocks towards the Potomac River.  We crossed the street, there use to be a Burger King (standard one story building, today a skyscraper) and a guy was asking for money.  We all just passed him and continued on walking to the restaurant.  The second day at work, again, they asked, you want to go to lunch with us.  SURE.  We walked across the street and we are going to Burger King.  The same guy is there asking for money.  The other guys, walked by, however, I stopped.  

I asked him, "What did you want?"<br>He said, "I need money for food".<br>I said, "I am going to Burger King, tell me what you want and I will get it for you".<br>He said, "Just give me money and I will get the food later".<br>I said, "No, just tell me what you want and I will bring out to you".<br>He said reluctantly, "Ok, Cheeseburger and Fries".<br>

I walked inside ordered my food, had lunch with the guys and just before we left, ordered a Cheeseburger and Fries.

I walked outside, I was trying to hand the bag of food to the guy and he said, "What is that?"<br>I said, "It's your food.  You told me you wanted a Cheeseburger and Fries".<br>He said, "I don't want that".<br>I said even louder, "I got your Cheeseburger and Fries".<br>

He started backing up and I keep walking towards him, and said over and over, louder and louder, "I got your Cheeseburger and Fries".<br>

He then started running down the street and I was following in pursuit, with the bag in the air, repeating, "I got your Cheeseburger and Fries".<br><br>
After a block, I stopped, walked back and the 7 others guys were dumb founded.

**No worries, the food was not wasted.  I was still hungry, glad I had that Cheeseburger and Fries.**

The next day, that guy was never there again.  I was then informed, he was there for 6 months and thanks to you, he is gone.  Never saw him again.

BOTTOM LINE:  I always tell people, I will only give food.  If you are legitimate and I see you are telling me the truth.  I will later give you money.
