# [Storms - Hayes, VA 23072](https://cv64.us/public/storms/)

- [3180 Horse Road](./3180/rebuild/README.md)
- [3198 Horse Road](./3198/README.md)

|Date (YYYYMMDD)|Name|Description|
|--|--|--|
|2001|[High Water](/public/storms)|- no damage|
|2002|[High Water](/public/storms)|- no damage|
|20030918|[Hurricane Isabel](https://en.wikipedia.org/wiki/Hurricane_Isabel)|- 3180 and 3198 Destroyed, Life Event, So Terrible<br>- Lost 3180<br>- Rebuilt 3198<br>- 12 days without power<br>- Homeless|
|2006|Storm 1 - forgot the name|- 3180 no damage<br>- 3198 replaced insulation again|
|2006|Storm 2 - forgot the name|- 3180 no damage<br>- 3198 replaced insulation again|
|20060901|[Tropical Storm Ernesto](https://www.weather.gov/mhx/Sep012006EventReview)|- 3180 no damage<br>- 3198 replaced insulation again|
|2009|Storm 1 - forgot the name|- 3180 no damage<br>- 3198 replaced insulation again|
|201108nn|[Hurricane Irene](https://en.wikipedia.org/wiki/Hurricane_Irene)|- no damage|
|20111029|NorEastern|- no damage|
|20121028|[Hurricane Sandy](https://en.wikipedia.org/wiki/Hurricane_Sandy)|- 3180 no damage<br>- 3198 replaced drywall|
|2012|Storm 1 - forgot the name|- no damage|
|20151002|Storm 1 - forgot the name|- no damage|
|20160903|[Tropical Storm Hermine](https://www.weather.gov/mhx/Hermine_090216)|- no damage|
|20180912|[Hurricane Florence](https://www.weather.gov/ilm/HurricaneFlorence)|- no damage|
|20190906|[Hurricane Dorian](https://en.wikipedia.org/wiki/Hurricane_Dorian)|- no damage|
|20191011|Flood|- no damage|
|20200919|Flood|- no damage|
|20211029|Flood|- no damage|
|20220312|Flood|- no damage|
