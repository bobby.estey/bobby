# 2003 Chevy S10 - Great Vehicle - Virginia License CV64US

## Perfect Truck Beatdown (Videos)

- [Jordan](./perfectTruckBeatdown/jordan.mov)
- [Poe](./perfectTruckBeatdown/poe.mov)
- [Scott](./perfectTruckBeatdown/scott.mov)
- [Melbert](./perfectTruckBeatdown/melbert.mov)
- [Tag Team](./perfectTruckBeatdown/tagTeam.mov)

## Signs

![sign1](./signs/20180607_180914.jpg)
![sign1](./signs/20180607_180926.jpg)
![sign1](./signs/20180611_123857.jpg)
![sign1](./signs/20180611_123902.jpg)

## Rollover - 14 March 2011 @ 1720 EDT

- Daylight Savings Time - I fell asleep

- [Video](./rollover/s10rollOver.avi)

![s10rollOver1](./rollover/s10rollOver1.jpg)
![s10rollOver2](./rollover/s10rollOver2.jpg)
![s10rollOver3](./rollover/s10rollOver3.jpg)
![s10rollOver4](./rollover/s10rollOver4.jpg)
![s10rollOver5](./rollover/s10rollOver5.jpg)
![s10rollOver6](./rollover/s10rollOver6.jpg)

## Election 20180607

![20181106](./election2018/20181106_161215.jpg)
