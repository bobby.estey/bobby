# [3180 Horse Road, Hayes, VA 23072](https://www.zillow.com/homedetails/3180-Horse-Rd-Hayes-VA-23072/94123616_zpid/)

## Stop Building Wood Homes

- Most builders today do not take advantage of today's technologies
- Wood Buildings:  **19th Century, Termite Infested, Rotting, Fire Starting Cracker Boxes**
- Why Build with [Concrete and Steel - Storms](../../storms.md)
- [Hurricane Isabel](https://en.wikipedia.org/wiki/Hurricane_Isabel) - 18 September 2003 destroyed thousands of homes including mine and what did builders do?  **Rebuilt Cracker Boxes**
- The Federal Government should of only approved new homes with new 21th Century Technology
- I built a new home using the latest in technology:
     - [Low E Barrier](https://www.google.com/search?q=low+e+barrier&client=ubuntu-chr&hs=47p&sca_esv=10eeeb18313d2e74&sca_upv=1&udm=2&biw=1882&bih=956&sxsrf=ADLYWILbfLvqPbgwvmwkCX93XgON0_cziA%3A1718845715531&ei=E4FzZrSRIOjdkPIPy9q8aA&ved=0ahUKEwj05rCi_-iGAxXoLkQIHUstDw0Q4dUDCBI&uact=5&oq=low+e+barrier&gs_lp=Egxnd3Mtd2l6LXNlcnAiDWxvdyBlIGJhcnJpZXJIrhhQAFjVFnAAeACQAQCYAWagAZUIqgEEMTIuMbgBA8gBAPgBAZgCCKACqwXCAgQQIxgnwgIFEAAYgATCAggQABiABBixA8ICChAAGIAEGEMYigXCAgsQABiABBixAxiDAcICBhAAGAgYHsICBxAAGIAEGBiYAwCSBwM3LjGgB60x&sclient=gws-wiz-serp) and [30 Mil Tar paper](https://www.google.com/search?q=30+mil+tar+paper&client=ubuntu-chr&hs=ETV&sca_esv=10eeeb18313d2e74&sca_upv=1&udm=2&biw=1882&bih=956&sxsrf=ADLYWIKm3BH2xLmOJO6Nz-81I78igXxnyQ%3A1718845744923&ei=MIFzZu2KONu-kPIPjuWDgAE&ved=0ahUKEwjt4rKw_-iGAxVbH0QIHY7yABAQ4dUDCBI&uact=5&oq=30+mil+tar+paper&gs_lp=Egxnd3Mtd2l6LXNlcnAiEDMwIG1pbCB0YXIgcGFwZXJIwSVQAFimJHAAeACQAQCYAU6gAZsJqgECMTa4AQPIAQD4AQGYAgqgAoMGwgIIEAAYgAQYsQPCAgoQABiABBhDGIoFwgILEAAYgAQYsQMYgwHCAgUQABiABMICBhAAGAgYHsICBxAAGIAEGBiYAwCSBwIxMKAHxzA&sclient=gws-wiz-serp), superior to [Tyvek](https://www.google.com/search?q=tyvek&client=ubuntu-chr&hs=vTV&sca_esv=10eeeb18313d2e74&sca_upv=1&udm=2&biw=1882&bih=956&sxsrf=ADLYWIIq65H0R5QBjwvgn-N0v-yX8JdSUg%3A1718845787300&ei=W4FzZsf4EZbukPIPx-WMuAQ&ved=0ahUKEwiHks3E_-iGAxUWN0QIHccyA0cQ4dUDCBI&uact=5&oq=tyvek&gs_lp=Egxnd3Mtd2l6LXNlcnAiBXR5dmVrMgoQABiABBhDGIoFMggQABiABBixAzIFEAAYgAQyChAAGIAEGEMYigUyChAAGIAEGEMYigUyChAAGIAEGEMYigUyBRAAGIAEMgUQABiABDIFEAAYgAQyChAAGIAEGEMYigVI9xpQAFi6FHAAeACQAQCYAboBoAHlBqoBAzAuNbgBA8gBAPgBAZgCBaACiAfCAg4QABiABBixAxiDARiKBcICBBAAGAOYAwCSBwUwLjQuMaAH7Bc&sclient=gws-wiz-serp)
     - [Integrated Concrete Forms (ICF)](https://www.google.com/search?q=integrated+concrete+forms&client=ubuntu-chr&hs=7RV&sca_esv=10eeeb18313d2e74&sca_upv=1&udm=2&biw=1882&bih=956&sxsrf=ADLYWILJBOosqk4YT0GdBSJzJjJmsCVyJg%3A1718845675315&ei=64BzZrn3EtjbkPIPtfiv-Aw&oq=integrated+conc&gs_lp=Egxnd3Mtd2l6LXNlcnAiD2ludGVncmF0ZWQgY29uYyoCCAEyBRAAGIAEMgUQABiABDIFEAAYgAQyBhAAGAgYHjIGEAAYCBgeMgYQABgIGB4yBhAAGAgYHjIGEAAYCBgeMgYQABgIGB4yBhAAGAgYHkjEN1AAWPskcAB4AJABAJgBhQGgAfMJqgEEMTMuMrgBAcgBAPgBAZgCD6ACrwrCAgoQABiABBhDGIoFwgIOEAAYgAQYsQMYgwEYigXCAggQABiABBixA8ICCxAAGIAEGLEDGIMBwgINEAAYgAQYsQMYQxiKBZgDAJIHBDEzLjKgB_pR&sclient=gws-wiz-serp)
     - [Steel (12 Gauge Red Iron Zinc Oxide Steel)](https://www.google.com/search?client=ubuntu-chr&hs=km&sca_esv=10eeeb18313d2e74&sca_upv=1&sxsrf=ADLYWII7QjyHGYDYxc2gc5lU0cMDeqtYGQ:1718845671331&q=12+Gauge+Red+Iron+Zinc+Oxide+Steel&udm=2&fbs=AEQNm0DBTogQXaq7JsoUAbTsd0B6JEdyvwg9pOFJlHzVj4IGyljlmelwKWALZZi_asBuBYABb8VqRJoY_CFoUVIJzsGDz2dGoCUDJodyfFLo5IrWkpt73JLIDT1Ph855cJZXgieiCmMCvUYDF80ZnGZv3bmEdK507yZsQCMuF9bTnm6nKUU7RITEUtooypJvKp6nzY78F_hpRvmpn4AT0r4ceDrTR5dgSA&sa=X&ved=2ahUKEwjJgKeN_-iGAxUXJEQIHVWnAh8QtKgLegQIDhAB&biw=1882&bih=956&dpr=1)
     - [Tankless Water Heater](https://www.google.com/search?q=exterior+rinnai+tankless+gas+water+heater&client=ubuntu-chr&hs=zl&sca_esv=10eeeb18313d2e74&sca_upv=1&sxsrf=ADLYWIJCrOcC2bnD3pV8UFf1j0YblTUVKg%3A1718845624165&ei=uIBzZt3hCf6Pur8Pgoe32AU&oq=rinnai+tankless+water+heater+gas+ext&gs_lp=Egxnd3Mtd2l6LXNlcnAiJHJpbm5haSB0YW5rbGVzcyB3YXRlciBoZWF0ZXIgZ2FzIGV4dCoCCAAyBhAAGBYYHjIGEAAYFhgeMgsQABiABBiGAxiKBTILEAAYgAQYhgMYigUyCxAAGIAEGIYDGIoFMgsQABiABBiGAxiKBTILEAAYgAQYhgMYigUyCBAAGIAEGKIEMggQABiABBiiBDIIEAAYgAQYogRI1C5Q5wlY7w5wAXgBkAEAmAFuoAGcA6oBAzIuMrgBAcgBAPgBAZgCBaACrwPCAgoQABiwAxjWBBhHwgITEC4YgAQYsAMY0QMYQxjHARiKBcICCxAAGIAEGJECGIoFwgIFEAAYgASYAwCIBgGQBgmSBwMxLjSgB58i&sclient=gws-wiz-serp)
          - Builders were installing traditional gas water heaters
          - I wasn't going to have this at all, luckily back in 2003, the plumber actually suggested to install a Tankless Water Heater
          - Energy efficient, no sediment issues, endless hot water, was the greatest
     - [Spray Foam Insulation](https://en.wikipedia.org/wiki/Spray_foam)
          - Compared to batt insulation, there are NO LEAKS

|Layers|Description|
|--|--|
|Footer|- Footer Code for more than 95% of homes today is: 8" x 2'<br>- This home was 3' x 3'<br>- [Rebar 1](rebar1.png)&nbsp;&nbsp;&nbsp;&nbsp;[Rebar 2](rebar2.png)|
|1st Floor|- Masonry (Integrated Concrete Forms (ICF)<br>- 1.5" foam barrier each side<br>- 10" concrete gap<br>- 1/2" rebar every square foot both horizontal and vertical<br>- Exterior wall brick and Interior wall stucco, overall 19" thick walls<br>- Efficiency Rating - R75 more than **THREE** times more efficient than traditional homes<br>- [ICF 1](icf1.png)&nbsp;&nbsp;&nbsp;&nbsp;[ICF 2](icf2.png)&nbsp;&nbsp;&nbsp;&nbsp;[ICF 3](icf3.png)&nbsp;&nbsp;&nbsp;&nbsp;[ICF 4](icf4.png)&nbsp;&nbsp;&nbsp;&nbsp;[ICF 5](icf5.png)|
|2nd and 3rd Floors|- Steel (12 Gauge Red Iron Zinc Oxide Steel)<br>- [12 Gauge Red Iron Zinc Oxide](12gaugeRedIronZincOxide.png)<br>- [Internal 1](internal1.png)&nbsp;&nbsp;&nbsp;&nbsp;[Internal 2](internal2.png)|

#### HERE'S THE BEST PART

- The neighbors watched me build the home and they called the place, **AN ABOVE GROUND BOMB SHELTER**
- Cost building with Masonry and Metal is **CHEAPER THAN WOOD**
- Home expenses was **SIX** times cheaper than traditional homes, I owned two homes that were next to each other:

|Address|Structure|Square Feet|Electrical|
|--|--|--|--|
|[3198](../../3198/README.md)|Wood|1458|- Winter - $300 per month<br>- Summer - $400 per month|
|3180|Masonry / Metal|3055|- Winter - $100 per month<br>- Summer - $150 per month|

## Videos

|Channel|Description|
|--|--|
|[Concrete](https://rumble.com/c/c-6450766)|- Foundation<br>- Concrete Pump Truck<br>- Walls<br>- Slab|
|[Integrated Concrete Forms](https://rumble.com/c/c-6450798)|- Integrated Concrete Forms Setup|
|[Bobby Working Steel](https://rumble.com/c/c-6450723)|- Bobby Working on Zinc Oxide 12 Gauge Red Iron|
|[Steel / Interior](https://rumble.com/c/c-6451029)|- Steel Erection<br>- Interior|
