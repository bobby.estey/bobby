# Carshow - Truck at the Car Show

## Here's the Story

- I was at a 0800 meeting with my Cursillo Brothers
- There were just a couple Patron cars in the parking lot
- Finished breakfast, we all walked outside and were surprised there were about 20 classic cars roped off and my truck happen to be in the same group of cars
- I quickly turned my truck around where I parked earlier so that everyone could see the excellent care and workmanship
- I popped the hood to impress everyone. LOL
- My truck is a work of art, all beaten up, rolled once, in a rollover wreck, etc.
- **EVERYONE** was impressed, **OK, I am lying**

## Images

![S10 - At it's Finest](20151114_091038.jpg)
![Bob Donnelly - Awesome Guy / Mr Studley](20151114_091140.jpg)
![Roadrunner](20151114_091113.jpg)
![S10 - Lead Car](20151114_091204.jpg)
