# Estey City, New Mexico

|||
|--|--|
|Latitude/Longitude|33.5745186,-106.2672251|
|County|Lincoln|
|Post Office Established|1901|
|Post Office Discontinued|1903|
|Post Office Reestablished|1904|
|Post Office Discontinued|1910|

- 37 miles southwest of Carrizozo, New Mexico
- Estey City has long since disappeared.  Only old mine dumps serve as a reminder of its past existence.  Copper minerals had been known to exist in this region for many years, but it was not until the turn of the twentieth century that an effort was made to extract them.  The new town site on the southeastern edge of the Oscura Mountains was the namesake of David M. Estey, of Owasso, Michigan, the promoting force behind the Ester Mining and Milling Company, which at one time controlled about three hundred claims in the district.
- By July 31, 1901, the little but promising copper camp boasted of its 250 citizens, fifty dwelling, a large general merchandise store, one saloon, a post office and a mill.  Expansion was rapid.  Churches, a school, a large hotel capable of accommodating sixty guests, electric lights, installation of water pipes, erection of a smelter, building of new businesses and a doubling population followed the promising mining activities.  Times were favorable.  The various mining companies and their eastern backers had little doubts as to Estey City's propitious future.
- These auspicious days were to be short-lived.  Trouble was brewing in the form of the European panic of 1901-1902, which forced the price of copper downward.  In May of 1902, Estey City donned the gloom of a ghost town.  Mining ceased, the El Paso and Northeastern shut down the Oscura Station, which had served as Estey City's transportation link, mail service stopped and only four families from the former population of five hundred remained.  However, by midsummer of that year the situation had taken a favorable upswing.  Copper was once again in demand.  The Dividend Mining and Smelting Company bought up the Estey Mining and Milling properties and revived both mining and Estey City.  Other mining companies rushed in to the purchase claims, the post office reopened and vacant houses were quickly reoccupied.  Once again Estey City was alive and busy.
- The conditions, the times, and other unforeseen factors contributed to the town's eventual death about 1910.  Today the site of Estey City is inaccessible to the curious tourist, since it lies within the boundaries of the restricted Whites Sands Missile Range.

## Images

- [Estey City](esteyCity.jpg)
- [Estey City Abandoned](esteyCityAbandoned.jpg)
- [New Mexico Ghost Town Map](newMexicoGhostTownMap.jpg)
- [New Mexico Trinity Site Map](newMexicoTrinitySiteMap.jpg)
- [Estey City to Trinity Site](esteyCity2TrinitySite.png)

## Links

- [Trinity Site](trinity.md)
- [Mine Data Estey City](https://www.mindat.org/feature-5467111.html)
- [Western Mining History](https://westernmininghistory.com/mine-detail/10087891/)
- [Road On Map](https://www.roadonmap.com/us/where-is/Estey_City_(historical)-Socorro_NM,locale)
