# Maricopa, AZ (the town - when we lived there, not County)

[Maricopa, AZ](https://en.wikipedia.org/wiki/Maricopa,_Arizona) in [Pinal County](https://en.wikipedia.org/wiki/Pinal_County,_Arizona) you have to make sure you say CITY or people in Arizona will be confused.  The [Phoenix](https://en.wikipedia.org/wiki/Phoenix,_Arizona) Metropolitan Area is in [Maricopa County](https://en.wikipedia.org/wiki/Maricopa_County,_Arizona), two different places.  One of the reasons I hated living there was people always thought we were talking about the County.  

- Confusion Story - My Sister flew from Denver to Phoenix.  Went to a Rental Car Counter, rented a car and this is what happened:
     - Sister asked the Representative, "Can you give me directions to drive to Maricopa"
     - The Representative said, "You are in Maricopa County"
     - Sister said, "No, the Town Maricopa"
     - The Representative said, "There is no such place as Maricopa, AZ"
     - My Sister called me and explained to me what was happening and I told my sister to give the phone to the Representative (this is before speakers on cell phones)
     - I asked the Representative, "Do you know where Harrah's Ak-Chin Casino is located?"
     - Representative, "Yes"
     - THAT'S MARICOPA, AZ [^1]

We lived in Maricopa (TOWN) from 1998-1999 population was just under 1,000 people and blew up in population starting in 2005:

- Mass increase in population, 6000% from when we lived there, go here:
- [Population Maricopa AZ](https://www.google.com/search?q=population+maricopa+az&oq=population+maricopa+az)

## Maricopa, AZ - the town (1998 - 1999)

Life in Maricopa, AZ the from 1998 - October 1999 population was just under 1,000.

- Not one traffic light
     - The only thing that would stop traffic would be the train going through
     - The train didn't even slow down, did at least 60 MPH
     - We couldn't believe the stupid residents, they would pull up next to the track.  If the train derailed they were dead
- One gas station
     - Shell Gas
- One grocery store
     - Cereal boxes that had expiration dates of over 3 years
          - that's why people always drove to Phoenix to shop
     - Live animals in the back, which was illegal
     - The state finally shut the place down due to health reasons
- One hardware / feed store
- One restaurant
     - [Headquarters](https://www.google.com/search?q=headquarters+maricopa+az)
     - Still there today
- AND ONE CASINO
     - [Harrah's Ak-Chin Casino](https://www.caesars.com/harrahs-ak-chin)
     - A very small Casino at the time, with very BIG Entertainment, this was great
     - WHY?  Maricopa was 30 miles south of Phoenix and they tried everything to get people to visit
     - Las Vegas shows in a town of less than a thousand people
- THAT'S IT!!! - That was Maricopa the Town

## Maricopa, AZ - the city (2007)

- The last time we visited was in 2007 (8 years later) and not the same place we remember, AT ALL!!!
- Driving south on AZ 347 all of our jaws dropped, WHAT THE HELL HAPPEN HERE???
- Street lights, Gas Stations, Shopping Centers, Home Depot, Restaurants, and much more...

# Leaving

- 25 October 1999, we left Maricopa, driving across the country to [Rosslyn, VA](https://en.wikipedia.org/wiki/Rosslyn,_Virginia) to start work 01 November 1999
- [Driving Route](maricopaAZrosslynVA.png)
- When we left Maricopa and the Great State of Arizona was the biggest mistake of our lives
- I never wanted to leave Maricopa but Ester didn't like living rural
- I always tell Ester over and over, we should of never left

# References / Footnotes

[^1]: DUMBASS (pisses me off when people make statements without knowing the facts)
