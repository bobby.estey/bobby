# Chuck Davison

- Chuck was a friend of mine while working at Motorola
- Chuck was about 6'4" and 270 lbs
- 50% [Chiricahua Apache](https://en.wikipedia.org/wiki/Chiricahua) - 50% Scottish
- Captain [USMC Reconnaissance](https://en.wikipedia.org/wiki/United_States_Marine_Corps_Force_Reconnaissance) Vietnam War
- Chuck owned 122 sections of land in Arizona

![Chuck Davison 19971101](chuckDavison.png)

## Property

#### Zoomed In

![Chuck Davison Property Zoom](chuckDavisonPropertyZoom.jpg)

#### Zoomed Out

![Chuck Davison Property](chuckDavisonProperty.jpg)
