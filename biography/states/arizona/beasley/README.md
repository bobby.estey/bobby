# Norman Beasley (Grasshopper)

## Degrees

- 7th Degree Black Belt - Isshinryu Karate
- 7th Degree Black Belt - Osaka Shim Do (Moo Duk Kwan)
- Certified Level One Instructor - Shinkendo (Japanese Combat Swordsmanship)

## Other Studies

- Aikido
- Japanese - Jujutsu (not Brazilian Jiu-Jitsu)
- Judo
- Shōbayashi Shōrin-ryū
- Wing Chun
