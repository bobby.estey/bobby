Movies - Dayton, Nevada

- Several Movies were filmed in Dayton

## Locations

- Dayton Taphouse - 160 Main St, Dayton, NV 89403
- Rodeo - 200 Pike St, Dayton, NV 89403

## Misfits - 1961 - Clark Gable / Marilyn Monroe
[Movie](https://en.wikipedia.org/wiki/The_Misfits_(1961_film))
- 21:50
     - Nevada it's the Leave it State, if you got money to gamble, leave it here
     - If you have a Wife you want to get rid of, git rid of her here
     - If you have an extra atom bomb you don't need, blow it up here, nobody is going to mind in the slightest
     - The slogan of of Nevada is anything goes but don't complain if it went
     - My husband didn't leave me, he just didn't return
- 46:44 - Driving to Dayton
- 51:18 - Arrive in Dayton
- 56:00 - Downtown Dayton
- 59:30 - Dayton Rodeo - across the street from 200 Pike Street
- 60:51 - Dayton Taphouse
     
## Shootist - 1976 - John Wayne
[Movie](https://en.wikipedia.org/wiki/The_Shootist)

## Honky Tonk Man - 1982 - Clint Eastwood
[Movie](https://en.wikipedia.org/wiki/Honkytonk_Man)

## Pink Cadillac - 1989 - Clint Eastwood
[Movie](https://en.wikipedia.org/wiki/Pink_Cadillac_(film))
