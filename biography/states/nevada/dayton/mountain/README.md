# Mountain

- Dayton, NV - Mountain Summit - Fort Churchill Road and East of Chavez Road, Dayton, NV 89403

## Video Description

- First Mountain in my life I actually climbed, not a hill a Mountain
- You can hear and experience my struggles being a first timer
- [Pahrump, NV - Mountain Climbing](https://gitlab.com/bobby.estey/bobby/-/blob/main/biography/states/nevada/pahrump/mountains/README.md?ref_type=heads) 
     - My experience increased after moving to Pahrump with serious Mountain Climbing, almost everyday
     - The Mountains in Pahrump, advantages, NO SNAKES, disadvantages, STEEP, VERY STEEP.  80, 90+ Plus Degrees.  Yes, OVERHANGS.
- I can surely say, I can go up this mountain a lot easier now, after Pahrump

# Google Maps

- The Links are using Google Maps, so you can zoom in / out and move in all directions.  I have the zooms all the way in

|Key|Value|
|--|--|
|Latitude|39.304252|
|Longitude|-119.475677|
|Base Altitude|4300 ft|
|Summit Altitude|[5092 ft](elevation.png)|
|Mountain Street View|[Mountain Street View](https://www.google.com/maps/@39.2983016,-119.4996976,3a,15y,72.54h,92.49t/data=!3m6!1e1!3m4!1spekcjrZ4Ca9H0_uHKaBpkA!2e0!7i16384!8i8192!5m1!1e4?entry=ttu)
|Mountain Summit Image|[Mountain Top Image](https://www.google.com/maps/place/39%C2%B018'15.3%22N+119%C2%B028'32.4%22W/@39.3042033,-119.4756446,54m/data=!3m1!1e3!4m4!3m3!8m2!3d39.30425!4d-119.4756667?entry=ttu)
|Mountain Summit Terrain|[Mountain Top Terrain](https://www.google.com/maps/place/39%C2%B018'15.3%22N+119%C2%B028'32.4%22W/@39.3036887,-119.480337,15.97z/data=!4m4!3m3!8m2!3d39.30425!4d-119.4756667!5m1!1e4?entry=ttu)|
|Route View 1|[Route View 1](routeView1.png)|
|Route View 2|[Route View 2](routeView2.png)|
|Post View 1|[Post View 1](postView1.jpg)|
|Post View 2|[Post View 2](postView2.png)|
|East Dayton NV|[East Dayton NV](eastDaytonNVtop.png)|
|Sectional Chart|[Sectional Chart](sectionalChart.png)|

# Videos

- [Mountain Summit](https://rumble.com/v3095l6-dayton-nv-mountain-top.html)
     - This is the top of the Mountain
     - There was a wooden pole that had a hive of [Alate - Flying Ants](https://en.wikipedia.org/wiki/Alate) all over the pole.  I removed that pole and replaced with a [Green Fence Post](https://media.suthlbr.com/products/images/18446/1612597_ep_1509131563_0.jpg).  There were thousands of the Alate, luckly they are just a nuisance and don't bite
     - 0:00 - Facing West towards Virgina City, NV - way in the background, with Alate flying around
     - 0:22 - Stagecoach, NV
     - 0:37 - Carson River, Dayton, NV is in the background just before the first set of mountains
     - 0:39 - live just left of the Green Fence Post  next to the road (Fort Churchill Road)

## Additional Videos

#### Climb

- [20230714_181852](https://rumble.com/v5iuc7f-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_181939](https://rumble.com/v5iubvh-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_182211](https://rumble.com/v5ipqdf-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_182325](https://rumble.com/v5ipqnx-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_183115](https://rumble.com/v5ipqv1-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_183115](https://rumble.com/v5ipr7d-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_183637](https://rumble.com/v5iubei-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_183716](https://rumble.com/v5ipri1-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_183720](https://rumble.com/v5iproh-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_184753](https://rumble.com/v5iprwx-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_185424](https://rumble.com/v5ips7p-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_190416](https://rumble.com/v5ipsf7-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_191543](https://rumble.com/v5ipsmz-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_192146](https://rumble.com/v5ipstn-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230714_193137](https://rumble.com/v5ipt0x-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)

#### Fort Churchill Road

- [20230719_160534](https://rumble.com/v5iptlo-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230719_161325](https://rumble.com/v5ipujx-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230719_171511](https://rumble.com/v5ipuq3-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230719_172117](https://rumble.com/v5ipux1-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230719_172317](https://rumble.com/v5ipv83-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)
- [20230719_172349](https://rumble.com/v5ipvdx-dayton-nevada-89403-mountain-fort-churchill-rd-and-east-of-chavez-rd-202307.html)

# Statistics

|Date|Time|Temperature (F)|Humidity (%)|Wind Speed|Direction|Location|
|--|--|--|--|--|--|--|
|20230714|1435|96.8|11.09|7|nne|Base of Mountain|
|20230714|1800|98.6|9.08|5|w|Top of Mountain|
|20230714|1930|96.8|9.59|3|wnw|Base of Mountain|
