# Rock Point Mill

## Why Was it Built?

- 1840 - 1850's - The California Gold Rush and the discovery of Gold in Virginia City brought prospectors to the area

- 1860's - Mill development was necessary for processing raw ore (rock) to release the Gold and Silver.  
    - Mill sites required transportation routes, sloping terrain and an abundance of water, all available in Dayton

- 1861 - The first mill was built for $200,000 and was one of the largest in the territory.  Power was supplied by Carson River water from the Rock Point Dam.  Water was brought a distance of 2000 feet in a wooden flume.  The flume supplied two water wheels.

- 1863 - C. C. Stevenson Mill at Rock Point - Stevenson, later Governor of Nevada, leased the mill site in 1883
    - Ore was first supplied from Stevenson's Kentuck Mine in Gold Hill, later from the Savage Mine via a spur track from Gold Hill to Dayton, built by the narrow gauge Carson & Colorado Railroad
    - The track was removed in 1893 due to long periods of inactivity

- 1898 - Nevada Reduction Works at Rock Point
    - Herman Davis of Dayton bought the Stevenson Mill in 1898
    - In conjunction with a cyanide plant, Davis worked nearly all the tailings in the vicinity
    - In 1900 the mill worked the ore from the new mines in Tonopah
    
NOTE:  With 8 batteries of five stamps each the mill had a capacity to crush 40 tons of ore per day.  In 1864 an additional 16 stamps were added at a cost of $93,000

|Rock Point Mill Map|dayton1861|
|--|--|
|![rockPointMillHistory.jpg](rockPointMillHistory.jpg)|![dayton1861.jpg](dayton1861.jpg)|
|![mill1.jpg](mill1.jpg)|![mill2.jpg](mill2.jpg)|
|![mill3.jpg](mill3.jpg)|![mill4.jpg](mill4.jpg)|

## The Final Days

- 1882 - The first of several fires destroyed the original mill

- 1909-1920 - Fire, flood and population decline plagued the mill, eventually forcing its closure.
    - 1920, the mill was dismantled and move to Silver City

- 1920 - 1954 - the old mill site was used as the local dump until the mill was purchased by the Nevada Department of Highways

|1822 Fire|1909 - 1920|
|--|--|
|![1882](1882fire.jpg)|![1909-1920](1909-1920.jpg)|

## Miscellaneous Images

||||||
|--|--|--|--|--|
|![./images/misc01.jpg](./images/misc01.jpg)|![./images/misc02.jpg](./images/misc02.jpg)|![./images/misc03.jpg](./images/misc03.jpg)|![./images/misc04.jpg](./images/misc04.jpg)|![./images/misc05.jpg](./images/misc05.jpg)|
|![./images/misc06.jpg](./images/misc06.jpg)|![./images/misc07.jpg](./images/misc07.jpg)|![./images/misc08.jpg](./images/misc08.jpg)|![./images/misc09.jpg](./images/misc09.jpg)|![./images/misc10.jpg](./images/misc10.jpg)|
|![./images/misc11.jpg](./images/misc11.jpg)|![./images/misc12.jpg](./images/misc12.jpg)|![./images/misc13.jpg](./images/misc13.jpg)|![./images/misc14.jpg](./images/misc14.jpg)|![./images/misc15.jpg](./images/misc15.jpg)|
|![./images/misc16.jpg](./images/misc16.jpg)|![./images/misc17.jpg](./images/misc17.jpg)|![./images/misc18.jpg](./images/misc18.jpg)|![./images/misc19.jpg](./images/misc19.jpg)|![./images/misc20.jpg](./images/misc20.jpg)|
|![./images/misc21.jpg](./images/misc21.jpg)|![./images/misc22.jpg](./images/misc22.jpg)|![./images/misc23.jpg](./images/misc23.jpg)|![./images/misc24.jpg](./images/misc24.jpg)|![./images/misc25.jpg](./images/misc25.jpg)|
|![./images/misc26.jpg](./images/misc26.jpg)|![./images/misc27.jpg](./images/misc27.jpg)||||

## Videos

- [Rock Point Mill Video - Part 1](https://rumble.com/v2j3sq8-rock-point-mill-dayton-nv-89403-part-1.html)
- [Rock Point Mill Video - Part 2](https://rumble.com/v2j3sx8-rock-point-mill-dayton-nv-89403-part-2.html)
- [Rock Point Mill Video - Part 3](https://rumble.com/v2j3taa-rock-point-mill-dayton-nv-89403-part-3.html)
- [Rock Point Mill Video - Part 4](https://rumble.com/v2j3ttw-rock-point-mill-dayton-nv-89403-part-4.html)
- [Rock Point Mill Video - Part 5](https://rumble.com/v2j3un2-rock-point-mill-dayton-nv-89403-part-5.html)
