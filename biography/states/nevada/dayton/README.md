# [Dayton, NV](https://en.wikipedia.org/wiki/Dayton,_Nevada)

- [Dayton - Nevada's First Settlement](./daytonNV.jpg) and home to the oldest hotel in Nevada
- [Where we Lived](https://www.google.com/maps/place/Dayton,+NV/@39.2508268,-119.9389372,77709m/data=!3m1!1e3!4m6!3m5!1s0x809907d00a56cacd:0x55dfdba9a1f16133!8m2!3d39.2371353!4d-119.5929521!16zL20vMHhkMzc?entry=ttu)
- [Wild Horses](./horsesInYard.png)
    - All over the place and we are talking hundreds.  Sometimes they will walk into residential yards and just start eating the grass, laying around and more
- [Pony Express](https://gitlab.com/bobby.estey/bobby/-/blob/main/biography/states/nevada/ponyExpress/ponyExpress.md)
- [Dayton Marker](https://www.google.com/maps/place/39%C2%B011'58.1%22N+119%C2%B034'15.6%22W/@39.199473,-119.570993,867m/data=!3m2!1e3!4b1!4m4!3m3!8m2!3d39.199473!4d-119.570993)
- We are approximately 6 hours northwest of [Las Vegas, NV](https://en.wikipedia.org/wiki/Las_Vegas), 2 hours east of Sacramento, CA, 4 hours east of San Francisco, CA

![Video](../videoIcon.jpg) [Flight from Dayton, NV to Virginia Beach, NV](https://rumble.com/v28i2pc-reno-flight-dayton-nevada-over-ft-churchill-road-20230202-112555.html)
- Reno Flight Dayton, NV over Ft Churchill Road - 20230202_112555 - continuation from Reno (RNO) to Denver (DIA)
- Ft Churchill Road (East Dayton NV), Stagecoach, Silver Springs ending up with Virginia Beach, NV

## Dayton Taphouse 
- [160 Main St, Dayton, NV 89403](https://www.google.com/maps/place/160+Main+St,+Dayton,+NV+89403/@39.2363923,-119.5937455,17z/data=!3m1!4b1!4m6!3m5!1s0x809907c52547159f:0xe1d63d7ed6993bf4!8m2!3d39.2363923!4d-119.5911706!16s%2Fg%2F11c1y92bwd?authuser=0)
- [Misfits Movie Dayton NV Taphouse](https://www.youtube.com/watch?v=9IuMUzCJ5m8&list=PLZbXA4lyCtqoy9Gs7EtszIglTmUWuP3Kf&index=5)

## Dayton Mountain
- [Dayton Mountain I Climbed](./mountain/README.md)

## Images / Videos

- [Dayton Rumble](https://rumble.com/c/c-6397037)
- [West Dayton Nevada](./westDaytonNV.jpg)
- [East Dayton Nevada](./eastDaytonNV.jpg)
- [Video of Entire Town](./daytonNV.mp4)

## Misfits Movie

- Filmed In Dayton, Nevada 1961 - Rodeo is filmed:  200 Pike St, Dayton, NV 89403
- [Location of Misfits Flat](https://www.google.com/maps/place/Misfits+Flat/@39.3464367,-119.3381478,25622m/data=!3m1!1e3!4m6!3m5!1s0x8098fe7764a4f771:0x24a9d745dd69e89b!8m2!3d39.3401933!4d-119.3835003!16s%2Fg%2F1tgwshtw?authuser=0&entry=ttu)
- [Best Scene of the Movie - Leave It State](https://youtu.be/6ISHd9kAsTw?list=PLZbXA4lyCtqoy9Gs7EtszIglTmUWuP3Kf&t=30)
- [All Clips](https://www.youtube.com/playlist?list=PLZbXA4lyCtqoy9Gs7EtszIglTmUWuP3Kf)
- [Dayton Movies Specifics](./movies/movies.md)
- The lake scene was shot at Pyramid Lake, NV - Home of "Ugliest Prehistoric Fish in the World" according to Arthur Miller. Can't be found anywhere else.

## LEAVE IT STATE

```
Well it's to Nevada, the Leave It State.
If you got money you want to gamble, leave it here.
You got a wife to want to get rid of, get rid of her here.
An extra Atom Bomb you don't need, blow it up here, nobody is going to mind in the slightest.
The Slogan of Nevada is, "Anything goes, but don't complain if it went".
```
