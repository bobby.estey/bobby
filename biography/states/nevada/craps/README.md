# Craps

|![off](off25x25.png) Puck Off|![on](on25x25.png) Puck On|
|--|--|
|- DONT PASS (Wins) - coming out:  2 / 3 / (12 Push)<br>- PASS LINE (Wins) - coming out:  7 / 11|- DONT PASS - must hit 7 before Point<br>- PASS LINE - must hit Point before 7|

## Double Down

- DONT PASS - 4 or 10, otherwise pull your money
- PASS LINE - 6 or 8 only

## Bad Bets

- NEVER BET MIDDLE
- FIELD - Sucker Bet - Sometimes Lucky

## Combinations

|1|2|3|4|5|6|
|--|--|--|--|--|--|
|2 / 12|3 / 11|4 / 10|5 /  9|6 /  8|7|

## Reference / Notes

- DONT PASS 
     - betting with the House
     - can pull your bet any time
     - pull bet if Point is not 4 or 10
     
- PASS LINE
     - betting against the House
     - cannot pull your bet
     
## Table

![Craps](craps.svg)

- [Craps PDF](craps.pdf)
