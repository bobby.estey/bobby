# Pony Express (April 1860 - October 1861)

- [National Pony Express Association](https://nationalponyexpress.org/)
- [National Park Service Pony Express](https://www.nps.gov/poex/index.htm)
 
![Pioneer Casino Rider Passing](./pioneerCasinoPonyExpress20230608T215301.png)

**Rider heading East US50 (Pony Express Trail) - Dayton, Nevada 89403 - Background Pioneer Casino**

![Pony Express Trail](https://i0.wp.com/octa-trails.org/wp-content/uploads/2021/04/octamap_Nevada_MAP.png?resize=1536%2C619&ssl=1)

**April 1860 - October 1861 Pony Express Trail**

We were visiting [Genoa, NV](https://en.wikipedia.org/wiki/Genoa,_Nevada) and went to two Museums.  The last Museum [Douglas County Courthouse](https://visitcarsonvalley.org/business-directory/courthouse-museum-in-genoa/) we were greeted by Kathy Lee.  We started our tour and saw they had a Pony Express exhibit.  Kathy then informed us every year there is a Pony Express Re-Ride that occurs in June.  Each year they alternate the annual route, West to East (Sacramento, CA to Joseph City, MO) or East to West, almost 2,000 miles with a stop to change horses every 10 miles.  What is really neat is we live next to the original trail.

The closest station is just east of us, [Buckland Station](https://parks.nv.gov/parks/buckland-station).  The Rider travels on the road known as Fort Churchill Road going by the old Fort, [Fort Churchill](https://parks.nv.gov/learn/park-histories/fort-churchill-history) next to the Carson River.  Please watch the videos that explain the process.

# Route

- [Route](https://nationalponyexpress.org/historic-pony-express-trail/stations/)
- [PDF Route](https://www.nps.gov/poex/planyourvisit/upload/National-Park-Service-Pony-Express-Map-508.pdf)
     - this is better, you can zoom in and see the trail.
- [Carson to Ft Churchill Route](carson2FtChurchill.png)

# Nevada Schedule

http://nationalponyexpress.org/the-states/nevada/#2023-nevada-reride-schedule

# Videos

- Pony Express 20230608 Reride - Dayton, NV 89403
     - [Rider Approaching @ 16 seconds is clearer](https://rumble.com/v2t1esq-pony-express-20230608-reride-rider-approaching-dayton-nv.html)
          - Rider approaching, coming down the hill in front of the Dayton, NV water tower, approaching on Main Street
     - [Downtown Dayton, NV 89403](https://rumble.com/v2t1d5m-pony-express-20230608-downtown-dayton-nv-89403.html)
          - Rider entering on Main Street, switching Horses and Bags, departs on Pike Street
          - 0:49 - this was the TAPHOUSE in the [Misfits 1961 Movie](https://en.wikipedia.org/wiki/The_Misfits_(1961_film))
          - Notice the Mailbag called a [Mochila](https://postalmuseum.si.edu/collections/object-spotlight/pony-express-mochila) which has four large pockets, called Cantinas.  They all have locks which the station master had the key
     - [Rider Changing Horses](https://rumble.com/v2t1gyy-pony-express-20230608-reride-dayton-nv-89403-us-50-and-ambrose-lane.html)
          - East Dayton - US 50 & Ambrose Lane - Same Rider changing horses
          - The Woman handling the horses for the Rider was Denise, we talked for a while before the Rider showed up.  Denise was nice and informative
          - What was very interesting was the horse replacement I was standing next to was silent the entire time.  When the rider was about one mile from us, you could hear the horse in the distance sounding off and the horse next to us replied back.  They kept doing this until they arrived
          
GOOD STUFF
