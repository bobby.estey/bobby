# [3421 South Underbrush Ave, Pahrump, NV 89048](https://www.zillow.com/homedetails/3421-Underbrush-Ave-Pahrump-NV-89048/220820762_zpid/)

## Diagrams

- [Interior Rooms Draw IO XML](interiorRooms.drawio.xml)
- [Interior Rooms Draw IO PNG](interiorRoomsGrid.drawio.png)
- [Interior Specs Draw IO XML](interiorSpecs.drawio.xml)
- [Interior Specs Draw IO PNG](interiorSpecsGrid.drawio.png)

## Notes

#### Owners

- Robert & Ester Estey
- Parcel 041-072-11 Lot 3
- 3421 South Underbrush Ave, Pahrump, NV  89048
- 775.513.7587
- bobbycv64@yahoo.com

#### Specifications

|Attribute|Dimensions|
|--|--|
|Building Size|60'L x 60'W|
|14 Windows|6'W x 4'H|
|1 Bay Window|2'D x 8'W x 4'H|
|Doors|3'W x 7'H|
|[Accordian Glass Door](https://www.homedepot.com/p/ERIS-Eris-120-in-x-80-in-Left-Swing-Outswing-Black-Aluminum-Folding-Patio-door-1R3L-BFO-12080-1R3L/322680306)|10'W x 80"H|
|[Refrigerator](https://www.homedepot.com/p/ZLINE-Kitchen-and-Bath-60-in-4-Door-French-Door-Refrigerator-with-Internal-Ice-and-Water-Dispenser-in-Fingerprint-Resistant-Stainless-Steel-RBIV-SN-60/326336100)|5'W x 2'D x 7'H|
|[Oven / Stove](https://www.homedepot.com/pep/Cosmo-Commercial-Style-48-in-5-5-cu-ft-Double-Oven-Gas-Range-with-8-Italian-Burners-in-Stainless-Steel-COS-GRP486G/319244073?g_store=&source=shoppingads&locale=en-US&pla&mtc=SHOPPING-CM-CML-GGL-D29A-029_013_REFRIG-NA-Multi-NA-PLALIA-5163653-NA-NA-NA-NBR-NA-NA-NA&cm_mmc=SHOPPING-CM-CML-GGL-D29A-029_013_REFRIG-NA-Multi-NA-PLALIA-5163653-NA-NA-NA-NBR-NA-NA-NA-71700000032407472-58700003838956052-92700078410029660&gad_source=1&gclid=CjwKCAjwps-zBhAiEiwALwsVYYydNIflhxkUyRT33v7jtky7kKIXwc7SUl10JCrVPkQTTf9NHivclhoCEScQAvD_BwE&gclsrc=aw.ds)|4'W x 28"D x 40"H|
|Dishwasher|34"H x 2'W x 24.5"D|
|Washer / Dryer|32"D x 54"W|
|Garage Door|8'W x 7'H|
|- [Walkin Shower1](walkinShower1.png)<br>- [Walkin Shower2](walkinShower2.png)<br>- [Walkin Shower3](walkinShower3.png)|8L' x 8'W|
|[Shower](https://encrypted-tbn3.gstatic.com/shopping?q=tbn:ANd9GcSAgN6OXokKmBdedOSsdARAVrvVb3LNzlRdqHUpi2-Wbd0cyaFUgja1XttmC_Z4p-IMgpAWlExD-esHWzKBBL1mYGPchPEVlS88JCPF6NskvbUK91jIaMDB)|4'W x 4'L|
