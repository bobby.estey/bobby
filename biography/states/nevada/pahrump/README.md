# Pahrump, Nevada

## Links

|Link|Description|
|--|--|
|[Mountains](https://gitlab.com/bobby.estey/bobby/-/blob/main/biography/states/nevada/pahrump/mountains/README.md)|- Mountains I have Climbed|
|[Signs](https://gitlab.com/bobby.estey/bobby/-/blob/main/biography/states/nevada/pahrump/signs/README.md)|- Signs I have seen while hiking in Pahrump, so **Hilarious**|
|[Vehicle Explosion 2023-12-01](https://www.youtube.com/watch?v=vfjzRnZvRDw)|- NV Hwy 372 and NV Hwy 160, just north of Walgreens<br>- Nugget Casino's Craps Table shook, doors rattled and everyone said, "What was that?"
|[Burros](https://rumble.com/v50m2cq-burros-dmv-pahrump-nv.html?e9s=src_v1_ucp)|- Burros at DMV|
|[Dog Honking 1](https://rumble.com/v48m9cq-dog-honking-horn-pahrump-nv-lakesize-casino-1.html?e9s=src_v1_ucp)|- Dog Honking Horn at Lakeside Casino 1|
|[Dog Honking 2](https://rumble.com/v48m9x3-dog-honking-horn-pahrump-nv-lakesize-casino-2.html?e9s=src_v1_ucp)|- Dog Honking Horn at Lakeside Casino 2|
|[Dogs Sleeping 1](https://rumble.com/v5awafp-dogs-asleep-on-the-job.html?e9s=src_v1_ucp)|- Dogs Sleeping on the Job 1|
|[Dogs Sleeping 2](https://rumble.com/v59ocwx-alert-dogs-not-dogs-asleep-on-the-job.html?e9s=src_v1_ucp)|- Dogs Sleeping on the Job 2|
|[Dogs Sleeping 3](https://rumble.com/v58mvx6-dogs-asleep-on-the-job-pahrump-nv.html?e9s=src_v1_ucp)|- Dogs Sleeping on the Job 3|
|[Network Outage](https://gitlab.com/bobby.estey/bobby/-/blob/main/biography/states/nevada/pahrump/pahrumpNV2023120807.png)|- Look Pahrump in the entire USA had a major Network Outage|

#### Balloon Landing

- Balloon that landed down the street from me - 20231229@0820 PT

|Link|Link|Link|
|--|--|--|
|[Balloon 1](https://rumble.com/v448syo-balloon-1-pahrump-nv-89060.html)|[Balloon 2](https://rumble.com/v448tbr-balloon-2-pahrump-nv-89060.html)|[Balloon 3](https://rumble.com/v448tko-balloon-3-pahrump-nv-89060.html)|
|[Balloon 4](https://rumble.com/v448u7n-balloon-4-pahrump-nv-89060.html)|[Balloon 5](https://rumble.com/v448ulu-balloon-5-pahrump-nv-89060.html)|[Balloon 6](https://rumble.com/v448ur8-balloon-6-pahrump-nv-89060.html)|
|[Balloon 7](https://rumble.com/v448uw9-balloon-7-pahrump-nv-89060.html)|[Balloon Descending](https://rumble.com/v448wof-balloon-descending-pahrump-nv-89060.html)|[Balloon Landed](https://rumble.com/v448xax-balloon-landing-pahrump-nv-89060.html)|
