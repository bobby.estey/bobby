# Shadow Mountain - Peak 1, Pahrump, NV 89060

## Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.287273|
|Longitude|-116.091853|
|Base Altitude|2600 ft|
|Summit Altitude|[3300 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

|Date|Time|Temperature (F)|Humidity (%)|Location|
|--|--|--|--|--|
|20240816|1630|103|17|Base of Mountain|
|20240816|1800|101|17|Top of Mountain|
|20240816|1950|98|17|Base of Mountain|

## Video Description

- First steep mountain climb in my life
- **Lessons Learned:  1) Harder going back down than going up 2) Never pull, Always push down on Rocks**
- I was on a scouting hike on a smaller mountain to figure out how to climb up the big guy (Shadow Mountain)
- **Last minute, decided to go all the way up**
- When I got to the top, I said to myself, **"I am not going to go back down the same way going up"**
- I went everywhere on the top and looking for an easier way down, all sides were CLIFFS
- I went down the same route going up, halfway, I found an easier route to go up next time
- **I WILL NEVER CLIMB THAT MOUNTAIN AGAIN**, was bad, real bad, real steep, most parts were 80+ degrees
- Limited Video, was getting dark fast and had to get off the Mountain

## Videos

- [Mountain Summit](https://rumble.com/v5bash1-pahrump-nv-mountain-south-of-shadow-mountain.html)
- [Mountain Summit - Very Top](https://rumble.com/v5basyl-pahrump-nv-mountain-south-of-shadow-mountain-very-top.html)

#### Climb

- [20240816T170628](https://rumble.com/v5jetbh-shadow-mountain-peak-1-pahrump-nv-89060-20240816t170628.html)
- [20240816T174029](https://rumble.com/v5jetgl-shadow-mountain-peak-1-pahrump-nv-89060-20240816t174029.html)
- [20240816T174148](https://rumble.com/v5jetnm-shadow-mountain-peak-1-pahrump-nv-89060-20240816t174148.html)

#### Summit

- [20240816T180142](https://rumble.com/v5jetxx-shadow-mountain-peak-1-pahrump-nv-89060-20240816t180142.html)
- [20240816T180430](https://rumble.com/v5jeu2d-shadow-mountain-peak-1-pahrump-nv-89060-20240816t180430.html)
- [20240816T180508](https://rumble.com/v5jeu7w-shadow-mountain-peak-1-pahrump-nv-89060-20240816t180508.html)
- [20240816T180611](https://rumble.com/v5jeukt-shadow-mountain-peak-1-pahrump-nv-89060-20240816t180611.html)
- [20240816T180844](https://rumble.com/v5jeuq5-shadow-mountain-peak-1-pahrump-nv-89060-20240816t180844.html)
- [20240816T181618](https://rumble.com/v5jev2t-shadow-mountain-peak-1-pahrump-nv-89060-20240816t181618.html)
- [20240816T181754](https://rumble.com/v5jev71-shadow-mountain-peak-1-pahrump-nv-89060-20240816t181754.html)
- [20240816T182115](https://rumble.com/v5jevc1-shadow-mountain-peak-1-pahrump-nv-89060-20240816t182115.html)
