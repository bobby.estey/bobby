# Shadow Mountain - Peak E, Pahrump, NV 89060

## Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.301676|
|Longitude|-116.090880|
|Base Altitude|3000 ft|
|Highest Point|[4240 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Easy Mountain to Climb
- Some areas were difficult
- Large Boulders, 

## Videos

# Climb Up

- [20241021T152657](https://rumble.com/v5jo5ka-shadow-mountain-peak-e-pahrump-nv-89060-20241021t152657.html)
- [20241021T153025](https://rumble.com/v5jo5ob-shadow-mountain-peak-e-pahrump-nv-89060-20241021t153025.html)
- [20241021GX010099](https://rumble.com/v5jmqvo-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx010099.html)
- [20241021GX010100 - Babby Rattlesnake @ 10:00](https://rumble.com/v5jmzdl-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx010100.html)
- [20241021GX010101](https://rumble.com/v5jn1i2-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx010101.html)
- [20241021GX010102](https://rumble.com/v5jn1x9-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx010102.html)

## Highest Point

- [20241021T163942](https://rumble.com/v5jo5sj-shadow-mountain-peak-e-pahrump-nv-89060-20241021t163942.html)
- [20241021T171220](https://rumble.com/v5jo5xp-shadow-mountain-peak-e-pahrump-nv-89060-20241021t171220.html)
- [20241021T171722](https://rumble.com/v5jo619-shadow-mountain-peak-e-pahrump-nv-89060-20241021t171722.html)
- [20241021GX010103](https://rumble.com/v5jn1zm-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx010103.html)
- [20241021GX010104 - Very Top - Really Steep](https://rumble.com/v5jn3vx-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx010104.html)
- [20241021GX010105](https://rumble.com/v5jo2n6-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx010105.html)

## Climb Down

- [20241021T172045](https://rumble.com/v5jo657-shadow-mountain-peak-e-pahrump-nv-89060-20241021t172045.html)
- [20241021T172317](https://rumble.com/v5jo6a5-shadow-mountain-peak-e-pahrump-nv-89060-20241021t172317.html)
- [20241021T172747](https://rumble.com/v5jo6du-shadow-mountain-peak-e-pahrump-nv-89060-20241021t172747.html)
- [20241021T173038](https://rumble.com/v5jo6gt-shadow-mountain-peak-e-pahrump-nv-89060-20241021t173038.html)
- [20241021T173504](https://rumble.com/v5jo6l9-shadow-mountain-peak-e-pahrump-nv-89060-20241021t173504.html)
- [20241021T181234](https://rumble.com/v5jo6qd-shadow-mountain-peak-e-pahrump-nv-89060-20241021t181234.html)
- [20241021T183900](https://rumble.com/v5jo6uj-shadow-mountain-peak-e-pahrump-nv-89060-20241021t183900.html)
- [20241021GX010106](https://rumble.com/v5jo2ve-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx010106.html)
- [20241021GX020099](https://rumble.com/v5jo3xx-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx020099.html)
- [20241021GX020101](https://rumble.com/v5jo5a9-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx020101.html)
