# Shadow Mountain - Peak 5, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.292798|
|Longitude|-116.114723|
|Base Altitude|3000 ft|
|Highest Point|[3280 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Never ascended to the Top
- The climb was nearly 80+ degrees

## Climb - 20240930

## Videos

#### Climb Up

- [20240930GX010023](https://rumble.com/v5iuez9-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010023.html)
- [20240930GX010025](https://rumble.com/v5iufgp-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010025.html)
- [20240930GX010026](https://rumble.com/v5iufqt-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010026.html)
- [20240930GX010027](https://rumble.com/v5iug2d-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010027.html)
- [20240930GX010028](https://rumble.com/v5iug8d-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010028.html)
- [20240930GX010029](https://rumble.com/v5iugk4-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010029.html)

#### Highest Point

- [20240930GX010030](https://rumble.com/v5iuh9x-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010030.html)

#### Climb Down

- [20240930GX010031](https://rumble.com/v5iuht5-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010031.html)
- [20240930GX010032](https://rumble.com/v5iuhxm-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010032.html)
- [20240930GX010033](https://rumble.com/v5iui6t-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010033.html)
- [20240930GX010034](https://rumble.com/v5iuicx-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010034.html)
- [20240930GX010035](https://rumble.com/v5iuikx-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010035.html)
- [20240930GX010036](https://rumble.com/v5iuivh-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010036.html)
- [20240930GX010037](https://rumble.com/v5iujfl-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010037.html)
- [20240930GX010038](https://rumble.com/v5iujph-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010038.html)
- [20240930GX010039](https://rumble.com/v5iuk2l-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010039.html)
