# Shadow Mountain - Peak 4, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.297569|
|Longitude|-116.089749|
|Base Altitude|3000 ft|
|Highest Point|[3320 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Never ascended to the Top
- The climb was nearly 80+ degrees
- Trying to find a route to the top of Shadow Mountain

## Videos

#### Climb - 20240928

###### Hike In

- [20240928GX010010](https://rumble.com/v5ixzbl-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010010.html)
- [20240928GX010011](https://rumble.com/v5iy02t-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010011.html)
- [20240928GX010012](https://rumble.com/v5iy0f7-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010012.html)
- [20240928GX010013](https://rumble.com/v5iy0sl-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010013.html)
- [20240928GX010014](https://rumble.com/v5iy11x-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010014.html)
- [20240928T131535](https://rumble.com/v5iytt9-shadow-mountain-peak-4-pahrump-nv-89060-20240928t131535.html)

###### Climbing

- [20240928GX010015](https://rumble.com/v5iy1n8-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010015.html)
- [20240928GX010016](https://rumble.com/v5iy28y-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010016.html)
- [20240928GX010017](https://rumble.com/v5iy3ct-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010017.html)
- [20240928GX010018](https://rumble.com/v5iy6cs-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010018.html)
- [20240928T142844](https://rumble.com/v5iyu39-shadow-mountain-peak-4-pahrump-nv-89060-20240928t142844.html)
- [20240928T144513](https://rumble.com/v5iyu91-shadow-mountain-peak-4-pahrump-nv-89060-20240928t144513.html)
- [20240928T145149](https://rumble.com/v5iyuj9-shadow-mountain-peak-4-pahrump-nv-89060-20240928t145149.html)

###### Highest Point

- [20240928GX010019](https://rumble.com/v5iyohe-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010019.html)

###### Going Down

- [20240928GX010020](https://rumble.com/v5iys1o-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010020.html)
- [20240928GX010021](https://rumble.com/v5iyt6c-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010021.html)
- [20240928GX010022](https://rumble.com/v5iytmd-shadow-mountain-peak-4-pahrump-nv-89060-20240928gx010022.html)
- [20240928T145753](https://rumble.com/v5iyure-shadow-mountain-peak-4-pahrump-nv-89060-20240928t145753.html)
- [20240928T151356](https://rumble.com/v5iyuzo-shadow-mountain-peak-4-pahrump-nv-89060-20240928t151356.html)
- [20240928T153937](https://rumble.com/v5iyv3w-shadow-mountain-peak-4-pahrump-nv-89060-20240928t153937.html)
