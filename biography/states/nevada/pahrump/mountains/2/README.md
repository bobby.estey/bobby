# Shadow Mountain - Peak 2, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.291767|
|Longitude|-116.089148|
|Base Altitude|3000 ft|
|Summit Altitude|[3200 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Easy Mountain to Climb

## Climb - 20240919

## Videos

#### Hike In

- [20240919_161028](https://rumble.com/v5j8sx1-shadow-mountain-peak-2-pahrump-nv-89060-20240919-161028.html)

#### Highest Point

- [20240919_170157](https://rumble.com/v5j8t65-shadow-mountain-peak-2-pahrump-nv-89060-20240919-170157.html)
- [20240919_171857](https://rumble.com/v5j8the-shadow-mountain-peak-2-pahrump-nv-89060-20240919-171857.html)

#### Heading Down

- [20240919_173047](https://rumble.com/v5j8tm9-shadow-mountain-peak-2-pahrump-nv-89060-20240919-173047.html)
- [20240919_173430](https://rumble.com/v5j8tst-shadow-mountain-peak-2-pahrump-nv-89060-20240919-173430.html)

#### Hike Back

- [20240919_173600](https://rumble.com/v5j8tz8-shadow-mountain-peak-2-pahrump-nv-89060-20240919-173600.html)
- [20240919_180226](https://rumble.com/v5j8u3p-shadow-mountain-peak-2-pahrump-nv-89060-20240919-180226.html)
- [20240919_180341](https://rumble.com/v5j8u7e-shadow-mountain-peak-2-pahrump-nv-89060-20240919-180341.html)
