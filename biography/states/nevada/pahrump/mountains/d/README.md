# Shadow Mountain - Peak D, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.292777|
|Longitude|-116.114733|
|Base Altitude|2880 ft|
|Summit Point|[3120 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Easy Mountains to Climb

#### Climb - 20240930

- [20240930GX010040](https://rumble.com/v5iukap-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010040.html)
- [20240930GX010041](https://rumble.com/v5iukyq-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010041.html)
- [20240930GX010042](https://rumble.com/v5iulhh-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010042.html)
- [20240930GX010043](https://rumble.com/v5iulq5-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010043.html)
- [20240930GX010044](https://rumble.com/v5iulwr-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010044.html)
- [20240930GX010045](https://rumble.com/v5ium1o-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010045.html)
- [20240930GX010046](https://rumble.com/v5iuoz7-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010046.html)
- [20240930GX010047](https://rumble.com/v5iurob--shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010047.html)
- [20240930GX010048](https://rumble.com/v5iuuii-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010048.html)
- [20240930GX020046]()
- [20240930GX020048](https://rumble.com/v5iuv1p-shadow-mountain-peak-5-pahrump-nv-89060-20240930gx010048.html)
- [20240930T164531](https://rumble.com/v5iuvi9-shadow-mountain-peak-5-pahrump-nv-89060-20240930t164531.html)
- [20240930T165440](https://rumble.com/v5iuw4x-shadow-mountain-peak-5-pahrump-nv-89060-20240930t165440.html)

#### Climb - 20241002

- [Summit View 1](https://rumble.com/v5ibavm-shadow-mountain-peak-d-pahrump-nv-89060-20241002t163628.html)
- [Summit View 2](https://rumble.com/v5ibb3p-shadow-mountain-peak-d-pahrump-nv-89060-20241002t164251.html)
- [Summit View 3](https://rumble.com/v5ibbat-shadow-mountain-peak-d-pahrump-nv-89060-20241002t164757.html)
- [Summit View 4](https://rumble.com/v5ibbgz-shadow-mountain-peak-d-pahrump-nv-89060-20241002t165049.html)
- [Summit View 5](https://rumble.com/v5ibbnp-shadow-mountain-peak-d-pahrump-nv-89060-20241002t170634.html)
