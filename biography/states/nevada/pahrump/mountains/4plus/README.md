# Shadow Mountain - Peak 4+, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.299391|
|Longitude|-116.089947|
|Base Altitude|3000 ft|
|Highest Point|[3800 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Never ascended to the Top
- The climb was nearly 80+ degrees
- Trying to find a route to the top of Shadow Mountain

#### Videos

#### Climb - 20240922

###### Climbing

- [20240922GX010110](https://rumble.com/v5j8crh-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010110.html)
- [20240922GX010111](https://rumble.com/v5j8koy-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010111.html)
- [20240922GX010112](https://rumble.com/v5j8l35-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010112.html)
- [20240922GX010113](https://rumble.com/v5j8lgt-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010113.html)

###### Highest Point

- [20240922GX010114](https://rumble.com/v5j8lrh-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010114.html)
- [20240922GX010115](https://rumble.com/v5j8m4d-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010115.html)
- [20240922GX010116](https://rumble.com/v5j8m9h-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010116.html)
- [20240922GX010117](https://rumble.com/v5j8mid-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010116.html)
- [20240922GX010118](https://rumble.com/v5j8mx1-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010118.html)
- [20240922GX010119](https://rumble.com/v5j8mzp-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010119.html)
- [20240922T151538](https://rumble.com/v5j8ofx-shadow-mountain-peak-4-pahrump-nv-89060-20240922t151538.html)

###### Going Down

- [20240922GX010120](https://rumble.com/v5j8n2t-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010120.html)
- [20240922GX010121](https://rumble.com/v5j8god-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010121.html)
- [20240922T160226](https://rumble.com/v5j8ooj-shadow-mountain-peak-4-pahrump-nv-89060-20240922t160226.html)

###### Hike Back

- [20240922GX010122](https://rumble.com/v5j8kak-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010122.html)
- [20240922GX010123](https://rumble.com/v5j8nac-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010123.html)
- [20240922GX010124](https://rumble.com/v5j8npv-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010124.html)
- [20240922GX010125](https://rumble.com/v5j8nud-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx010125.html)
- [20240922GX020121](https://rumble.com/v5j8o8l-shadow-mountain-peak-4-pahrump-nv-89060-20240922gx020121.html)
