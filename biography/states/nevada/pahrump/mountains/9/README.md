# Shadow Mountain - Peak 9, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.305802|
|Longitude|-116.110760|
|Base Altitude|3000 ft|
|Summit Point|[3360 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Easy Mountains to Climb

## Climb - 20240916

## Videos

#### Hike In

- [20240916GX010041](https://rumble.com/v5jagib-shadow-mountain-peak-9-pahrump-nv-89060-20240916gx010041.html)
- [20240916GX010042](https://rumble.com/v5jajol-shadow-mountain-peak-9-pahrump-nv-89060-20240916gx010042.html)

#### Canyon

- [20240916GX010043](https://rumble.com/v5jankz-shadow-mountain-peak-9-pahrump-nv-89060-20240916gx010043.html)
- [20240916GX010044](https://rumble.com/v5jaoea-shadow-mountain-peak-9-pahrump-nv-89060-20240916gx010044.html)
- [20240916GX010045](https://rumble.com/v5jaojm-shadow-mountain-peak-9-pahrump-nv-89060-20240916gx010045.html)

#### Hike Back

- [20240916GX010046](https://rumble.com/v5jaskb-shadow-mountain-peak-9-pahrump-nv-89060-20240916gx010046.html)
- [20240916GX020043](https://rumble.com/v5jaw90-shadow-mountain-peak-9-pahrump-nv-89060-20240916gx020043.html)
- [20240916GX020046](https://rumble.com/v5jbed6-shadow-mountain-peak-9-pahrump-nv-89060-20240916gx020046.html)
- [20240916GX030043](https://rumble.com/v5jbgyt-shadow-mountain-peak-9-pahrump-nv-89060-20240916gx030043.html)
