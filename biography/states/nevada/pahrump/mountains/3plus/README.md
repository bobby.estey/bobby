# Shadow Mountain - Peak 3+, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.292798|
|Longitude|-116.114723|
|Base Altitude|3000 ft|
|Highest Point|[3280 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Easy Mountain to Climb

## Climb - 20240924

## Videos

#### Hike In

- [20240917GX010048](https://rumble.com/v5j8xap-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010048.html)
- [20240917GX010049](https://rumble.com/v5j8xqd-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010049.html)
- [20240917GX010050](https://rumble.com/v5j8z8t-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010050.html)
- [20240917GX010051](https://rumble.com/v5j92f9-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010051.html)

#### Climbing

- [20240917GX010052](https://rumble.com/v5j931h-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010052.html)
- [20240917GX010053](https://rumble.com/v5j93sb-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010053.html)
- [20240917GX010054](https://rumble.com/v5j954s-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010054.html)
- [20240917GX010055](https://rumble.com/v5ja8po-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010055.html)
- [20240917GX020051](https://rumble.com/v5j9b7g-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx020051.html)
- [20240917GX030051](https://rumble.com/v5jabw2-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx030051.html)
- [20240917T161449](https://rumble.com/v5jabzx-shadow-mountain-peak-3-pahrump-nv-89060-20240917t161449.html)
- [20240917T162430](https://rumble.com/v5jac56-shadow-mountain-peak-3-pahrump-nv-89060-20240917t162430.html)
- [20240917T162905](https://rumble.com/v5jace5-shadow-mountain-peak-3-pahrump-nv-89060-20240917t162905.html)
- [20240917T165325](https://rumble.com/v5jacn8-shadow-mountain-peak-3-pahrump-nv-89060-20240917t165325.html)

#### Highest Point

- [20240917GX010056](https://rumble.com/v5jaa6p-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010056.html)
- [20240917GX010057](https://rumble.com/v5jaafh-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010057.html)
- [20240917GX010058](https://rumble.com/v5jaajl-shadow-mountain-peak-3-pahrump-nv-89060-20240917gx010058.html)
- [20240917T170342](https://rumble.com/v5jacrh-shadow-mountain-peak-3-pahrump-nv-89060-20240917t170342.html)
- [20240917T171313](https://rumble.com/v5jacw3-shadow-mountain-peak-3-pahrump-nv-89060-20240917t171313.html)

#### Heading Down

- [20240917T173756](https://rumble.com/v5jad1x-shadow-mountain-peak-3-pahrump-nv-89060-20240917t173756.html
