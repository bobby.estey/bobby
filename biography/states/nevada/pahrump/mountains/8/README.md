# Shadow Mountain - Peak 8, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.309987|
|Longitude|-116.102508|
|Base Altitude|3040 ft|
|Highest Point|[3320 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Nothing Climbed
- This was a hiking expedition, looking for another route to the Top

## Hike - 20240911

## Videos

#### Hike In

- [20240911GX010036](https://rumble.com/v5jbqpp-shadow-mountain-peak-8-pahrump-nv-89060-20240911gx010036.html)
- [20240911GX020036](https://rumble.com/v5jbuah-shadow-mountain-peak-8-pahrump-nv-89060-20240911gx020036.html)
- [20240911GX030036](https://rumble.com/v5jcxsl-shadow-mountain-peak-8-pahrump-nv-89060-20240911gx030036.html)

#### In the Canyon

- [20240911T154157](https://rumble.com/v5jcyus-shadow-mountain-peak-8-pahrump-nv-89060-20240911t154157.html)
- [20240911T155152](https://rumble.com/v5jcyyl-shadow-mountain-peak-8-pahrump-nv-89060-20240911t155152.html)
- [20240911T155418](https://rumble.com/v5jcz9h-shadow-mountain-peak-8-pahrump-nv-89060-20240911t155418.html)
- [20240911T155657](https://rumble.com/v5jczg3-shadow-mountain-peak-8-pahrump-nv-89060-20240911t155657.html)
- [20240911T155710](https://rumble.com/v5jcznp-shadow-mountain-peak-8-pahrump-nv-89060-20240911t155710.html)
- [20240911T160117](https://rumble.com/v5jczpu-shadow-mountain-peak-8-pahrump-nv-89060-20240911t160117.html)
- [20240911T160708](https://rumble.com/v5jczs5-shadow-mountain-peak-8-pahrump-nv-89060-20240911t160708.html)
- [20240911T161101](https://rumble.com/v5jczut-shadow-mountain-peak-8-pahrump-nv-89060-20240911t161101.html)
- [20240911T161830](https://rumble.com/v5jczxp-shadow-mountain-peak-8-pahrump-nv-89060-20240911t161830.html)
- [20240911T162246](https://rumble.com/v5jd00l-shadow-mountain-peak-8-pahrump-nv-89060-20240911t162246.html)
- [20240911T162436](https://rumble.com/v5jd02z-shadow-mountain-peak-8-pahrump-nv-89060-20240911t162436.html)
- [20240911T163604](https://rumble.com/v5jd06i-shadow-mountain-peak-8-pahrump-nv-89060-20240911t163604.html)
- [20240911T164104](https://rumble.com/v5jd07x-shadow-mountain-peak-8-pahrump-nv-89060-20240911t164104.html)
