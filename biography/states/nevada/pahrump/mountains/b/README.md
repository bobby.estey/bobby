# Shadow Mountain - Peak B, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.291687|
|Longitude|-116.124130|
|Base Altitude|3000 ft|
|Highest Point|[3480 ft](elevation.png)|
|Route 1 Terrain|[Route 1 Terrain](route1Terrain.png)|
|Route 1 View|[Route 1 View](route1View.png)|
|Route 2 Terrain|[Route 2 Terrain](route2Terrain.png)|
|Route 2 View|[Route 2 View](route2View.png)|

## Video Description

- Easy Mountain to Climb if using the easier routes
- There are some routes, extremely steep and dangerous

#### First Climb - 20241006

- [Summit View 1](https://rumble.com/v5i99v6--shadow-mountain-peak-b-pahrump-nv-89060-20241006gx010060.html)
- [Summit View 2](https://rumble.com/v5i9d39-shadow-mountain-peak-b-pahrump-nv-89060-20241006t153709.html)
- [Summit View 3](https://rumble.com/v5i9ebd-shadow-mountain-peak-b-pahrump-nv-89060-20241006t153927.html)

###### Videos

- [Start](https://rumble.com/v5i971x-shadow-mountain-peak-b-pahrump-nv-89060-20241006gx010057.html)
- [Half Way Up](https://rumble.com/v5i989x-shadow-mountain-peak-b-pahrump-nv-89060-20241006gx010058.html)
- [Lower Part of Mountain Top](https://rumble.com/v5i9930-shadow-mountain-peak-b-pahrump-nv-89060-20241006gx010059.html)
- [Lower Part of Mountain Top](https://rumble.com/v5i9dil-shadow-mountain-peak-b-pahrump-nv-89060-20241006t150949.html)
- [Heading Down](https://rumble.com/v5i9aql-shadow-mountain-peak-b-pahrump-nv-89060-20241006gx010061.html)
- [Heading Down](https://rumble.com/v5i9b21-shadow-mountain-peak-b-pahrump-nv-89060-20241006gx010062.html)
- [Heading Down](https://rumble.com/v5i9b7p-shadow-mountain-peak-b-pahrump-nv-89060-20241006gx010063.html)
- [Heading Down](https://rumble.com/v5i9cnv-shadow-mountain-peak-b-pahrump-nv-89060-20241006gx010068.html)
- [Heading Down](https://rumble.com/v5i9csl-shadow-mountain-peak-b-pahrump-nv-89060-20241006gx010069.html)
- [Heading Down](https://rumble.com/v5i9emi-shadow-mountain-peak-b-pahrump-nv-89060-20241006t163740.html)
- [Heading Down](https://rumble.com/v5i9f39-shadow-mountain-peak-b-pahrump-nv-89060-20241006t164700.html)

#### Second Climb - 20241007

- [Summit View 1](https://rumble.com/v5i819l-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010082.html)
- [Summit View 2](https://rumble.com/v5i81m1-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010083.html)
- [Summit View 3](https://rumble.com/v5i8bgd-shadow-mountain-peak-b-pahrump-nv-89060-20241007t163526.html)
- [Summit View 4](https://rumble.com/v5i8bkz-shadow-mountain-peak-b-pahrump-nv-89060-20241007t163700.html)
- Made a U turn at the beginning, walked into a 80 degree incline, rerouted
     - [U Turn](https://rumble.com/v5i7xt6-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010075.html)
- Almost complete with the hike, encounter a 60 foot cliff at the end of the wash, rerouted backup and around the tip
     - [60 Foot Cliff](https://rumble.com/v5i88zx-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010089.html)

###### Videos

- [Start](https://rumble.com/v5i7mo5-hiking-shadow-mountain-pahrump-nv-89060-20241007gx010073.html)
- [Start](https://rumble.com/v5i7w1h-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010074.html)
- [Reroute](https://rumble.com/v5i7y0j-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010076.html)
- [Caves on First Peak](https://rumble.com/v5i7ygk-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010077.html)
- [Caves on First Peak](https://rumble.com/v5i7ytp-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010078.html)
- [More Climbing](https://rumble.com/v5i7znl-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010079.html)
- [First Peak](https://rumble.com/v5i8b6q-shadow-mountain-peak-b-pahrump-nv-89060-20241007t155340.html)
- [Second Peak](https://rumble.com/v5i804c-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010080.html)
- [Second Peak](https://rumble.com/v5i80e3-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010081.html)
- [Heading Down](https://rumble.com/v5i81to-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010084.html)
- [Heading Down](https://rumble.com/v5i820t-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010085.html)
- [Heading Down - Showing First Climb Route](https://rumble.com/v5i828l-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010086.html)
- [Heading Down](https://rumble.com/v5i82lm-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010087.html)
- [Heading Down - Wash](https://rumble.com/v5i88n0-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010088.html)
- [Finished Climb](https://rumble.com/v5i89aj-shadow-mountain-peak-b-pahrump-nv-89060-20241007gx010090.html)
- [Finished Climb](https://rumble.com/v5i8bt9-shadow-mountain-peak-b-pahrump-nv-89060-20241007t174912.html)
