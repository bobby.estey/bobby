# Shadow Mountain - Peak 7, Pahrump, NV 89060

- Pahrump, NV - Mountain Summit - North of Corbin Street and Bell Vista Avenue, Pahrump, NV 89060

## Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.304785|
|Longitude|-116.099670|
|Base Altitude|2700 ft|
|Summit Altitude|3950 ft|
|Image Shadow Mountain 2-1|[imageShadowMountain2-1](imageShadowMountain2-1.png)|
|Image Shadow Mountain 2-2|[imageShadowMountain2-2](imageShadowMountain2-2.png)|
|Route View Part 1|[Route View Part 1](routeShadowMountain2-1.png)|
|Route View Part 2|[Route View Part 2](routeShadowMountain2-2.png)|

#### [Weather](https://weatherspark.com/h/d/2230/2024/9/10/Historical-Weather-on-Tuesday-September-10-2024-in-Pahrump-Nevada-United-States)

|Date|Time|Temperature (F)|Humidity (%)|Location|
|--|--|--|--|--|
|20240910|1420|101|7|Base of Mountain|
|20240910|1700|100|7|Top of Mountain|
|20240910|1930|96|7|Base of Mountain|

## Video Description

- I call this Mountain Shadow 2 - secondary Mountain to Shadow
- This was an easy climb, not to steep
- Still takes a lot of energy going up and down the mountain

## Videos

#### First Climb - 20240907 - Just Below Shadow 2

- [20240907T135811](https://rumble.com/v5jdb7x-shadow-mountain-peak-7-pahrump-nv-89060-20240907t135811.html)
- [20240907T143030](https://rumble.com/v5jdbcd-shadow-mountain-peak-7-pahrump-nv-89060-20240907t143030.html)
- [20240907T143516](https://rumble.com/v5jdbgt-shadow-mountain-peak-7-pahrump-nv-89060-20240907t143516.html)
- [20240907T145010](https://rumble.com/v5jdbkd-shadow-mountain-peak-7-pahrump-nv-89060-20240907t145010.html)
- [20240907T151052](https://rumble.com/v5jdbor-shadow-mountain-peak-7-pahrump-nv-89060-20240907t151052.html)
- [20240907T151346](https://rumble.com/v5jdbs5-shadow-mountain-peak-7-pahrump-nv-89060-20240907t151346.html)
- [20240907T154603](https://rumble.com/v5jdbzd-shadow-mountain-peak-7-pahrump-nv-89060-20240907t154603.html)

#### Second Climb - 20240910

###### Climb Up

- [20240910T151802](https://rumble.com/v5jd47x-shadow-mountain-peak-7-pahrump-nv-89060-20240910t151802.html)
- [20240910T152332](https://rumble.com/v5jd4ct-shadow-mountain-peak-7-pahrump-nv-89060-20240910t152332.html)
- [20240910T153535](https://rumble.com/v5jd4gx-shadow-mountain-peak-7-pahrump-nv-89060-20240910t153535.html)
- [20240910T160037](https://rumble.com/v5jd4le-shadow-mountain-peak-7-pahrump-nv-89060-20240910t154029.html)
- [20240910T164043](https://rumble.com/v5jd584-shadow-mountain-peak-7-pahrump-nv-89060-20240910t164043.html)

###### Summit

- [20240910T165423](https://rumble.com/v5jd5bx-shadow-mountain-peak-7-pahrump-nv-89060-20240910t165423.html)
- [20240910T171550](https://rumble.com/v5jd5e5-shadow-mountain-peak-7-pahrump-nv-89060-20240910t171550.html)
- [20240910T171637](https://rumble.com/v5jd5pp-shadow-mountain-peak-7-pahrump-nv-89060-20240910t171637.html)
- [20240910T172654](https://rumble.com/v5jd5sx-shadow-mountain-peak-7-pahrump-nv-89060-20240910t172654.html)

###### Climb Down

- [20240910T172829](https://rumble.com/v5jd5xx-shadow-mountain-peak-7-pahrump-nv-89060-20240910t172829.html)
- [20240910T172915](https://rumble.com/v5jd5zd-shadow-mountain-peak-7-pahrump-nv-89060-20240910t172915.html)
- [20240910T175013](https://rumble.com/v5jd62h-shadow-mountain-peak-7-pahrump-nv-89060-20240910t175013.html)
- [20240910T181116](https://rumble.com/v5jd6vp-shadow-mountain-peak-7-pahrump-nv-89060-20240910t181116.html)
- [20240910T183831](https://rumble.com/v5jd6yc-shadow-mountain-peak-7-pahrump-nv-89060-20240910t183831.html)
- [20240910T191758](https://rumble.com/v5jd729-shadow-mountain-peak-7-pahrump-nv-89060-20240910t191758.html)
