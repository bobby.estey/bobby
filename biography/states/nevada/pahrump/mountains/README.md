# Pahrump, NV 89060 - Northern Mountains

When we moved to Pahrump, NV there are several mountains near by.  Almost everyday I hike or climb the mountains.

|Extreme Videos / Key|Map|
|--|--|
|- [Mountain 1](https://rumble.com/v5bash1-pahrump-nv-mountain-south-of-shadow-mountain.html)<br>- [Mountain 4+](https://rumble.com/v5j8ofx-shadow-mountain-peak-4-pahrump-nv-89060-20240922t151538.html)<br>- [Mountain E](https://rumble.com/v5jn3vx-shadow-mountain-peak-e-pahrump-nv-89060-20241021gx010104.html)<br>- [Mountain E Next to Summit](https://rumble.com/v5jo5xp-shadow-mountain-peak-e-pahrump-nv-89060-20241021t171220.html)<hr><b>Description</b><br>- 1 Easy (30 Degrees)<br>- 5 Extreme (80+ Degrees)<br><br><b>Color</b><br>- Red (Summit not achieved)<br>- Yellow (Not a Climb, only a Hike)<br>- Green (Summit achieved)|![Peak Map](peakMap.png)|

|Date|Detail Links|Difficulty|Description|
|--|--|--|--|
|20240815|[Mountain 0](./0/README.md)|1|- First attempt to find a route to peak of Shadow Mountain<br>- Easy Climb until I got to a steep wall<br>- Almost 90 degrees cliff<br>- Could not continue further|
|20240816|[Mountain 1](./1/README.md)|5|- First steep mountain climb in my life<br>- **Lessons Learned:  1) Harder going back down than going up 2) Never pull, Always push down on Rocks**<br>- I was on a scouting hike on a smaller mountain to figure out how to climb up the big guy (Shadow Mountain)<br>- **Last minute, decided to go all the way up**<br>- When I got to the top, I said to myself, **"I am not going to go back down the same way going up"**<br>- I went everywhere on the top and looking for an easier way down, all sides were CLIFFS<br>- I went down the same route going up, halfway, I found an easier route to go up next time<br>- **I WILL NEVER CLIMB THAT MOUNTAIN AGAIN**, was bad, real bad, real steep, most parts were 80+ degrees<br>- Limited Video, was getting dark fast and had to get off the Mountain|
|20240919|[Mountain 2](./2/README.md)|1|- Easy Mountain to Climb|
|20240924|[Mountain 3](./3/README.md)|3|- Easy Mountain to Climb|
|20240917|[Mountain 3+](./3plus/README.md)|3|- Easy Mountain to Climb|
|20240928|[Mountain 4](./4/README.md)|5|- Never ascended to the Top<br>- The climb was nearly 80+ degrees<br>- Trying to find a route to the top of Shadow Mountain|
|20240922|[Mountain 4+](./4plus/README.md)|5|- Never ascended to the Top<br>- The climb was nearly 80+ degrees<br>- Trying to find a route to the top of Shadow Mountain|
|20240929|[Mountain 5](./5/README.md)|5|- Never ascended to the Top|
|20240000|[Mountain 6](./6/README.md)|3|- Never ascended to the Top|
|20240907/10|[Mountain 7](./7/README.md)|2|- Easy Mountain to Climb|
|20240911|[Hike Canyon 8](./8/README.md)|0|- Nothing Climbed<br>- This was a hiking expedition, looking for another route to the Top|
|20240916|[Mountain 9](./9/README.md)|1|- Easy Mountains to Climb|
|20241006|[Mountain B](./b/README.md)|2|- Easy if using the easier routes<br>- There are some routes, extremely steep and dangerous|
|20240930|[Mountain D](./d/README.md)|2|- Easy Mountains to Climb|
|20241021|[Mountain E](./e/README.md)|3 / 5|- Easy Mountain to Climb<br>- Some areas were difficult<br>- The summit was a 5, very steep almost 90 degrees in some areas<br>- Very hard to climb the summit part and if I slipped, would be over<br>- Narrow at the top about two feet wide<br>- Coming down was very difficult|
