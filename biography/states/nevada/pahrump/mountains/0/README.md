# Shadow Mountain - Peak 0, Pahrump, NV 89060

## Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.297459|
|Longitude|-116.079132|
|Base Altitude|2720 ft|
|Highest Point|[2920 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- First attempt to find a route to peak of Shadow Mountain
- Easy Climb until I got to a steep wall
- Almost 90 degrees cliff
- Could not continue further

## Videos

# Climb Up

- [20240815T151203](https://rumble.com/v5jfhr5-shadow-mountain-peak-0-pahrump-nv-89060-20240815t151203.html)
- [20240815T153233](https://rumble.com/v5jfkg1-shadow-mountain-peak-0-pahrump-nv-89060-20240815t153233.html)

## Highest Point

- [20240815T154112](https://rumble.com/v5jfklf-shadow-mountain-peak-0-pahrump-nv-89060-20240815t154112.html)
- [20240815T154425](https://rumble.com/v5jfkr9-shadow-mountain-peak-0-pahrump-nv-89060-20240815t154425.html)

## Climb Down

- [20240815T160027](https://rumble.com/v5jfkua-20240815t160027.html)
- [20240815T161833](https://rumble.com/v5jfl05-shadow-mountain-peak-0-pahrump-nv-89060-20240815t161833.html)
- [20240815T163306](https://rumble.com/v5jfl4l-shadow-mountain-peak-0-pahrump-nv-89060-20240815t163306.html)
- [20240815T164310](https://rumble.com/v5jfl9h-shadow-mountain-peak-0-pahrump-nv-89060-20240815t164310.html)
- [20240815T164835](https://rumble.com/v5jflfm-shadow-mountain-peak-0-pahrump-nv-89060-20240815t164835.html)
- [20240815T165547](https://rumble.com/v5jflld-shadow-mountain-peak-0-pahrump-nv-89060-20240815t165547.html)
