# Shadow Mountain - Peak 3, Pahrump, NV 89060

# Statistics / Images

|Key|Value|
|--|--|
|Latitude|36.299465|
|Longitude|-116.086764|
|Base Altitude|3000 ft|
|Highest Point|[3600 ft](elevation.png)|
|View|[View](view.png)|
|Route|[Route](route.png)|

## Video Description

- Easy Mountain to Climb

## Climb - 20240924

## Videos

#### Hike In

- [20240924GX010001](https://rumble.com/v5iyzgx-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx010001.html)
- [20240924GX010002](https://rumble.com/v5iz2x1-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx010002.html)

#### Climbing

- [20240924GX010003](https://rumble.com/v5iz4k2-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx010003.html)
- [20240924GX010004](https://rumble.com/v5iz542-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx010004.html)
- [20240924GX010005](https://rumble.com/v5iz5p1-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx010005.html)
- [20240924GX010006](https://rumble.com/v5iz88c-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx010006.html)
- [20240924GX010007](https://rumble.com/v5izajx-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx010007.html)
- [20240924GX010008](https://rumble.com/v5izb2c-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx010008.html)
- [20240924T162544](https://rumble.com/v5izgxp-shadow-mountain-peak-2-pahrump-nv-89060-20240924t162544.html)

#### Highest Point

- [20240924GX010009](https://rumble.com/v5ize56-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx010009.html)
- [20240924GX020002](https://rumble.com/v5izem9-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx020002.html)
- [Possible Route up to the Top - 20240924T164852](https://rumble.com/v5izijg-possible-route-up-to-the-top-20240924t164852.html)

#### Heading Down

- [20240924GX020009](https://rumble.com/v5izg4l-shadow-mountain-peak-2-pahrump-nv-89060-20240924gx020009.html)
- [20240924T172340](https://rumble.com/v5izhb1-shadow-mountain-peak-2-pahrump-nv-89060-20240924t172340.html)
- [20240924T173030](https://rumble.com/v5izhw9-shadow-mountain-peak-2-pahrump-nv-89060-20240924t173030.html)
- [20240924T174043](https://rumble.com/v5izi1x-shadow-mountain-peak-2-pahrump-nv-89060-20240924t174043.html)
