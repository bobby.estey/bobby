# [Beatty, Nevada  89003](https://en.wikipedia.org/wiki/Beatty,_Nevada)

![beatty](./images/beattys.png)

# Wild Donkeys all over the town

![1](./images/1s.png)
![2](./images/2s.png)
![3](./images/3s.png)
![4](./images/4s.png)

# Videos

- [1 - feeding outside behind car](https://rumble.com/v2vngpq-donkeys-beatty-nv-us-95-20230619-1.html)
- [2 - driver side](https://rumble.com/v2vngv6-donkeys-beatty-nv-us-95-20230619-2.html)
- [3 - driver side](https://rumble.com/v2vnhm0-donkeys-beatty-nv-us-95-20230619-3.html)
- [4 - passenger side](https://rumble.com/v2vnken-donkeys-beatty-nv-us95-4.html)
- [5 - driver side](https://rumble.com/v35tx88-donkeys-beatty-nv-us95-5.html)
