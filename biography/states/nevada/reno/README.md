## [Reno, NV](https://en.wikipedia.org/wiki/Jesse_L._Reno)

- Reno is approximately 1 hour northwest of Dayton with two main routes, either through Carson City or Virginia City
- Reno is no longer "The Biggest Little City In The World".  Reno's population is 500,000.  The main street is Virginia Street
- 2022 September 18 we went to the [Reno Air Races](https://airrace.org) near the end of the day, a jet crashed and killed the pilot.  Since that accident, Reno Air Races announced that 2023 will be the last year of air racing in Reno.  When we lived in [Mesa, AZ](https://en.wikipedia.org/wiki/Mesa,_Arizona) there were two years of Air Racing and then the population around the former [Williams Air Force Base](https://en.wikipedia.org/wiki/Williams_Air_Force_Base) increased, they cancelled future events due to safety concerns

![Video](../videoIcon.jpg) [Flight from Reno to Denver](https://rumble.com/v28q6jg-flight-reno-rno-to-denver-dia-lake-tahoe-carson-city-dayton-nevada.html)
- 00:00 - Take off from Reno (RNO), Runway 35L - 17R, south bound, 7500 feet from start to in the air
- 00:50 - 01:25 (35 seconds) 
- 01:40 - I580 visible
- 04:10 - Lake Tahoe
- 04:40 - Carson City, NV (State Capitol) 
- 05:00 - Mound House, NV The flight is directly over somewhere near Virginia City, NV at this time
- 05:30 - Dayton, NV. I accidentally stopped the video, no worries, I continued from Dayton, NV to Virginia Beach, NV in a second recording, located under Dayton, NV
