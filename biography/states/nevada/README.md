# ![State Flag](../../nevada.png) Nevada (NV) - 31 October 1864 - 36th State

[TOC]

# Background
[2774 Miles - Hayes, VA -> Verdi, NV](./2774miles.png)

Ester and I arrived in [Verdi, NV](https://en.wikipedia.org/wiki/Verdi,_Nevada) west of [Reno, NV](./reno/reno.md) on 28 March 2022 from a long cross country move from [Hayes, Virginia](https://en.wikipedia.org/wiki/Hayes,_Virginia) which started on 25 March 2022 and we stayed at Boomtown Hotel / Casino, our original plans were to live in either [Minden, NV](https://en.wikipedia.org/wiki/Minden,_Nevada) or [Gardnerville, NV](https://en.wikipedia.org/wiki/Gardnerville,_Nevada)

We stayed in Verdi for 3 nights (Checkin 28 March 2022 - Checkout 31 March 2022) and then lived temporarily in [Carson City, NV](https://en.wikipedia.org/wiki/Carson_City,_Nevada) in a Motel up to 09 April 2022.  Bobby continued to work remotely and after work or weekends, we would familiarize ourselves with our new location:  Reno, Carson City and other cities and towns.  After a lot of exploring we decided to rent a home in [Dayton, NV](./dayton/dayton.md) and moved in on 09 April 2022

# Geography - There is a thing about Virginia out here
![Nevada](./nevada50x50.png)

## [Dayton, Nevada](./dayton/README.md) - Where we Lived

## [Reno, NV](./reno/README.md) - No longer "The Biggest Little City In The World"

## Carson City, NV
- Carson City the Capitol is 12 miles west of Dayton
- Named after [Kit Carson](https://en.wikipedia.org/wiki/Kit_Carson)
- Carson City is a small city, surrounded by Mountains, very clean and with a variety of entertainment

## [Virginia City, NV](./virginiaCity/README.md) - Still the Wild West

## [Virginia Beach, NV](https://www.google.com/maps/dir//Virginia+Beach,+Silver+Springs,+Nevada/@39.4110762,-119.2311872,7372m/data=!3m1!1e3!4m8!4m7!1m0!1m5!1m1!1s0x8098f13c8e8e4663:0x6e12dcc8368a0db3!2m2!1d-119.1715508!2d39.4165827?entry=ttu)
- Virginia Beach is about 30 miles east of Dayton
- Funny, doesn't look like [Virginia Beach, VA](https://en.wikipedia.org/wiki/Virginia_Beach,_Virginia)??

## [Beatty, NV](https://gitlab.com/bobby.estey/bobby/-/blob/main/biography/states/nevada/beatty/beatty.md)
- Wild Donkeys all over the Town

## [Gardnerville, NV](https://en.wikipedia.org/wiki/Gardnerville,_Nevada)
- Was the place we thought we were going to live.  To expensive

## [Genoa, NV](https://en.wikipedia.org/wiki/Genoa,_Nevada)
- Beautiful location next to the Mountains

## [Lake Tahoe, CA / NV](https://en.wikipedia.org/wiki/Lake_Tahoe)
- 35 miles west of Dayton

## [Mercury, NV](./mercury/README.md)
- Lots of Stuff to Talk about, Nuclear Test Site, Still today

## [Pyramid Lake, NV](https://en.wikipedia.org/wiki/Pyramid_Lake_(Nevada))
- A very interesting lake, you just have to see the large pyramid rocks, inside the lake

## [Walker Lake, NV](https://en.wikipedia.org/wiki/Walker_Lake_(Nevada))
- A very large lake next to [Hawthorne Army Depot](https://en.wikipedia.org/wiki/Hawthorne_Army_Depot) the Largest Ammunition Depot in the World

## [Mound House, NV](./moundHouse)
- **Do Not Enter This Link - So Why??? Did you click the links**
- Industrial and Other Things Professional
- We learned why the Carson City Airport stays in Business, this is due to Mound House, here are a couple links:
     - All Google Maps, you can navigate to your pleasure
     - The one place (KitKat) is open 25 hours, SERIOUSLY
     - [#1 - Don't Ask](https://www.google.com/maps/@39.2100527,-119.6634557,3a,75y,189.18h,88.34t/data=!3m6!1e1!3m4!1s3JWAXBU7DyfRRJ9bIndxsw!2e0!7i13312!8i6656?entry=ttu)
     - [#2 - Don't Ask](https://www.google.com/maps/@39.2183975,-119.6706035,3a,75y,320.38h,95.53t/data=!3m6!1e1!3m4!1syhl4th5LqSGhJVNksd2PEA!2e0!7i13312!8i6656?entry=ttu)
