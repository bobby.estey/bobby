# [Mercury, NV](https://en.wikipedia.org/wiki/Mercury,_Nevada)

- [Estey City, NM](../../newMexico/esteyCityNM/esteyCityNM.md) - the closest city to the first nuclear bomb in history, 13 miles
- We drive by Mercury, NV all the time on US95 mile marker 136

## Testing

- [Atomic Journeys - The Nevada Test Site](https://www.youtube.com/watch?v=Z7X7QNDi1mQ)
- Mercury is where they do all the Atomic Bomb testing / research since the late 1940's
- [Atomic Museum](https://www.atomicmuseum.vegas/)
     - We went here, very interesting

#### Testing with Troops

- I feel sorry for the US Army, they had 5,000 soldiers walk towards the blasts and no one at the time understood the effects of RADIATION
     - [US Army](https://www.youtube.com/watch?v=4f4NOP2k7jU)
- The Bikini Islands US Navy and Marines were also part of the blast tests
     - [US Navy](https://www.youtube.com/watch?v=phKPb5-WyF8)

## Speeding Ticket ([101 MPH](101mph.png))

- Got a speeding ticket north of mile marker 107 (2017/12/28) by Trooper Davenport, doing 101 MPH
     - Ironically, my First Car maximum speed was 107 MPH
     - Trooper Davenport was so awesome and gave me a break, he asked me where I was coming from, I said Mercury, was looking at accepting a position there years ago
     - He immediately said, "YOU DO NOT WANT TO WORK THERE.  I have seen people enter with 2 eyes and come out with 3".  Obviously he was joking.  He just said, "SLOW DOWN, BE SAFE".  Trooper Davenport, he was a nice man
     - I actually visited the Tonopah Nevada State Police Office in 2022.  Talked to the Trooper who was present at the time and found out Davenport was retired and we talked for a while, both are good men
