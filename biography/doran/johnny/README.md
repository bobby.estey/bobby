# Grandpa Johnny Doran

- According to my Mother, her Dad was always in mischief, nothing evil, just a Rascal

## Kite Flying - Patterson Building

- 817 17th Street, Denver, CO 80202
- Daniels and Fischer Tower Denver, CO - Clock in Background
- Johnny around his 30's would fly kites on top of downtown Denver, CO buildings
- Sometimes a kite would drop from the sky, cross the trolley car lines and cause havoc, sometimes stopping the trolley

## Johnny Doran - Annual Kite Day

![Johnny Doran - Annual Kite Day](johnnyDoranKiteDay.png)

## References - Denver, CO

![Denver, CO 1](denverCO1.jpg)
![Denver, CO 2](denverCO2.jpg)
- [Street View](https://www.google.com/maps/@39.7470612,-104.9922111,3a,75y,60.07h,90t/data=!3m6!1e1!3m4!1sM6Y12pvOExRzfUzARaKi_Q!2e0!7i16384!8i8192?entry=ttu)
- [Satellite View](https://www.google.com/maps/place/817+17th+St,+Denver,+CO+80202/@39.7472463,-104.9919708,63m/data=!3m1!1e3!4m6!3m5!1s0x876c78d0b1e1d2b1:0x131a09170a434422!8m2!3d39.7471732!4d-104.9919699!16s%2Fg%2F11csgn5yc1?entry=ttu)


## Father

- Michael Dennis Doran AKA Sliver 
     - Worked for the Railroad
