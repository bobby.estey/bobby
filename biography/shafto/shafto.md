# Shafto - Second Family

I was super close to this Family - They were my Brothers, Sister and non-biological Family.  The Shafto's were also from the "Great State of Nebraska"

## Photos / Videos

- [Walter John Loretta](walterJohnLoretta_shafto.jpg)
- [Loretta Kathy Rob Walter John Tim](lorettaKathyRob_walterJohnTim_shafto.jpg)
- [Walter Loretta Rob Tim Kathy John](walterLorettaRob_timKathyJohn_shafto.jpg)
- [Johnny Shafto - Bobby Estey 20230202_183542](johnnyShaftoBobbyEstey20230202_183542.mp4)

## Mr Walter Shafto (RIP)

- Mr Shafto was a Great Man, very intelligent and Loved to tell jokes

#### United States Navy

- [Aviation Storekeeper 3rd Class](https://s.turbifycdn.com/aah/militarybest/navy-red-e-4-aviation-storekeeper-ak-decal-sticker-63.gif)
- Served in the United States Navy on board [Bon Homme Richard CV31, AKA Bonny Dick](https://en.wikipedia.org/wiki/USS_Bon_Homme_Richard_(CV-31))

- My Mother worked at Rio Grande Railroad as a Claims Agent and here is a claim she was working with Mr Shafto at Ellis Foods (Ellis Canning Company)
     - [Account Analysis](geraldineEsteyClaimstoWalterShaftoEllis.pdf)

## Johnny Shafto

- Best Friend in High School and Member of the ["Hole in the Head Gang"](../bobby/holeInTheHeadGang/holeInTheHeadGang.md)
- The reason I joined the US Navy and we were in the same Company in Boot Camp 77-164
- Name mentioned in Andy Griffith Show, title, "Barney's Sidecar"
     - [Thank You Johnny Shafto - 2:44](johhnyShafto.mp4)
     
#### Slap Fight

John and I were at WORK (Robo Car Wash, Northglenn, CO) and we were bored and said to each other, "We don't want to get into a FIST FIGHT, let's be civilized with a SLAP FIGHT". WE SLAPPED THE SHIT OUT OF EACH OTHER.  This was not a back and forth, 1 to 1.  We would slap the shit out each other as much as possible before the slappee bailed out run away to get their brain clutch back engaged.  When you get slapped 4 to 5 times in a couple seconds, you are seeing stars, brain clutch is disengaged, hurts like hell and get out of there.  I am laughing now.  When I was able to get in 4 to 5 shots without a response was great, however, he would run away, get his marbles back then here comes the counter attack.  This went on for about an HOUR and let me be clear, was not COOL AT ALL, VERY SERIOUS and then finally, WE BOTH REALIZED, this is getting out of hand.

Instead of a simple innocent friendship fight turned bad and was serious. We both agreed, never to do this again and wished we should of just did a FIST FIGHT.

#### United States Navy

- [Aircrew Survival Equipmentman 2nd Class](https://i.ebayimg.com/images/g/W6gAAOxytdlQ~w2C/s-l1600.jpg)
- Served in the United States Navy on board
     - [Enterprise CVN65 - VA97](https://www.navysite.de/cruisebooks/cvn65-78/278.htm)
     - [Coral Sea CV43 - VA97](https://www.navysite.de/cruisebooks/cv43-80/236.htm)

#### Owner ANC Glass, Denver CO

- [Wells Fargo Building, Denver, CO](johnShafto_wellsFargoDenver.jpg)
