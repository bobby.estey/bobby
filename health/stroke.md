# Stroke

## Remember the 1st Three Letters....S.T.R. (Smile,Talk,Raise)

## STROKE IDENTIFICATION

During a BBQ, a friend stumbled and took a little fall - she assured everyone that she was fine (they offered to call paramedics) .she said she had just tripped over a brick because of her new shoes.

They got her cleaned up and got her a new plate of food. While she appeared a bit shaken up, Ingrid went about enjoying herself the rest of the evening

Ingrid's husband called later telling everyone that his wife had been taken to the hospital - (at 6:00 pm Ingrid passed away.) She had suffered a stroke at the BBQ. Had they known how to identify the signs of a stroke, perhaps Ingrid would be with us today. Some don't die. they end up in a helpless, hopeless condition instead.

It only takes a minute to read this...

A neurologist says that if he can get to a stroke victim within 3 hours he can totally reverse the effects of a stroke...totally. He said the trick was getting a stroke recognized, diagnosed, and then getting the patient medically cared for within 3 hours, which is tough.

## RECOGNIZING A STROKE

Thank God for the sense to remember the '3' steps, STR . Read and Learn!

Sometimes symptoms of a stroke are difficult to identify. Unfortunately, the lack of awareness spells disaster. The stroke victim may suffer severe brain damage when people nearby fail to recognize the symptoms of a stroke.

Now doctors say a bystander can recognize a stroke by asking three simple questions:

S *Ask the individual to SMILE.
T *Ask the person to TALK and SPEAK A SIMPLE SENTENCE (Coherently)
(i.e. It is sunny out today.)
R *Ask him or her to RAISE BOTH ARMS.

If he or she has trouble with ANY ONE of these tasks, call emergency number immediately and describe the symptoms to the dispatcher.

New Sign of a Stroke -------- Stick out Your Tongue

NOTE: Another 'sign' of a stroke is this: Ask the person to 'stick' out his tongue.. If the tongue is 'crooked', if it goes to one side or the other, that is also an indication of a stroke.

A cardiologist says if everyone who gets this e-mail sends it to 10 people; you can bet that at least one life will be saved.
