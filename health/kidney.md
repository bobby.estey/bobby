# Foods for Kidney Health

## maximums - https://fdc.nal.usda.gov/index.html
|item|grams (g)|milligrams (mg)|
|--|--|--|
|phosphorus|2|2000|
|potassium|2|2000|
|sodium|1.3|1300|
|sugar|50|50000|

## DOS
- water - at least 0.5 gallons
- cook with nature herbs and spices
- fresh or frozen meats, fish or poultry, skinless chicken
- spices - basil, bell peppers, chili peppers, cinnamon, garlic, ginger, mushrooms, olive oil, oregano, rosemary
- vegetables - all greens, arugula, beets, cauliflower, bell peppers, cabbage, ,  radish, turnips,  mushrooms, onions, shiitake
- fruits - apples, berries, blueberries, cherries, cranberries, lemons, peaches, pineapple, red grapes 
- drinks - cranberry juice, tea
- buckwheat, bulgur, couscous, grits
- honey, jam, jelly
- vanilla or lemon flavored desserts
- sea bass
- nuts - macadamia, walnuts

## LIMITED
almonds, carrots, cashews, mixed nuts, peanuts, pistachios

## DONTS
- processed or enhanced meats, fish or poultry
- baked potato, french fries, oatmeal, potatoes, sweet potatoes
- caffeine lower than 300 mg per day
- chips, crackers, pretzels, whole wheat bread
- dairy - cheese or processed cheese, milk, yogurt
- peanut butter, pumpkin seed, sunflower seeds
- amaranth, brown rice, millet, quinoa, spelt
- avocados, beet greens, canned foods, chili sauce, spinach, tomatoes, tomato sauce
- apricots, bananas, cantaloupe, dates, honeydew, kiwi, oranges, prunes, raisins
- olives, pickles, relish
