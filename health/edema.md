# Edema - flush lymph vessels

## [Video](https://detoxifyandvitalize.com/)

- Pina Colada without the Rum - drink down in 5 minutes or less

## Myths

- Edema is about the cardio system, heart, arteries, blood vessels, etc
- This is False

## Truth

- Edema is about the Lymphatic system
- Clears the body of all garbage through Lymph vessels which carry Lymph fluid
- Lymph fluid contain Bad Cholesterol, toxins, bacteria and bad cells
- When Lymph vessels start clogging causes damage to the body and stops draining the body of Lymph fluid
- Inflammation damages Lymph vessels
- The right balance of Sodium and Potassium are key and both have Electrolytes 

## Coumarin 

- relieves tightness and tension in the body
- helps with opening up Lymph vessels and increases cardio system and blood vessel flow
- found in citrus fruits
     - lime peels
     - lemon peels
     - orange peels
     - grapefruit peels

## Bromelain 

- stops Inflammation and opens up Lymph vessels
- helps skin to be strong and healthy
- weak skin allows bacteria and infections into the body
- pineapple

## Electrolyte (Potassium, Phosphorus and Magnesium) 

- 1 Cup of Kelp a day is all needed for Potassium and Magnesium
     - reduces pain and swelling
     - keeps blood flowing
- Gatorade
- Potassium - requires 4700 mG / day
     - Banana - 10 Bananas per day = 4700 mG
          - has both Potassium and Fiber
          - balances blood sugar
          - high blood sugar causes Inflammation
- Magnesium

#### Coconut Cream 

- Good Fats
- Stabilize good Cholesterol
- Boost Immune System
- Turn on Fat Body furnace
- Rich in Electrolytes
- reduces cramps and tingling 

## Fucoxanthin 

- Reduces Cholesterol
- Removes Fat from the body

## Dandelion (Taraxacum officinale) 

- good for kidneys and heart
- contains Taraxcim and Taracerin taraxacum
- high amounts of Potassium
- increases going to the bathroom

## Echinacea 

- boost immune system and fights infection
- used to cure cold and flu

## Burdock Root 

- potent blood purifier
- flushes toxins out of the body

## Cleavers and Rutin 

- clear lymph vessels
- improve Circulation and reduces Inflammation 
     
## Oils

- Good - Butter, Olive Oil and Avocado Oil
- Bad - vegetable oil - 10 times worse than sugar and salt
