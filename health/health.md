# Health Page

## [Phosphorus Food Guide](https://gitlab.com/bobby.estey/bobby/-/blob/main/health/Phosphorus-Guide.pdf)

## [Potassium Food Guide](https://gitlab.com/bobby.estey/bobby/-/blob/main/health/Potassium-Guide.pdf)

## [Kidney Health](https://gitlab.com/bobby.estey/bobby/-/blob/main/health/kidney.md)

## [Testosterone Foods](https://www.healthline.com/nutrition/testosterone-boosting-food)

## [KREBS Cycle](https://gitlab.com/bobby.estey/bobby/-/blob/main/health/krebs.md)

## [Edema](https://gitlab.com/bobby.estey/bobby/-/blob/main/health/edema.md)

## [Alternatives to Ozempic]

|Item|Description|
|--|--|
|Berberine|Improves Gut Health|
|Chromium|Found in Greens and Turkey|
|Turmeric|Anti-Inflamtory|
|Green Tea|Natural Fat Burner|
|Magnesium|Metabolism Supplement|

## Food as Medicine

HEADACHE? EAT FISH!  Eat plenty of fish -- fish oil helps prevent headaches.  So does ginger, which reduces inflammation and pain.
HAY FEVER? EAT YOGURT! Eat lots of yogurt before pollen season. Also-eat honey from your area (local region) daily.
TO PREVENT STROKE DRINK TEA! Prevent buildup of fatty deposits on artery walls with regular doses of tea. (actually, tea suppresses my appetite and keeps the pounds from invading....Green tea is great for our immune system)!
INSOMNIA (CAN'T SLEEP?) HONEY! Use honey as a tranquilizer and sedative.
ASTHMA? EAT ONIONS!!!! Eating onions helps ease constriction of bronchial tubes. (when I was young, my mother would make onion packs to place on our chest, helped the respiratory ailments and actually made us breathe better).
ARTHRITIS? EAT FISH, TOO!! Salmon, tuna, mackerel and sardines actually prevent arthritis. (fish has omega oils, good for our immune system)
UPSET STOMACH?  BANANAS - GINGER!!!!! Bananas will settle an upset stomach. Ginger will cure morning sickness and nausea.
BLADDER INFECTION? DRINK CRANBERRY JUICE!!!! High-acid cranberry juice controls harmful bacteria.
BONE PROBLEMS? EAT PINEAPPLE!!! Bone fractures and osteoporosis can be prevented by the manganese in pineapple.
PREMENSTRUAL SYNDROME? EAT CORNFLAKES!!!! Women can ward off the effects of PMS with cornflakes, which help reduce depression, anxiety and fatigue.
MEMORY PROBLEMS? EAT OYSTERS! Oysters help improve your mental functioning by supplying much-needed zinc.
COLDS? EAT GARLIC! Clear up that stuffy head with garlic. (remember, garlic lowers cholesterol, too.)
COUGHING? USE RED PEPPERS!! A substance similar to that found in the cough syrups is found in hot red pepper. Use red (cayenne) pepper with caution-it can irritate your tummy.
BREAST CANCER?  EAT Wheat, bran and cabbage Helps to maintain estrogen at healthy levels.
LUNG CANCER? EAT DARK GREEN AND ORANGE AND VEGGIES!!! A good antidote is beta carotene, a form of Vitamin A found in dark green and orange vegetables.
ULCERS? EAT CABBAGE ALSO!!! Cabbage contains chemicals that help heal both gastric and duodenal ulcers.
DIARRHEA? EAT APPLES! Grate an apple with its skin, let it turn brown and eat it to cure this condition. (Bananas are good for this ailment)
CLOGGED ARTERIES? EAT AVOCADO! Mono unsaturated fat in avocados lowers cholesterol.
HIGH BLOOD PRESSURE? EAT CELERY AND OLIVE OIL!!! Olive oil has been shown to lower blood pressure. Celery contains a chemical that lowers pressure too.
BLOOD SUGAR IMBALANCE? EAT BROCCOLI AND PEANUTS!!! The chromium in broccoli and peanuts helps regulate insulin and blood sugar.
Kiwi: Tiny but mighty. This is a good source of potassium, magnesium, Vitamin E &fiber. It's Vitamin C content is twice that of an orange.
Apple: An apple a day keeps the doctor away? Although an apple has a low Vitamin C content, it has antioxidants &flavonoids which enhances the activity of Vitamin C thereby helping to lower the risks of colon cancer, heart attack & stroke.
Strawberry: Protective fruit. Strawberries have the highest total antioxidant power among major fruits &protects the body from cancer causing, blood vessels clogging free radicals. (Actually, any berry is good for you..they're high in anti-oxidants and they actually keep us young.........blueberries are the best and very versatile in the health field........they get rid of all the free-radicals that invade our bodies)
Orange: Sweetest medicine. Taking 2 - 4 oranges a day may help keep colds away, lower cholesterol, prevent & dissolve kidney stones as well as lessen the risk of colon cancer.
Watermelon: Coolest Thirst Quencher. Composed of 92% water, it is also packed with a giant dose of glutathione which helps boost our immune system.  They are also a key source of lycopene - the cancer fighting oxidant.  Other nutrients   found in watermelon are Vitamin C &Potassium. (watermelon also has natural substances [natural SPF sources] that keep our skin healthy, protecting our skin from those darn suv rays)
Guava &Papaya: Top awards for Vitamin C. They are the clear winners for their high Vitamin C content. Guava is also rich in fiber which helps prevent constipation.
Papaya is rich in carotene, this is good for your eyes. (also good for gas and indigestion)
Tomatoes are very good as a preventative measure for men, keeps those prostrate problems from invading their bodies.

