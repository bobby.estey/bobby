# Cancer Update from Johns Hopkins

1. Every person has cancer cells in the body.  These cancer cells do not show up in the  standard Tests until they have multiplied to a few billion.  When doctors tell cancer patients that there are no more cancer cells in their bodies after treatment, it just means the tests are unable to detect the cancer cells because they have not reached the detectable size.  

2. Cancer cells occur between 6 to more than 10 times in a person's life time.

3. When the person's immune system is strong the cancer cells will be destroyed and prevented from multiplying and forming tumors.  

4. When a person has cancer it indicates the person has multiple nutritional deficiencies. These could be due to genetic, environmental, food and lifestyle (lack of sleep) factors.  

5. To overcome the multiple nutritional deficiencies, changing diet and including supplements will strengthen the immune system.

6. Chemotherapy involves poisoning the rapidly-growing cancer cells and also destroys rapidly-growing healthy cells in the bone marrow, gastro-intestinaltract etc, and can cause organ damage, like liver, kidneys, heart, lungs etc.

7. Radiation while destroying cancer cells also burns, scars and damages healthy cells, tissues and organs.

8. Initial treatment with chemotherapy and radiation will often reduce tumor size. However prolonged use of chemotherapy and radiation do not result in more tumor destruction.

9.  When the body has too much toxic burden from chemotherapy and radiation the immune system is either compromised or destroyed, hence the person can succumb to various kinds of infections and complications.

10. Chemotherapy and radiation can cause cancer cells to mutate and become resistant and difficult to destroy. Surgery can also cause cancer cells to spread to other sites.

11. An effective way to battle cancer is to starve the cancer cells by not feeding it with the foods it needs to multiply.

CANCER CELLS FEED ON:

A. Sugar is a cancer-feeder.  By cutting off sugar it cuts off one important food supply to the cancer cells. Sugar substitutes like NutraSweet, Equal, Spoonful, etc are made with Aspartame and it is harmful.  A better natural substitute would be Manuka honey or molasses but only in very small amounts.  Table salt has a chemical added to make it white in color.  Better alternative is Bragg's aminos or sea salt.

B. Milk causes the body to produce mucus, especially in the gastro-intestinaltract.  Cancer feeds on mucus.  By cutting off milk and substituting with unsweetened soya milk cancer cells are being starved.

C. Cancer cells thrive in an acid environment.   A meat-based diet is acidic and it is best to eat fish, and a little chicken rather than beef or pork.  Meat also contains  livestock antibiotics, growth hormones and parasites, which are all harmful, especially to people with cancer.

D. A diet made of 80% fresh vegetables and juice,  whole grains, seeds, nuts and a little fruits help put the body into an alkaline environment.  About 20% can be from cooked food including beans.  Fresh vegetable juices provide live enzymes that are easily absorbed and reach down to cellular levels within 15 minutes to nourish and enhance growth of healthy cells.  To obtain live enzymes for building healthy cells, try and drink fresh vegetable juice (most vegetables including bean sprouts) and eat some raw vegetables 2 or 3 times a day. Enzymes are destroyed at temperatures of 104 degrees F (40 degrees C).  

E.. Avoid coffee, tea, and chocolate, which have high caffeine.  Green tea is a better alternative and has cancer-fighting properties.  Water -- best to drink purified water, or filtered, to avoid known toxins and heavy metals in tapwater..  Distilled water is acidic, avoid it.  

12. Meat protein is difficult to digest and requires a lot of digestive enzymes.  Undigested meat remaining in the intestines become putrified and leads to more toxic buildup.  
>
>     
>
>      13. Cancer cell walls have a tough protein covering.
>  By refraining
>      from or eating less meat it frees more enzymes to
> attack the protein
>      walls of cancer cells and allows the body's
> killer cells to destroy the
>      cancer cells.
>
>     
>
>      14. Some supplements build up the immune system (IP6,
>  Flor-ssence,
>      Essiac, anti-oxidants, vitamins, minerals, EFAs,
> etc.) to enable the
>      body's own killer cells to destroy cancer cells..
>  Other supplements
>      like vitamin E are known to cause apoptosis, or
> programmed cell death,
>      the body's normal method of disposing of damaged,
> unwanted, or unneeded
>      cells.  
>
>     
>
>      15. Cancer is a disease of the mind,
>      body, and spirit.  A proactive and
> positive spirit
>      will help the cancer warrior be a survivor.
>  Anger, unforgiveness
>      and bitterness put the body into a stressful and
> acidic environment.
>       Learn to have a loving and forgiving spirit.
>  Learn to relax
>      and enjoy life.
>
>     
>
>      16. Cancer cells cannot thrive in an oxygenated
> environment..
>       Exercising daily, and deep breathing help to
> get more oxygen down
>      to the cellular level.  Oxygen therapy is
> another means employed to
>      destroy cancer cells.  
>
>     
>
>      (PLEASE FORWARD IT TO PEOPLE YOU CARE ABOUT)
>  
>
>     
>
>      CANCER UPDATE FROM JOHNS HOPKINS HOSPITAL
>
>     
>
>      1. No plastic containers in micro.
>
>      2. No water bottles in freezer.  
>
>      3. No plastic wrap in microwave.
>
>       
>
>      Johns Hopkins has recently sent this out in its
> newsletters..  This
>      information is being circulated at Walter Reed Army
> Medical Center as
>      well.
>
>     
>
>      Dioxin chemicals causes cancer, especially breast
> cancer.
>
>     
>
>      Dioxins are highly poisonous to the cells of our
> bodies..
>
>     
>
>      Don't freeze your plastic bottles with water in
> them as this releases
>      dioxins from the plastic.
>
>     
>
>      Recently, Dr. Edward Fujimoto, Wellness Program
> Manager at Castle
>      Hospital , was on a TV program to explain this health
> hazard.  He
>      talked about dioxins and how bad they are for us.
>  He said that we
>      should not be heating our food in the microwave using
> plastic containers.
>
>     
>
>      This especially applies to foods that contain fat.
>  He said that the
>      combination of fat, high heat, and plastics releases
> dioxin into the food
>      and ultimately into the cells of the body.
>  Instead, he recommends
>      using glass, such as CorningWare, Pyrex or ceramic
> containers for heating
>      food.  You get the same results, only without
> the dioxin.  So
>      such things as TV dinners, instant and soups, etc..,
> should be removed
>      from the container and heated in something
>  else.

Paper isn't bad but you don't know what is in the paper.  It's just safer to use tempered glass, Corning Ware, etc. He reminded us that a while ago some of the fast food restaurants moved away from the foam containers to paper. The dioxin problem is one of the reasons.

Also, he pointed out that plastic wrap, such as Saran, is just as dangerous when placed over foods to be cooked in the microwave.  As the food is nuked, the high heat causes poisonous toxins to actually melt out of the plastic wrap and drip into the food.  Cover food with a paper towel instead.