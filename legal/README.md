# Legal Rights

- While most Cops are good people, there are others that just want to lock you up
- This is not about being Paranoid, but knowing your legal rights
- **BOTTOM LINE:  KEEP YOUR MOUTH SHUT**

# Amendments

- 4th Amendment - Right to Refuse Consent to Search
- 5th Amendment - Right to Remain Silent
- 6th Amendment - Right to Counsel

## Terms

- Furtive Movement - acting secretly and quietly to avoid being noticed
     - moving items under the seat

# Videos (Must Watch Videos)

 - [4, 5, 6 Amendments](https://www.youtube.com/watch?v=ET37ax_gWl4)
 - [Remain to Be Silent](https://www.youtube.com/watch?v=ZNUw2G9WWOk)
 - [Guns](https://www.youtube.com/watch?v=REFxINEo6o4)

## Response

- I am happy to answer any and all questions, however, I must have my Attorney Present **6th Amendment** and I exercise my right to remain silent **5th Amendment**
- After saying this, **KEEP YOUR MOUTH SHUT**

## Difference between a Voluntary / Involuntary Encounter

- If a Cop walks up to you to start a conversation, can you turn around and just leave?  **YES**
- How do you know if the Cops are **FISHING** for evidence against you:
     - **ONLY** ask the following Question
     - "Am I Free to Leave?"
- If the Cop says no, **You are Under Criminal Investigation and being Detained**
- If the Cop says yes, **LEAVE IMMEDIATELY**

## Tips - How to Deal with Cops in Common Situations

- **ALWAYS** - Exercise 4th, 5th and 6th Amendments

#### Cops come to the door

- Don't open the door
     - If you open the door the Cop will be doing Surveillance, **SO KEEP THE DOOR CLOSED** 
- Make the Cops get a search or arrest warrant
     - If the Cop has either items, they will come into the home, **REGARDLESS**
     - The Cop can only search what is on the search on the warrant
     - **DO NOT, I REPEAT DO NOT let the Cops search for anything outside the Warrant**

#### Cops pull you over for a traffic stop

- Provide drivers license, proof of insurance and valid registration
- When the Cop after their investigation, Ask, "Am I free to go?"
- **NOTE** - MOST STATES - Passenger in the car, **VERY IMPORTANT**:
     - The Driver is under investigation, not the Passenger
     - Passenger does not have to provide any answers to questions or identification
     - Passenger should remain silent at all times

# Cop Demands Your Guns

- Never reach for a gun to give to a Cop
- Always allow the Cop to remove the Gun
- Remind the Cop you still will not allow Searching the property / vehicle
- Exercise the 4th Amendment

# Self Defense Statement - Post Incident Instructions from USCCA - 877.677.1919

- [United States Concealed Carry Association (USCCA) Website](https://usconcealedcarry.com/)
- I am a member of USCCA so the following is Applicable, good advice for non-members

## Call 911

- I was attacked, feared for my life and had to defend myself
- Please send **BOTH** POLICE and an Ambulance to this location
- **HANG UP THE PHONE** and say nothing until you have your Lawyer Present

## Call Official Response Team 877.677.1919

- Press # for immediate assistance

## When Responding Cops Arrive

- **Comply** - Follow All Police Instructions
- **Medical** - Request medical assistance, if necessary
- **Silence** - I will cooperate 100 percent, but first I need my Attorney

#### DO NOT TALK FURTHER without your Lawyer Present
