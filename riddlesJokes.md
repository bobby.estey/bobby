# THE BEST EVER

- Juneau the Capitol of Alaska?  ANSWER:  Don't JUNEAU?

# Links

[Jokes 4 US](http://www.jokes4us.com/miscellaneousjokes/worldjokes/polandjokes.html)
[Laugh Factory](https://www.laughfactory.com/jokes)

# Riddles and Jokes

## Riddles / One Liners / Quick Jokes / Dad Jokes

- A guy asked a girl, "Will you marry me".  She said, "No".  The guy lived happily ever after, went fishing, hunting, played golf, drank beer and farted whenever he wanted.
- I called my wife the other night and said, "I was thinking about the last time we had sex and I am getting excited".  She said, "Who is this?"
- My Girl Friend Dumped Me, so I stole her wheel chair, Guess who came back crawling back
- What do Asians do during erections?  Vote
- What did the Mexican take anxiety pills?  For HisPanik
- How do you catch a Virgin in Arkansas? She can't out run her Brothers
- How many * does it take to oil a car? 1, depends on how you hit them (Mike)
- What has 5 toes and isn't your foot?  My other Foot (Ryan)
- What happened to the Cat that ate Yarn?  The Cat had mittens (Ryan)
- How do you make hard water with three letters?  ICE (Barbara)
- What do you call a woman with no breasts and no butt?  No Body
- What did the left side of the * say to the right side of the *?  We got along until this * came between us
- What do you call 5 White guys sitting on the bench?  The NBA
- What do you call 4 Mexicans in quick sand?  Quatro Cinco
- What do you call a fake pasta?  An impasta (Ester)
- Why do fire fighters use Asbestos?  So they can fight the fire as best as they can
- What was the last thing, the Buffalo said to its Child when they departed?  Bison
- What is the difference between a Zippo and and Hippo?  A Zippo is lighter
- What did the acorn say when it grew up?  GEOMETRY
- A Neutron walks into a bar and asks, "How much for a drink". The Bartender says, "No Charge".
- A Photon walks in hotel's Lobby.  "Any luggage?", asks the Receptionist.  "No", answers the Photon, "I'm traveling light!"
- What is lighter than green? light green ()
- I didn't slap you, I just high fived your face (saw this on a T-Shirt)
- The Police Precint's Toilet was stolen.  The Police have nothing to go on
- How many seconds in a year? 12 - January 2nd, February 2nd, ...
- What do you call a Chicken laying an egg on the roof?  An Eggroll ()
- How do you catch a unique Rabbit?  Unique on them ()
- Why did the Lemon stop in the middle of the road?  He ran out of juice ()
- What is worse than Ants?  Uncles
- When is the best time to land on the Sun?  Night Time
- What do you call a Fish with no eyes?  Fsh
- How old were the the Donner Kids?  8 and up
- What are the benefits of V*?  Fix a flat and miracle grow
- What did the Banana say to the *?  What are you shaking for, I am going to be eaten
- How do you unstick toilet paper, lick your tongue
- What were the names of the Mexican Firefighters?  Hose A and Hose B

## Men strike back (Part 1)

- My wife while driving said, "You never open the door for me"
- I hear that Females can't fly planes.  I know that isn't true, they just can't land
- If your dog is barking at the back door and your wife is yelling at the front door, who do you let in first?  The dog, of course.  He'll shut up once you let him in
- Why do men die before their wives?  They want to
- How many men does it take to open a beer?  None. It should be opened when she brings it
- What's worse than a Male Chauvinist Pig?  A woman who won't do what she's told
- How do you fix a woman's watch?  You don't. There is a clock on the oven
- How do you know when a woman is about to say something smart?  When she starts a sentence with "A man once told me..."
- In the beginning, God created the earth and rested. Then God created Man and rested. Then God created Woman.  Since then, neither God nor Man has rested
- Why is a Laundromat a really bad place to pick up a woman?  Because a woman who can't even afford a washing machine will probably never be able to support you
- Why do women have smaller feet than men?  It's one of those "evolutionary things" that allows them to stand closer to the kitchen sink
- Why do men fart more than women?  Because women can't shut up long enough to build up the required pressure
- I married a Miss Right.  I just didn't know her first name was Always
- Scientists have discovered a food that diminishes a woman's sex drive by 90%.  It's called a Wedding Cake
- Women will never be equal to men until they can walk down the street with a bald head and a beer gut, and still think they are sexy
- What do you tell a woman with two black eyes?  Nothing, she's been told twice

## Men strike back (Part 2)

- What is the difference between men and women?  A woman wants one man to satisfy her every need. A man wants every woman to satisfy his one need
- What is the insensitive part at the base of the penis called?  The man
- Why don't men often show their true feelings?  Because they don't have any
- What's the difference between a man's wife and his girlfriend?  60 pounds
- What's the difference between a woman's husband and her boyfriend?  60 minutes
- Why are men like commercials?  You can't believe a word they say
- Why are men like popcorn?  They satisfy you, but only for a little while
- Why are men like blenders?  You need one, but you're not quite sure why
- Why do so many women fake orgasm?  Because so many men fake foreplay
- What's a man's definition of a romantic evening?  Sex
- What is the only time a man thinks about a candlelit dinner?  When the power goes off
- What do men and women have in common?  They both distrust men
- How can you tell the difference between men's real gifts and their guilt gifts?  Guilt gifts are nicer
- What's a man's idea of safe sex?  A padded headboard
- What do you instantly know about a well-dressed man?  His wife is good at picking out clothes
- How is a man like the weather?  Nothing can be done to change either one of them
- What is the difference between a man and childbirth?  One can be terribly painful and sometimes almost unbearable while the other is just having a baby
- What do you call a man who expects to have sex on the second date?  Slow
- What is the one thing that all men at singles bars have in common?  They're married
- How does a man make sex more interesting?  He leaves town
- For a woman, marriage is more than just a word.  It's a sentence
- What do you give a man who has everything?  Penicillin
- There are two times in his life when a man doesn't understand women.  Before marriage, and after marriage
- Where can you find a committed man?  In a mental institution
- How do you know when a man's had an orgasm?  He snores
- What's a sure sign a man will become unfaithful?  He has a penis
- Why are men like floor tiles?  If you lay them right the first time, you can walk all over them the rest of your life
- Why does a man like going to bed with two women?  So they'll have someone to talk to
- What does a man consider to be a seven-course meal?  A hot-dog and a six-pack of beer
- Why don't men know the meaning of fear?  They only know one four-letter word beginning with F
- When's the only time you can change a man?  When he's a baby
- Wife: I'd like to thank my husband for three wonderful years of marriage - 1982, 1984 and 1987
- How can you tell a man is thinking about sex?  He's breathing
- Man: Fancy a quickie?  Woman: As opposed to what?
- What's a man's idea of helping with the housework?  Lifting his legs so you can vacuum underneath

###### Angel - 30 years younger ()

- A couple was married for 30 years, they were both 60 years old
- An Angel appeared and granted them a wish a piece
- The wife wanted a trip around the world with her husband
- The husband wanted to be on the trip with a woman 30 years younger than him
- The Angel granted them their wishes, they received a world trip around the world
- The husband was 90 years old

###### Big G

- What are the 3 forms of communication?  Telegraph, Telephone, TELE-WOMAN
- My wife asked me to get her lipstick.  I got her a glue stick. She hasn't spoke to me since
- What happens when you throw a Red Rock into the Black Sea?  It sinks
- There was a pair of jumper cables that went to a bar.  The bartender said, "I'll serve you under one condition, as long as you don't start anything"
- Why can't dinosaurs clap their hands?  They're extinct
- What's the difference between a Ferrari and a lawn mower?  I don't have a Ferrari in my garage
- My Boss said, "To dress for the job you want, not the job you have".  So I went in dressed like Batman
- In a recent study, they proved that people eat more bananas than monkeys.  I don't recall seeing anyone eating a monkey
- How many feet are in a yard?  Depends on how many people are standing in it
- I met this girl named Bree at a party last night.  She was a little cheesy
- Why was Cinderella so bad at soccer?  She kept running away from the ball
- What do you call a pig that practices karate?  A pork chop
- What did Sushi A say to Sushi B?  WASABI! (Said like the old Bud light commercial)
- What did they call the Terminator when he retired? The Exterminator
- I gave the handyman a list of things to do, but he only did items 1, 3 and 5.  Turns out he only does odd jobs
- What's the difference between jelly and jam?  You can't jelly a printer
- Do you know what my uncle said before he kicked the bucket?  Watch how far I can kick this bucket
- What did the lettuce say to the celery?  Stop stalking me
- Why did the rolling stones quit making music? They got to the bottom of the hill
- What do Kermit the Frog and Alexander the Great have in common?  Both middle names are "THE"
- The number one cause of car accidents in Tennessee is Deer.  I didn't know that they allowed Deer to drive
- Of all the inventions the last 100 years, the dry erase board has to be the most remarkable
- Unlike me, what can run without running out of breath?  River, Fridge, many other things

#### Yo Mamma (TJ)

- Yo mamma is so ugly when she tried to join an ugly contest they said, "Sorry, no professionals."

###### Is so Stupid

- She stole a car off a tow truck
- She sold the TV to pay the cable bill

#### Crude

- Ms Bill Gates, please describe your *.  She said, "Oh, that's easy, it's *"
- What's the hardest part of a vegetable to eat?  The *
- How do you * a hillbilly?  You kick his * in the mouth ()
- What is the biggest tragedy of a Bus full of * going over a cliff? (Marty, AKA Juneau)
- What is long and hard *?  The first day working at McDonalds ()
- How do you keep * from entering the backyard?  You hang a * in the front yard ()
- Lincoln never said, "*", he actually said, "*"
- What's the difference *? * Doesn't scream
- What did Cinderella do when she when she got to the ball?  *
- Why can't Ken get *?  Because he * in a different box
- If * is beautiful, then I * a masterpiece. ()
- How does every * joke start?  By looking around
- Definition of Gross:  Kissing Grandma and she slips you the tongue
- Definition of Gross:  Farting in a Spacesuit
- Confucius say:  Man who stand on Toilet, get high on Pot
- Confucius say:  Man who run behind car, get exhausted
- Confucius say:  Man who goes to be wakes with sticky, smelly fingers

## Jokes

#### Funny or Stupid

###### Beer

- She told me, "We couldn't afford beer anymore and I would have to quit"
- Then I caught her spending $65 on makeup
- Then I asked, "How come I had to give up stuff and she didn't"
- She said, "She needed the makeup to look pretty for me"
- I told her, "That was what the beer was for"
- I don't think she's coming back

###### $40 in a Pocket (Minuteman Randy USAF)

- A man was very drunk at the bar and accidentally threw up on his shirt and said to the man next to him, "My wife is going to be upset at me for messing up my shirt"
- The other man told him, "First thing you need to do is put $20 in your shirt pocket.  When you get home, tell her that I was sitting next to you, threw up on your shirt and paid you $20 for my mistake"
- The man went home, reported to his wife what happened
- The wife reached in the pocket and discovered $40 dollars
- The wife said, "I see $40 not $20"
- The man said, "Oh, the man also shit in my pants"

###### Stop That Flight

- This DRUNK guy calls an airport control tower and asks the question, "What flights are leaving after 2PM from Denver to Omaha".
- The controller replies, "There are four flights, United has 2 flights, Western a flight and Frontier a flight"
- The guy says, "You have to stop the Western flight, STOP THAT FLIGHT, STOP THAT FLIGHT!!!"
- The controller replies, "Sir why do I have to stop the flight"
- The guy says, stupidly drunk, "Because I am the pilot"

###### Woman Cared For

- A woman asks a man, "Have you ever met a woman you cared for?"
- The man replies, "Yes, it was love at first site"
- The woman now asked, "Did you marry her?"
- The man replies, "No, I took a second look"

###### Golfing

- A Guy calls up his Friend and says, "Hey let's go golfing".  His Friend says, "Let's meet at 8 or 830"
- They meet the next day at 8 and his Friend shot a 66 right handed and the Guy cannot believe it.  The Guy say's, "Let's go golfing tomorrow".  His Friend says, "Let's meet at 8 or 830"
- They meet the next day at 8 and his Friend shot a 64 left handed and the Guy cannot believe it.  The Guy asks, "How do you do it?"
- His Friend says, "When my wife sleeps on her right side, I swing right handed and on her left I swing left handed"
- The Guy asks, "What about if she sleeps on her back".  The Friend says, "I will be there at 830"

###### Potato Chips ()

- There were 10 potato chips on the side of a mountain
- 5 of the chips jumped off the cliff
- Why did the other chips not jump
- Because they were Wise

###### Don't Touch Me (Frank David)

- A parapalegic, blind man and a * were in a bar
- Jesus walks in and touches the parapalegic who is cured immediately.  The parapalegic is running around and is happy
- Jesus then touches the blind man, the blind man can see and is happy
- The * immediately yells, "DON'T TOUCH ME, DON'T TOUCH ME, I am on workman's compensation"

###### 5 Pennies (Carson Nugget)

- Put down one penny, do you smell anything.  A Cent
- Put down two pennies, do you see any fruit.  A Pair  
- Put down three pennies, do you see any snakes.  Three Copperheads
- Put down four pennies, do you see any cars.  Four Lincolns
- Put down five pennies, do you see any *****, not for 5 Cents

###### Airplane - Time (My Father)

Introduction:  My Father worked for the original Frontier Airlines in the 60's and 70's.  Here is a joke that was shared with the employees

Four Airlines are flying their passengers from Denver to Omaha

- United Airlines (UA) was the first plane to arrive to Omaha controlled airspace.  UA says, "Omaha this is UA, what is the time?"  Omaha responds, "1400"
- Continental Airlines (CA) was the second plane to arrive to Omaha controlled airspace.  CA says, "Omaha this is CA, what is the time?"  Omaha responds, "2PM"
- Western Airlines (WA) was the third plane to arrive to Omaha controlled airspace.  WA says, "Omaha this is WA, what is the time?"  Omaha responds, "Big Hand on 12 Little Hand on 2"
- Frontier Airlines (FA) was the forth plane to arrive to Omaha controlled airspace.  FA says, "Omaha this is FA, what is the time?"  Omaha responds, "Tuesday"

###### Airplane - Ambassadors (Bernadette)

- Airplane flying Ambassadors from all over the world.  The Pilot starts talking on the Intercom
- Hello Passengers, "This is the Captain and we have a problem"
- We lost our first engine, we cannot maintain altitude, if one passenger can depart the plane that person can save all our lives
- English Man walks up to the front of the plane and yells, "God Save the Queen".  Jumps out and saves everyones lives.  Everyone is cheering, "He was a Hero"
- The Pilot gets on the Intercom and again says, "We just lost another engine, someone needs to jump"
- French Man walks up to the front of the plane and yells, "Viva La France".  Jumps out and saves everyones lives.  Everyone is cheering, "He was a Hero"
- The Pilot gets on the Intercom one last time and says, "We just lost our last engine and I can see the airfield, someone needs to jump"
- A * walks up to the front of the plane and yells, "*", and throws a * off the plane.

###### Airplane - Parachutes

- Airplane flying carrying 3 people and there are only 2 parachutes
- They all decide whoever gets the question wrong, doesn't get a parachute
- First man is asked, "What year was the sinking of the Titanic?", He replies, "1912"
- Second man is asked, "How many people died on the Titantic?", He replies, "1517"
- Third man is asked, "Name them"

###### Waiting Room

- Prior to 1980s the Father would be in the Waiting Room waiting for their Child to be delivered
- The Doctor comes out, hands the Father a new Baby and says, "Sir, Congratulations you have a healthy Baby Girl"
- The Doctor continued and says, "However, your Wife did not make it"
- The Father hands the Baby back to the Doctor and says, "Well give me the one she made"

###### Carousel (Tom)

- Your riding a horse as fast as you can
- There's a Lion to the right and a Tiger to the left
- What do you do?
- Get your drunk ass off the Carousel

###### Smart Girl ()

- A girl went to Kindergarten and the first day they were learning about the ABCs
- The girl told her mother, "Today we were learning the ABCs.  The class was able to go up to D, but I was able to go to J"
- The girl asked her mother, "Is that because I am *?"  
- The Mother replies, "No, it's because you are smart"
- The next day the girl went to class and they were learning how to count
- The girl told her mother, "Today we were learning how to count.  The class could count to 6, but I was able to count to 10"
- The girl asked her mother, "Is that because I am *?"  
- The Mother replies, "No, it's because you are smart"
- The next day the class went to the gymnasium.  After gym the girls were taking a shower
- The girl told her mother, "Today we went to gym class, after class we all took a shower.  All the girls in my class had no boobs and I have 36Ds.  Is that because I am *?"  
- The Mother replies, "No it's because you are 25"

#### Religious

###### 3 Graduates (Mr Walter Shafto)

Introduction:  Mr Walter Shafto - he was the Greatest, had so many jokes, I wish I could remember all of them

- Three Catholic Girls just Graduated High School
- Mother Superior (MS) asked each of them what are they going to do in the future
- The first Girl says, "I am going to be a Scientist"
- MS says, "Oh Bless you Child"
- The second Girl says, "I am going to be an Accountant"
- MS says, "Oh Bless you Child"
- The third Girl says, "I am going to be a Prostitute"
- MS almost faints and says, "What did you say?"
- The third Girl says, "I am going to be a Prostitute"
- MS says, "Oh Thank Heaven, I though you said Protestant, Bless you Child"

###### Three Nuns - Adam and Eve

- Three Nuns were in a car and there was a terrible accident and they all died
- They are being judged and Jesus told them, "You all did well on the Earth, however, I will ask each of you a question and if you get the question correct, Heaven otherwise ...."
- Jesus asks the first Nun, "Who delivered the 10 commandments?"
- Nun replies, "Moses", HEAVEN
- Jesus asks the second Nun, "Who is the Saviour of the World?"
- Nun replies, "Jesus, you are God and the Saviour of the World", HEAVEN
- Jesus asks the third Nun, "What did Eve say the first time she saw Adam?".
- The Nun is scratching her head, thinking, doesn't know and says, "That's a hard one".  HEAVEN

###### Arm and a Leg (Johnny S)

- God created Adam and Adam was all happy, got along with the animals, had shelter and food.  Everything was great
- After a while Adam started getting bored, lonely and depressed
- God said to Adam, "Why are you so sad?"
- Adam said, "I don't know what it is, you have given me everything and there is something missing"
- God said, "Adam, I am God and I know everything, what you need is a Woman"
- Adam said, "What is a Woman?"
- God said, "A Woman is just like you, just a little different.  So what do you want your Woman to do?"
- Adam said, "I would like her to wash, iron, clean, hunt, fish, basically be my servant"
- God said, "Hold on there Adam, that's going to cost you, cost you a lot"
- Adam said, "What's it going to cost me?"
- God said, "An Arm and a Leg"
- Adam said, "What do I get for a Rib?"

##### Son does go to Church ()

- A mother wakes up her son and says, "Son, it's Sunday morning and you need to get up and go to Church".
- The son says, "I don't want to go to church because of two reasons.  They don't like me and I don't like them".
- The mother says, "Well I have two reasons why you should go to church.  One you are 54 years old and two you're the Pastor".

#### Sick

###### Son Learns on His Own

- A young naive Son about 13 years old, needs to talk to his Father
- Looking throughout the house and finally finds the Parents *
- Both Parents are shocked and surprised, the Father quickly says, "Son, calm down, let Daddy finish up and I will explain to you later"
- The Son leaves the room waiting for his Father
- The Father finishes and is walking throughout the house and finally finds his Son *
- The Father is in shock and says, "What are you doing?"
- The Son says, "Well I got tired of waiting for you, I saw you *"

###### Hotdog - Drinks for Free

- Two guys wake up in the morning after going out and boozing it all up the previous night
- They had a blast and agreed, let's do it again
- Guy 1 says, "How much money do you have?"
- Guy 2 says, "Nothing" and they are both sad
- Then Guy 2 asks Guy 1, "How much money do you have?", One Dollar
- Guy 2 says, "Perfect, let's go to the store and buy the largest hotdog we can get for a dollar
- They purchase a hotdog and the plan is go to the bar, start a tab
- Once the bartender asks them to pay the tab, Guy 1 who has the hotdog *
- They go to the first bar.  Built a $50 tab and the Bartender says, "Hey you need to pay the tab"
- Immediately Guy 1 *, pulls out the hotdog and Guy 2 starts going *
- Bartender says, "You *, get out of here".  FREE DRINKS
- The two Guys continue their stunt in 5 bars
- Finally, Guy 2 says, "Hey my knees are getting sore, let's switch, give me the hotdog
- Guy 1 says, "Hotdog?  Oh, I got hungry after the second bar

###### 3 Syllables

- 1st Grade Teacher tells the class, the past weeks we were learning about 1 and 2 syllable words
- Today we are going to continue to advance with 3 syllable words, before we start, is there anyone in the class that knows a 3 syllable word?
- Little Bobby says, "I know a 3 syllable word, Urinate"
- The Teacher says, "Bobby, well that is kind of nasty, however, you are correct"
- Bobby says, "I would of gave you a 10 if you *"

###### Father / Son (James - BT)

- A Father needs to talk to his son, walking throughout the home, checking every room in the home
- The Father finally opens up his son's bedroom door and sees his son *
- The Father says, "Son, you need to stop doing that or you will go blind"
- Son says, "Dad, I am over here"

###### Jeffrey Dahmer ()

- Jeffrey Dahmer's Mother visits him for lunch
- They are eating lunch and his Mother says, "Jeffrey, I don't like your friends"
- Jeffrey says, "Just keep eating"

###### Aliens versus Gas Pumps (James - BT)

- Two Aliens were observing the Earth and decided to land, they happen to land at night in a small town
- The Aliens walk to a Gas Station which is Closed, no one there
- The Aliens walk up to a Gas Pump, pull out their Ray Guns and say, "Take us to your leader"
- The Gas Pump says nothing
- The Aliens make the same request again
- The Gas Pump says nothing
- The Aliens pull their triggers, the Gas Pump explodes throwing the Aliens several feet away
- The Aliens recuperate and are looking at each other
- One Alien said to the other Alien, "I knew that guy was a badass"
- He continued, "Anyone who can wrap their * around their body and stick it in their ear is a bad ass"

###### Farmer and the Alien ()

- Farmer was sitting on the porch late one night and saw a ship land in his field
- An Alien came out of the ship, walked towards the Farmer 
- The Alien said, "I will give you $1M if you give me all your sheep".  The Farmer agreed
- The Alien went back inside the ship and came out with a box, opened up the box and there was a small sheep inside which came out and ate all the Farmers sheep.  The Alien then left
- 3 months later the Alien returned and said, "I will give you $1M if you give me all your pigs".  The Farmer agreed
- The Alien went back inside the ship and came out with a box, opened up the box and there was a small pig inside which came out and ate all the Farmers pigs.  The Alien then left
- 3 months later the Alien returned and said, "I will give you $1M if you give me all your cows".  The Farmer agreed
- The Alien went back inside the ship and came out with a box, opened up the box and there was a small cow inside which came out and ate all the Farmers cows.  The Alien then left
- 3 months late the Alien returned and started to speak, however, the Farmer interrupted the Alien
- The Farmer said, "I will give you $3M if you can go back into your ship and come out with a box with a small *"

###### Rich Man ()

- A rich man gives three women $5000 each and asks them to buy something for him and whoever gives him the best gift he will marry them
- The first woman purchased jewelry and the man is pleased
- The second woman purchased fine clothing and the man is pleased
- The third woman purchased a golf cart, clubs and the man is pleased
- Which woman did the man marry?  The woman with the biggest *

## Beer Theories

#### Authorities

|Author|Statement|
|--|--|
|Babe Ruth|Sometimes when I reflect on all the beer I drink, I feel ashamed.  Then I look into the glass and think about the workers in the brewery and all of their hopes and dreams.  If I didn't drink this beer, they might be out of work and their dreams would be shattered.  I think, "It is better to drink this beer and let their dreams come true than be selfish and worry about my liver|
|Lyndon B. Johnson|I feel sorry for people who don't drink.  When they wake up in the morning, that's as good as they're going to feel all day|
|Paul Horning|When I read about the evils of drinking, I gave up reading|
|H. L. Mencken|24 hours in a day, 24 beers in a case.  Coincidence?  I think not.|
|George Bernard Shaw|When we drink, we get drunk.  When we get drunk, we fall asleep.  When we fall asleep, we commit no sin.  When we commit no sin, we go to heaven.  So, let's all get drunk and go to heaven!|
|Benjamin Franklin|Beer is proof that God loves us and wants us to be happy|
|Dave Barry|Without question, the greatest invention in the history of mankind is beer.  Oh, I grant you that the wheel was also a fine invention, but the wheel does not go nearly as well with pizza|
|W. C. Fields|BEER: HELPING UGLY PEOPLE HAVE SEX SINCE 3000 B.C.|
|Professor Irwin Corey|Remember "I" before "E," except in Budweiser|
|Leo Durocher|To some it's a six-pack, to me it's a Support Group.  Salvation in a can!|

## Cliff Calvin explained the" Buffalo Theory" to his buddy Norm

Well, ya see, Norm, it's like this.  A herd of buffalo can only move as fast as the slowest buffalo.  And when the herd is hunted, it is the slowest and weakest ones at the back that are killed first. This natural selection is good for the herd as a whole, because the general speed and health of the whole group keeps improving by the regular killing of the weakest members! ; In much the same way, the human brain can only operate as fast as the slowest brain cells.  Excessive intake of alcohol, as we know, kills brain cells.  But naturally, it attacks the slowest and weakest brain cells first.  In this way, regular consumption of beer eliminates the weaker brain cells, making the brain a faster and more efficient machine!  That's why you always feel smarter after a few beers.
